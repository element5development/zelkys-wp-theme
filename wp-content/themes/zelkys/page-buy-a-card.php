<?php get_header(); ?>

<main class="page-container page-purchase-a-card">
    <div class="buy-card-head">
        <p class="buy-card-head__title">Buy a card now, pick it up when you come in!</p>

        <p class="buy-card-head__subtitle">(And a secret free gift...)</p>

            <p class="buy-card-head__description game-point-switch-guest">
                Have an account?
                <a class="link" href="/sign-in">Sign In</a>
                or
                <a class="link" href="/register">Create an Account</a>
            </p>
    </div>

    <?php if ( have_posts() ) :
        while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>