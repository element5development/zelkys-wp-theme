<?php
/* Template Name: Packages */
get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php get_template_part( 'template-parts/inner', 'header' ); ?>
  <?php get_template_part( 'template-parts/fancy', 'title' ); ?>
  <section class="price text-center" style="padding-bottom: 0;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="heading" style="padding-top: 0">
            <h3><?php the_field('package_section_heading'); ?></h3> 
            <p><?php the_field('package_section_sub_heading'); ?></p>
          </div>
        </div>
      </div>
      <div class="row">

        <?php if( have_rows('small_packages') ): ?>
          <?php $num = 1; while ( have_rows('small_packages') ) : the_row(); ?>
            <div class="price-box-container">
              <?php echo do_shortcode('
                [game-point-add-package-button id="' . get_sub_field('package_id') . '"]
                  <div class="price-box">
                    <h3>' . get_sub_field('big_price') . '
                      <span>' . get_sub_field('small_price') .'</span>
                    </h3>
                    <p>' . get_sub_field('text') . '</p>
                  </div>
                [/game-point-add-package-button]
              ') ?>
            </div>
          <?php $num++; endwhile; ?>
        <?php endif; ?>

        <?php if( have_rows('big_packages') ): ?>
          <?php $num = 1; while ( have_rows('big_packages') ) : the_row(); ?>
            <?php echo do_shortcode('
              [game-point-add-package-button id="' . get_sub_field('package_id') . '"]
                <div class="price-box bigbox">
                  <h4>' . get_sub_field('big_title') . '</h4>
                  <h3>' . get_sub_field('big_price') . ' <br>
                    <span>' . get_sub_field('small_price') . '</span>
                  </h3>
                  <p>' . get_sub_field('text') . '</p>
                </div>
              [/game-point-add-package-button]
            ') ?>
          <?php $num++; endwhile; ?>
        <?php endif; ?>

      </div>
    </div>
  </section>

        <?php get_template_part( 'template-parts/section', 'buttons' ); ?>

        <section class="price text-center">
              <div class="container">
                    <div class="row">
                          <div class="col-md-12">
                                <div class="section_heading">
                                    <h3><?php the_field('wwa_heading'); ?></h3> 
                                </div>
                          </div>
                    </div>
              </div>
        </section>

        <section class="tabs inner first">
            <div class="container">
                <?php if (get_field('wwa_image')): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <center><img src="<?php echo get_field('wwa_image'); ?>" style="margin-top: -71px;" class="img-responsive"></center>
                        </div>
                    </div>
                <?php endif ?>
                <?php if (get_field('wwa_content')): ?>
                <div class="row small_container" style="margin-bottom: -20px;">
                    <div class="col-md-12 price-sec big_text package_head">
                        <?php the_field('wwa_content') ?>    
                    </div>
                </div>
                <?php endif ?>
            </div>
        </section>

        <section class="price-content one" style="padding-top: 0px; padding-bottom: 0;  display: none;">
            <div class="container">
                <h3 class="text-center" style="margin-top: 0;"><?php the_field('card_details_heading'); ?></h3>
                <div class="row">
                    <div class="col-md-6 pull-right">
                        <img src="<?php the_field('pink_card_image') ?>">
                    </div>
                    <div class="col-md-6">
                        <h4><?php the_field('pink_card_details_heading') ?></h4>
                        <p><?php the_field('pink_card_details_text') ?></p>
                        <button>Some of the benefits:</button>
                        <ul>
                              <?php if( have_rows('pink_card_benefits') ): ?>
                                    <?php $num = 1; while ( have_rows('pink_card_benefits') ) : the_row(); ?>
                                    <li><?php the_sub_field('text'); ?></li>
                                    <?php $num++; endwhile; ?>
                              <?php endif; ?>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
        <section class="price-content two" style="padding: 0; display: none;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="<?php the_field('gold_card_image') ?>">
                    </div>
                    <div class="col-md-6">
                        <h4><?php the_field('gold_card_details_heading') ?></h4>
                        <p><?php the_field('gold_card_details_text') ?></p>
                        <button>Some of the benefits:</button>
                        <ul>
                              <?php if( have_rows('gold_card_benefits') ): ?>
                                    <?php $num = 1; while ( have_rows('gold_card_benefits') ) : the_row(); ?>
                                    <li><?php the_sub_field('text'); ?></li>
                                    <?php $num++; endwhile; ?>
                              <?php endif; ?>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
<style>
  .price-content .row {
    margin-top: 80px;
  }
</style>
        <section class="price-content three" style="padding-top: 0; display: none;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 pull-right">
                        <img src="<?php the_field('platinum_card_image') ?>">
                    </div>
                    <div class="col-md-6">
                        <h4><?php the_field('platinum_card_details_heading') ?></h4>
                        <p><?php the_field('platinum_card_details_text') ?></p>
                        <button>Some of the benefits:</button>
                        <ul>
                              <?php if( have_rows('platinum_card_benefits') ): ?>
                                    <?php $num = 1; while ( have_rows('platinum_card_benefits') ) : the_row(); ?>
                                    <li><?php the_sub_field('text'); ?></li>
                                    <?php $num++; endwhile; ?>
                              <?php endif; ?>
                        </ul>

                        <small><?php the_field('card_details_bottom_text') ?></small>

                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; endif; ?>
    
    
    <footer class="footer">
            <div class="container text-center">
                  <div class="row footer-menu">
                        <div class="col-md-12">
                               <div class="footer-logo" style="display: none;">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/footer-logo.png">
                              </div>
                              <div class="subscribe">
                                    <h3>register your card</h3>
                                    <div class="subscribe-box clearfix">
                                      <?php echo do_shortcode('[game-point-register-card-init-form]'); ?>
                                    </div>
                              </div>
                              <ul>
                                    <?php
                              wp_nav_menu(array(
                                  'theme_location' => 'footer-menu',
                                  'container'      => '',
                                  'items_wrap'    => '%3$s'
                              ));
                           ?>
                              </ul>
          <div class="social-icons">
            <a href="<?php echo ot_get_option('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="<?php echo ot_get_option('instagram'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
          </div>
                              <div class="copyrights">
            <?php echo ot_get_option('copyrights_text'); ?>
                              </div>
                        </div>
                  </div>
            </div>
      </footer>
  <div class="mobile-nav">  
    <div class="close-btn">
      <img src="<?php bloginfo('template_url'); ?>/assets/images/cross.png">
    </div>
    <figure class="text-center">
    <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/mobile-logo.png"></a>
    </figure>
        <?php
        wp_nav_menu( array(
            'theme_location'    => 'main-menu',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'my-nav-bar',
            'container_id'      => 'myNavbar',
            'menu_class'        => 'nav navbar-nav navbar-right',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker()
    ) );
        ?>
    </div>
</div>

  <script src="<?php bloginfo('template_url'); ?>/assets/js/lib.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/isotope.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/parallax.js"></script>
  <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function ($) {
    var userloggedIn = $('#menu-item-716').is(":hidden");
    // console.log(userloggedIn);
    if (userloggedIn) {
      console.log('subscribe - visible');
    } else {
      $('footer .subscribe').hide();
      // .css('visibility', 'hidden');
      $('.footer-logo').show();
    }
  });
    var maxSlides,
        width = $(window).width();

    if (width < 600) {
        maxSlides = 2;
    } else {
        maxSlides = 4;
    }

    jQuery(function(){
      $('.testi-slider').bxSlider({
            slideWidth: 330,
          minSlides: 1,
          maxSlides: 3,
          slideMargin: 20,
          pager: false
        });

      $('.gallery-slider').bxSlider({
            slideWidth: 230,
          minSlides: 2,
          maxSlides: maxSlides,
          slideMargin: 20,
          pager: false
        });

      $('.get_all_images_slider').bxSlider({
            slideWidth: 124,
          minSlides: 3,
          maxSlides: 3,
          moveSlides: 1,
          slideMargin: 10,
          controls: true,
          pager: false
        });

      $('.games-slider').bxSlider({
          pager: false,
          controls: true
        });

      $('.testimonial-int-slider').bxSlider({
          pager: false,
          controls: true
        });

        var $grid = $('.grid').isotope({
        itemSelector: '.element-item',
        masonry: {
          columnWidth: 0
        }
      });

      setTimeout(function(){
        $('#newGallery').click();
      }, 500);

      // filter items on button click
      $('.filter-button-group').on( 'click', 'button', function() {
          var filterValue = $(this).attr('data-filter');
          $grid.isotope({ filter: filterValue });
          $('button').removeClass('active');
        $(this).addClass('active');
      });

      $('.game-vertical').bxSlider({
          mode: 'vertical',
          minSlides: 8,
          slideMargin: 0,
          auto: false,
          moveSlides: 1,
          controls: true,
          pager: false,
          infiniteLoop: false,
          hideControlOnEnd: true
      });

      $('.timingFilter button').click(function(){
        $('.seasons').hide();
        var abc = $(this).attr('data-filter');
        $(abc+'-season').show();
        $('.seasons-text').html(abc.replace('.','')+'-season');
      });

      $('.parallax-window').parallax( {
        speed: 0.8,
      });

      $('.north-map a').click(function() {
          $('.nav-pills>li.active-north').addClass('active');
          $('.nav-pills>li.active-central').removeClass('active');
          $('.nav-pills>li.active-south').removeClass('active');
      });

      $('.central-map a').click(function() {
          $('.nav-pills>li.active-central').addClass('active');
          $('.nav-pills>li.active-north').removeClass('active');
          $('.nav-pills>li.active-south').removeClass('active');
      });

      $('.south-map a').click(function() {
          $('.nav-pills>li.active-south').addClass('active');
          $('.nav-pills>li.active-north').removeClass('active');
          $('.nav-pills>li.active-central').removeClass('active');
      });

      $('#my-nav').click(function() {
          $('.mobile-nav').fadeIn(400);
      });

      $('.close-btn').click(function() {
          $(this).parent('.mobile-nav').fadeOut(400);
      });

    $('#menu-item-75').html('<a title href="tel:<?php echo ot_get_option('south_phone'); ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link southAdd" id="menu-item-dropdown-75"><i class="phone-icon fa fa-phone"></i><font><?php echo ot_get_option('south_phone'); ?></font></a> <a  href="tel:<?php echo ot_get_option('central_phone'); ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link centralAdd" id="menu-item-dropdown-75"><i class="phone-icon fa fa-phone"></i><font><?php echo ot_get_option('central_phone'); ?></font></a> <a href="tel:<?php echo ot_get_option('north_phone'); ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link northAdd" id="menu-item-dropdown-75"><i class="phone-icon fa fa-phone"></i><font><?php echo ot_get_option('north_phone'); ?></font></a> <ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-75" role="menu"> <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-391" class="southAdd menu-item menu-item-type-custom menu-item-object-custom menu-item-391 nav-item" style="display: block;"><a target="_blank" href="https://maps.google.com/?q=<?php echo ot_get_option('southaddress'); ?>" class="dropdown-item"><div class="southAdd" style="display: block;"><table><tbody><tr><td>South address:</td><td><?php echo ot_get_option('south_address'); ?></td></tr></tbody></table></div></a></li> <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-392" class="centralAdd menu-item menu-item-type-custom menu-item-object-custom menu-item-392 nav-item" style="display: none;"><a target="_blank" href="https://maps.google.com/?q=<?php echo ot_get_option('central_address'); ?>" class="dropdown-item"><div class="centralAdd" style="display: none;"><table><tbody><tr><td>Central address:</td><td><?php echo ot_get_option('central_address'); ?></td></tr></tbody></table></div></a></li> <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-393" class="northAdd menu-item menu-item-type-custom menu-item-object-custom menu-item-393 nav-item" style="display: none;"><a target="_blank" href="https://maps.google.com/?q=<?php echo ot_get_option('north_address'); ?>" class="dropdown-item"><div class="northAdd" style="display: none;"><table><tbody><tr><td>North address:</td><td><?php echo ot_get_option('north_address'); ?></td></tr></tbody></table></div></a></li> </ul>');

      $('.southAdd').show();
      $('.centralAdd').hide();
      $('.northAdd').hide();

      $('.locations ul li a').click(function(){
        $('.locations ul li a').removeClass('active');
        var htmlLo = $(this).html();
        $('#menu-item-75 > a font').html($(this).attr('dataCall'));
        $('#menu-item-75 > a').attr('href', 'tel:'+$(this).attr('dataCall'));
        if (htmlLo == 'NORTH') {
          $('.southAdd').hide();
          $('.centralAdd').hide();
          $('.northAdd').show();
        }
        if (htmlLo == 'SOUTH') {
          $('.southAdd').show();
          $('.centralAdd').hide();
          $('.northAdd').hide();
        }
        if (htmlLo == 'CENTRAL') {
          $('.southAdd').hide();
          $('.centralAdd').show();
          $('.northAdd').hide();
        }
        $(this).addClass('active');
      });

      $('.locationButton').click(function(){
        if ($(this).find('.dropdown-menu').is(":visible")) {
          $(this).find('.dropdown-menu').hide();
        } else {
          $('.locationButton .dropdown-menu').hide();
          $(this).find('.dropdown-menu').show();
        }
      });
    $('li#menu-item-637[style="display: block !important;"]').attr('style', 'display: inline-block !important;');
      $('li#menu-item-714[style="display: block !important;"]').attr('style', 'display: inline-block !important;');
      // $('#myNavbar ul li a').click(function(){
      //  alert();
      //  $('#myNavbar ul li').removeClass('open');
      //  setTimeout(function(){
      //    $(this).parent().addClass('open');
      //  }, 500);
      // });
      if ($(window).width() < 800) {
        $('.tab-content .tab-pane').scrollLeft(400)
      }

    });

    if ($(window).width() > 1024) {
      jQuery(window).scroll(function() {
        // console.log($(this).scrollTop());
       //    if ($(this).scrollTop() > 600) { //use `this`, not `document`
       //        $('.fixy').addClass('fun-fixed');
       //        $('.map').css('margin-top', '995px');
       //    } else {
       //        $('.map').css('margin-top', '0px');
       //        $('.fixy').removeClass('fun-fixed');
       //    }
      });
    }

//    jQuery(window).scroll(function() {
//      var scroll = $(this).scrollTop();
//      if (scroll >= 1) {
//        $(".header-desktop, .header-mobile").addClass("sticky-header");
//      } else {
//        $(".header-desktop, .header-mobile").removeClass("sticky-header");
//      }
//        if (scroll > 900) { //use `this`, not `document`
//            $('.fun-detail').css({
//                // 'display': 'none'
//            });
//        } else {
//          $('.fun-detail').css({
//                // 'display': 'block'
//            });
//        }
//    });
  </script>
  <?php wp_footer(); ?>
</body>
</html>
