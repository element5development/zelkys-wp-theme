<?php
/* Template Name: Home */
get_header(); ?>



      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <section class="fixy">
            <!-- <div style="height: 995px;"> -->
            <div>
            <style>
				.videoBG_wrapper, .videoBG, #bVideo {
/* 					left: -50% !important;
					transform: translateX(50%); */
/* 					width: 100% !important; */
/* 					transform: translate(-0%, 50%); */
				}
            @media (min-width: 768px) {
                  .main-wrapper {
                        padding-top: 80px;
                  }
            }
            @media (max-width: 1024px) {
				.videoBG, #bVideo {
					height: auto !important;
					position: relative !important;
					width: 100% !important;
					margin-bottom: -3px;
				}
                  #div_video, .banner {
					  height: auto !important;
                  }
            }
            @media (max-width: 767px) {
				.videoBG, #bVideo {
					height: auto !important;
					position: relative !important;
					width: 100% !important;
					margin-bottom: -3px;
				}
                  .banner {
                      margin-top: 80px;
					  height: auto !important;
                  }
            }
            @media (min-width: 991px) and (max-width: 1025px) {
				header .navbar ul li a {
					font-size: 12px;
					padding:15px 5px;
				}
				.videoBG, #bVideo, .banner {
					height: 260px !important;
					position: relative !important;
/* 					width: 100% !important; */
					margin-bottom: -3px;
					overflow: hidden;
				}
				#bVideo {
					height: 300px !important;
				}
            }
            @media (min-width: 760px) and (max-width: 780px) {
				.videoBG, #bVideo, .banner {
					height: 200px !important;
					position: relative !important;
/* 					width: 100% !important; */
					margin-bottom: -3px;
					overflow: hidden;
				}
				#bVideo {
					height: 240px !important;
				}
				.banner {
					margin-top: -10px;
				}
				.mobile-phone {
					margin-right: -15px;
				}
            }
				@media (min-width: 767px) and (max-width: 768px) {
					.banner {
						margin-top: -15px !important;
					}
				}

            /*div.banner {background-image: url(<?php echo (get_field('banner_image')) ? get_field('banner_image') : get_bloginfo('template_url').'/assets/images/screen-shot-2018-03-06-at-15-18-44.png'; ?>);}*/
      </style>

            <div class="banner" id="div_video">
                <video src="<?php bloginfo('template_url'); ?>/assets/zelkys_banner.mp4" type="video/mp4" class="front-video" autoplay loop muted playsinline>
                    <source src="<?php bloginfo('template_url'); ?>/assets/zelkys_banner.mp4" type='video/mp4'>
                    <source src="<?php bloginfo('template_url'); ?>/assets/zelkys_banner.webm" type='video/webm'>
                    <source src="<?php bloginfo('template_url'); ?>/assets/zelkys_banner.mov" type='video/mov'>
                </video>
            </div>

            <section class="fun-with-family" style="position: relative; z-index: 9;">
                  <div class="container">
                        <figure class="fun-img" style="background-image: none;">
                              <!-- <img class="desktop" src="<?php bloginfo('template_url'); ?>/assets/images/Main-Board1.gif"> -->
                              <img id="home-animation" class="deskdtop" src="<?php bloginfo('template_url'); ?>/assets/images/Step1.png">
                              <!-- <img class="mobile" src="<?php bloginfo('template_url'); ?>/assets/images/mob-banner.png"> -->
<!--                               <img class="mobile" src="<?php bloginfo('template_url'); ?>/assets/images/Board.png"> -->
                        </figure>
                        <!-- <div class="row text-center">
                              <div class="curve-box">
                                    <div class="curve-shadow">
                                          <h3>Fun for the family, <br>guaranteed.</h3>
                                          <h4><span>30</span> Years And Counting</h4>
                                    </div>
                              </div>
                              
                              <div class="arcade">
                                    <div class="shadow">
                                          <h3>Best Family Arcades <span> in the State of Delaware</span></h3>
                                    </div>
                              </div> </div>-->
                        <div class="fun-detail text-center">
                              <?php the_field('welcome_content'); ?>
                              <h3 class="mt-40 second"><img src="<?php bloginfo('template_url'); ?>/assets/images/slice-1.png"> <?php the_field('drive_heading'); ?> <img src="<?php bloginfo('template_url'); ?>/assets/images/slice-1.png"></h3>
                        </div>
                  </div>
            </section>
            </div>
      </section>
            <section class="map">
                        <div id="exTab1">
                              <div class="container">
                                    <div class="map-marker">
                                          <?php if( have_rows('home_locations') ): ?>
                                                <?php $num = 1; while ( have_rows('home_locations') ) : the_row(); ?>
                                                      <div class="<?php echo ($num==1) ? 'north-map':''; ?><?php echo ($num==2) ? ' central-map':''; ?><?php echo ($num==3) ? 'south-map':''; ?>">
                                                            <a href="javascript:;">
                                                                  <img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png">
                                                                  <span class="mobile"><?php the_sub_field('title'); ?></span>
                                                            </a>
                                                      </div>
                                                <?php $num++; endwhile; ?>
                                          <?php endif; ?>
                                    </div>
                              </div>
                              <div class="tab-content clearfix">
                                    <div class="tab-pane active" id="north">
                                          <!-- <div class="parallax-window" data-parallax="scroll" data-image-src="<?php the_field('map_image'); ?>"></div> -->

                                          <div class="map-marker show768">
                                                <?php if( have_rows('home_locations') ): ?>
                                                      <?php $num = 1; while ( have_rows('home_locations') ) : the_row(); ?>
                                                            <div class="<?php echo ($num==1) ? 'north-map':''; ?><?php echo ($num==2) ? ' central-map':''; ?><?php echo ($num==3) ? 'south-map':''; ?>">
                                                                  <a href="javascript:;">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png">
                                                                        <span class="mobile"><?php the_sub_field('title'); ?></span>
                                                                  </a>
                                                            </div>
                                                      <?php $num++; endwhile; ?>
                                                <?php endif; ?>
                                          </div>
<!-- 										<img src="http://www.zelkys.com/wp-content/uploads/2018/06/map-new.png"> -->
                                          <img src="<?php the_field('map_image'); ?>">
                                    </div>
                                    <div class="tab-pane" id="central">
                                          <!-- <div class="parallax-window" data-parallax="scroll" data-image-src="<?php the_field('map_image'); ?>"></div> -->

                                          <div class="map-marker show768">
                                                <?php if( have_rows('home_locations') ): ?>
                                                      <?php $num = 1; while ( have_rows('home_locations') ) : the_row(); ?>
                                                            <div class="<?php echo ($num==1) ? 'north-map':''; ?><?php echo ($num==2) ? ' central-map':''; ?><?php echo ($num==3) ? 'south-map':''; ?>">
                                                                  <a href="javascript:;">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png">
                                                                        <span class="mobile"><?php the_sub_field('title'); ?></span>
                                                                  </a>
                                                            </div>
                                                      <?php $num++; endwhile; ?>
                                                <?php endif; ?>
                                          </div>
                                         <img src="<?php the_field('map_image'); ?>">
                                    </div>
                                    <div class="tab-pane" id="south">
                                          <!-- <div class="parallax-window" data-parallax="scroll" data-image-src="<?php the_field('map_image'); ?>"></div> -->

                                          <div class="map-marker show768">
                                                <?php if( have_rows('home_locations') ): ?>
                                                      <?php $num = 1; while ( have_rows('home_locations') ) : the_row(); ?>
                                                            <div class="<?php echo ($num==1) ? 'north-map':''; ?><?php echo ($num==2) ? ' central-map':''; ?><?php echo ($num==3) ? 'south-map':''; ?>">
                                                                  <a href="javascript:;">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png">
                                                                        <span class="mobile"><?php the_sub_field('title'); ?></span>
                                                                  </a>
                                                            </div>
                                                      <?php $num++; endwhile; ?>
                                                <?php endif; ?>
                                          </div>
                                          <img src="<?php the_field('map_image'); ?>">
                                    </div>
                              </div>
                              <div class="tabs desktop">
                                    <ul  class="nav nav-pills text-center container">
                                          <?php if( have_rows('home_locations') ): ?>
                                                <?php $num = 1; while ( have_rows('home_locations') ) : the_row(); ?>
                                                      <li class="<?php echo ($num==1) ? 'active active-north':''; ?><?php echo ($num==2) ? ' active-central':''; ?><?php echo ($num==3) ? 'active-south':''; ?> ">
                                                            <a  href="/our-locations" data-toggle="tab" onclick="location.href = '/our-locations/';">
                                                                  <img src="<?php the_sub_field('image'); ?>" width="200">
                                                            </a>
                                                            <div class="tabs-text">
                                                                  <?php the_sub_field('title'); ?>
                                                            </div>
                                                      </li>
                                                <?php $num++; endwhile; ?>
                                          <?php endif; ?>
                                    </ul>
                              </div>
                        </div>
            </section>
            <section class="purchase text-center purchase_sec">
                  <div class="container">
                        <h6 class="desktop"><?php the_field('purchase_button_heading'); ?></h6>
                        <div class="desktop purchase-button text-center">
                              <figure class="arrow-left"><img class="animated-arrow" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-left.png"></figure>
                                    <a href="<?php the_field('purchase_button_link'); ?>" class="purchase-btn"><?php the_field('purchase_button_text'); ?></a>
                              <figure class="arrow-right">
                            <img class="animated-arrow-right" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-right.png">
                        </figure>
                        </div>
                        <h3><?php the_field('testimonial_section_heading'); ?></h3>
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/stars-icon.png">
                        <h2><?php the_field('testimonial_section_heading_2'); ?></h2>
                  </div>
            </section>
            <section class="testimonial">
                  <div class="container">
                        <ul class="testi-slider">
                              <?php if( have_rows('home_testimonials') ): ?>
                                    <?php $num = 1; while ( have_rows('home_testimonials') ) : the_row(); ?>
										<li>
											<h3 class="author"><?php the_sub_field('name'); ?></h3>
											<h4 class="city"><?php the_sub_field('title'); ?></h4>
											<p><?php the_sub_field('text'); ?></p>
										</li>
                                    <?php $num++; endwhile; ?>
                              <?php endif; ?>
                        </ul>
                  </div>
            </section>
            <section class="games">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                                    <h5><?php the_field('games_section_heading'); ?></h5>
                                    <h3><?php the_field('games_section_heading_2'); ?></h3>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="games-sec">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                                    <ul class="games-slider">
                                          <?php if( have_rows('games_slides') ): ?>
                                                <?php $num = 1; while ( have_rows('games_slides') ) : the_row(); ?>
                                          <li>
                                                <figure class="big-img">
                                                      <img src="<?php the_sub_field('big_image'); ?>">
                                                      <div class="overlay">
                                                            <div>
                                                                  <p>There's hundreds of other games waiting for you: the latest hits, the timeless classics</p>
                                                                  <p><span>and many, many, prizes ...</span></p>
																	  <a href="javascript:;" class="games-btn">See our favorites</a>
                                                            </div>
                                                      </div>
                                                </figure>
                                                <figure class="small-img active">
                                                      <img class="desktop" src="<?php bloginfo('template_url'); ?>/assets/images/games01.png" style="width: 258px;height: 257px; box-shadow: none; transform: scale(1.2);">
                                                      <img class="mobile" src="<?php bloginfo('template_url'); ?>/assets/images/397410-stern-pinball-arcade-playstation-4-front-cover.png" style="width: 258px;height: 257px; box-shadow: none; transform: scale(1.2);">
                                                      <div class="overlay" style="background: none;">
                                                            <div>
                                                                  <p><?php the_sub_field('title'); ?></p>
                                                                  <p><span><?php the_sub_field('text'); ?></span></p>
                                                                  <a href="<?php the_sub_field('button_link'); ?>" class="games-btn" style="padding: 8px 18px;"><?php the_sub_field('button_text'); ?></a>
                                                            </div>
                                                      </div>
                                                </figure>
                                                <figure class="small-img">
                                                      <img src="<?php the_sub_field('small_image_1'); ?>">
                                                      <div class="overlay">
                                                            <div>
                                                                  <p>There's hundreds of other games waiting for you: the latest hits, the timeless classics</p>
                                                                  <p><span>and many, many, prizes ...</span></p>
                                                                  <a href="javascript:;" class="games-btn">See our favorites</a>
                                                            </div>
                                                      </div>
                                                </figure>
                                                <figure class="small-img">
                                                      <img src="<?php the_sub_field('small_image_2'); ?>">
                                                      <div class="overlay">
                                                            <div>
                                                                  <p>There's hundreds of other games waiting for you: the latest hits, the timeless classics</p>
                                                                  <p><span>and many, many, prizes ...</span></p>
                                                                  <a href="http://www.zelkys.com/hot-games/" class="games-btn">See our favorites</a>
                                                            </div>
                                                      </div>
                                                </figure>
                                                <figure class="small-img">
                                                      <img src="<?php the_sub_field('small_image_3'); ?>">
                                                      <div class="overlay">
                                                            <div>
                                                                  <p>There's hundreds of other games waiting for you: the latest hits, the timeless classics</p>
                                                                  <p><span>and many, many, prizes ...</span></p>
                                                                  <a href="http://www.zelkys.com/hot-games/" class="games-btn">See our favorites</a>
                                                            </div>
                                                      </div>
                                                </figure>
                                          </li>
                                                <?php $num++; endwhile; ?>
                                          <?php endif; ?>
                                    </ul>
                                    <div class="mobile overlay-text">
                                          <p>There's hundreds of other games waiting for you: the latest hits, the timeless classics</p>
                                          <p><span>and many, many, prizes ...</span></p>
                                          <a href="<?php bloginfo('url'); ?>/hot-games/" class="games-btn">See our favorites</a>
                                    </div>
                              </div>
                        </div>
                  </div>
            </section>

<style>
	@media (min-width: 1025px) {
		.map .tab-content .tab-pane {
			background-image: url(<?php the_field('map_image'); ?>);
			height: 684px;
			background-position: center;
			background-repeat: no-repeat;
		}
	}
</style>

            <section class="card desktop">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-6">
                                    <div class="card-content">
                                          <h3><?php the_field('swipe_card_heading'); ?></h3>
                                          <?php the_field('swipe_card_content'); ?>
                                          <a href="<?php the_field('swipe_card_button_link'); ?>" class="button"><?php the_field('swipe_card_button_text'); ?></a>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                    <div class="card-content-right">
                                          <h2><?php the_field('swipe_card_right_heading'); ?></h2>
                                          <img class="img-responsive" src="<?php the_field('swipe_card_right_image'); ?>">
                                    </div>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="gallery home_gal_sec">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                                    <h3><?php the_field('gallery_heading'); ?></h3>
								  <p><a href="<?php the_field('gallery_heading_2_link'); ?>" style="color:inherit"><?php the_field('gallery_heading_2'); ?></a></p>
                                    <ul class="gallery-slider">
                                          <?php if( have_rows('home_gallery_images') ): ?>
                                                <?php $num = 1; while ( have_rows('home_gallery_images') ) : the_row(); ?>
                                                <li>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_1'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_1'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_1'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                                                  <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_1'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_2'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_2'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_2'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                                                  <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_2'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                </li>
                                                <li>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_3'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_3'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_3'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                                                  <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_3'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_4'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_4'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_4'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
<!--                                                                   <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_4'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                </li>
                                                <li>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_5'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_5'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_5'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                                                  <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_5'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_6'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_6'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_6'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                                                  <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_6'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                </li>
                                                <li>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_7'); ?>">
                                                            <div class="share">
                                                                  share
                                                                  
                                                                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_sub_field('image_7'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                                                  <a href="https://twitter.com/share?url=<?php the_sub_field('image_7'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                                                  <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                                                  <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php the_sub_field('image_7'); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                                            </div>
                                                      </div>
                                                      <div class="gallery-sec">
                                                            <img src="<?php the_sub_field('image_8'); ?>">
                                                            <div class="share">
                                                                  <strong><a style="font-size: 13px; width: auto; border: 0px; color: #f3a9c4; background: #fff;" href="<?php the_sub_field('last_image_link'); ?>"><?php the_sub_field('last_image_text'); ?> &nbsp; <i class="fa fa-chevron-right"></i></a></strong>
                                                            </div>
                                                      </div>
                                                </li>
                                                <?php $num++; endwhile; ?>
                                          <?php endif; ?>
                                    </ul>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="what-we-do desktop bottom_home_sec">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-12">
                              <?php if( have_rows('bottom_cta') ): ?>
                                    <?php $num = 1; while ( have_rows('bottom_cta') ) : the_row(); ?>
                                    <div class="box">
                                          <a href="<?php the_sub_field('link'); ?>">
                                          <figure>
                                                <img src="<?php the_sub_field('icon'); ?>">
                                                <figcaption><?php the_sub_field('title'); ?></figcaption>
                                          </figure>
                                          </a>
                                    </div>
                                    <?php $num++; endwhile; ?>
                              <?php endif; ?>
                              </div>
                          </div>
                      </div>
            </section>
    <?php endwhile; endif; ?>

<?php get_footer(); ?>