<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>
	<h1 class="entry-title"><?php printf( __( 'Search Results for: %s', 'zel' ), get_search_query() ); ?></h1>
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin-bottom: 50px;">
            <h1 class="header__title"><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </article>
    <?php endwhile; endif; ?>

<?php get_footer(); ?>