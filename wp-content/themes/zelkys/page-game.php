<?php
/* Template Name: Game */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
            <section class="game desktop">
                <div class="container">
                    <div class="row">
                        <aside class="col-md-4">
                            <ul class="game-vertical">
                                <?php if( have_rows('games') ): ?>
                                    <?php $num = 1; while ( have_rows('games') ) : the_row(); ?>
                                        <li class="<?php echo ($num==1) ? 'active' : ''; ?>">
                                            <a href="#tab<?php echo $num; ?>" data-toggle="tab"><?php the_sub_field('game_title'); ?></a>
                                        </li>
                                    <?php $num++; endwhile; ?>
                                <?php endif; ?>
                            </ul>
                        </aside>
                        <div class="col-md-8 tab-content desktop">
                            <?php if( have_rows('games') ): ?>
                                <?php $num = 1; while ( have_rows('games') ) : the_row(); ?>
                                    <div class="row tab-pane <?php echo ($num==1) ? 'active' : ''; ?>" id="tab<?php echo $num; ?>">
                                        <div class="col-md-7 hide-tablet">
                                            <img src="<?php the_sub_field('game_image'); ?>">
                                        </div>
                                        <div class="col-md-5">
                                            <div class="game-content">
                                                <div>
                                                    <h4><?php the_sub_field('game_title'); ?></h4>
                                                    <p><?php the_sub_field('game_description'); ?></p>
                                            		<img class="img-responsive show-tablet" src="<?php the_sub_field('game_image'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $num++; endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>

            <section class="game mobile">
                <div class="container">
                  <div id="accordion1" class="panel-group">
                    <?php if( have_rows('games') ): ?>
                        <?php $num = 1; while ( have_rows('games') ) : the_row(); ?>
                            <div class="panel">
                                <ul>
                                    <li>
                                        <a href="#panelBody<?php echo $num; ?>" class=" accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"><?php the_sub_field('game_title'); ?></a>
                                    </li>
                                </ul>
                                <div id="panelBody<?php echo $num; ?>" class="panel-collapse collapse">
                                    <div>
                                        <h4><?php the_sub_field('game_title'); ?></h4>
                                        <p><?php the_sub_field('game_description'); ?></p>
                                    </div>
                                    <figure>
                                        <img class="img-responsive" src="<?php the_sub_field('game_image'); ?>">
                                    </figure>
                                </div>
                            </div>
                        <?php $num++; endwhile; ?>
                    <?php endif; ?>
                  </div>
                </div>
            </section>
    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>