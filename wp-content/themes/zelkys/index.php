<?php get_header(); ?>

    <?php get_template_part( 'template-parts/inner', 'header' ); ?>
    <section class="blog_sec">
        <div class="container">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="row blog_sec_sec">
                        <?php if (has_post_thumbnail()): ?>
                            <div class="col-md-5 thumbnail_sec desktopBlog">
                                <figure class="imgBgBlog" style="position: relative;">
                                    <div id="mainImage2" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; width: 100%; z-index: 9; background-size: contain; background-position: center; background-repeat: no-repeat;"></div>
                                    <img id="mainImage" src="<?php the_post_thumbnail_url(); ?>" class="img-responsive">
                                </figure>
                                <div class="imagesSlider">
                                    <?php echo get_all_images_slider(get_the_content(), get_the_ID()); ?>
                                </div>
                            </div>
                            <div class="col-md-7 content_sec">
                        <?php else : ?>
                            <div class="col-md-12 content_sec">
                        <?php endif ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class('blog-listing'); ?>>
                                <div class="time mobileBlog" style="color: #94a3a8; font-family: 'Work Sans'; font-size: 16px; font-weight: 700; text-transform: uppercase; letter-spacing: 0.53px; background-color: #edf0f0; padding: 00 10px;"><?php the_time(get_option('date_format')); ?></div>
                                <h2 class="header__title" style="text-transform: uppercase; margin-top: 12px; min-height: 70px;"><?php the_title(); ?></h2>
                                <div class="col-sm-12 thumbnail_sec mobileBlog">
                                    <figure>
                                        <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive">
                                    </figure>
                                </div>
                                <div class="post_meta_sec text-right">
                                    <div class="time desktopBlog" style="color: #94a3a8; font-family: 'Work Sans'; font-size: 16px; font-weight: 700; text-transform: uppercase; letter-spacing: 0.53px; background-color: #edf0f0; padding: 00 10px;"><?php the_time(get_option('date_format')); ?></div>
                                    <div class="share" style="margin-top: 8px; text-align: right;">
                                        share
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/share?url=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                        <?php /*
                                        <a href="https://pinterest.com/pin/create/button/?url=&media=<?php echo get_permalink(); ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                        */ ?>
                                    </div>
                                </div>
                                <div class="text_sec">
                                    <?php the_content(); ?>
                                </div>
                            </article>
                        </div>
                    </div>
                <?php endwhile; ?>
            <div class="text-center">
                <?php wp_pagenavi(); ?>
            </div>
        </div>
    </section>
	<?php endif; ?>
<style type="text/css">
    body {
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,fff1f5+23 */
    background: rgb(255,255,255); /* Old browsers */
    background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,241,245,1) 23%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(255,241,245,1) 23%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(255,241,245,1) 23%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#fff1f5',GradientType=0 ); /* IE6-9 */
    }
</style>
<?php get_footer(); ?>