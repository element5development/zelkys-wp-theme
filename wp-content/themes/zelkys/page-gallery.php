<?php
/* Template Name: Photo Gallery */
get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php get_template_part('template-parts/inner', 'header'); ?>
    <section class="main-gallery">
        <div class="container">
            <div class="button-group filter-button-group">
                <button id="newGallery" class="active btn-tab" data-filter="*" style="outline: 0px;">New</button>
                <?php if (have_rows('gallery')): ?>
                    <?php $num = 1;
                    while (have_rows('gallery')) : the_row(); ?>
                        <?php $filter = strtolower(preg_replace('/\s+/', '_', get_sub_field('gallery_title'))); ?>
                        <button class="btn-tab" data-filter=".<?php echo $filter; ?>"
                                style="outline: 0px;"><?php the_sub_field('gallery_title'); ?></button>
                        <?php $num++; endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="grid">
                <?php if (have_rows('gallery')): ?>
                    <?php 
                    while (have_rows('gallery')) : the_row(); $num = 1;
                    $i = 0; $flag = false;?>
                        <?php $filter = strtolower(preg_replace('/\s+/', '_', get_sub_field('gallery_title'))); ?>
                        <?php $images = get_sub_field('gallery_images'); ?>
                        <?php foreach ($images as $key => $value): ?>
                            <div data-created="<?php echo $value['date']; ?>"
                                 class="element-item <?php echo $filter; ?>" id="<?php echo 'class'.$value['id']; ?>">
                                <figure>
                                    <img src="<?php echo $value['url']; ?>">
                                    <div class="share">
                                        share
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $value['url']; ?>"
                                           target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/share?url=<?php echo $value['url']; ?>"
                                           target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.instagram.com/zelkysbeacharcade/" target="_blank"><i class="fa fa-instagram"></i></a>
                                        <!-- <a href="https://pinterest.com/pin/create/button/?url=&media=<?php echo $value['url']; ?>&description=" target="_blank"><i class="fa fa-pinterest-p"></i></a> -->
                                    </div>
                                </figure>
                            </div>
                            <?php
                            if (++$i == 3) {
                                $flag = true;
                                break;
                            }
                            ?>
                        <?php endforeach ?>

                        <?php
                        // if ($flag)
                        //     break;
                        ?>
                        <?php $num++; endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="button-wrapper">
                <button class="btn btn-show-more purchase-btn" data-page="2" style="display: none;">Show more</button>
            </div>
        </div>
    </section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>