<?php
/* Template Name: Hours */
get_header(); ?>
<?php
    $currentTime = strtotime("now");
    $preSeasonStart = strtotime(get_field('pre-season_start_date'));
    $preSeasonEnd = strtotime(get_field('pre-season_end_date'));
    $inSeasonStart = strtotime(get_field('in-season_start_date'));
    $inSeasonEnd = strtotime(get_field('in-season_end_date'));
    $postSeasonStart = strtotime(get_field('post-season_start_date'));
    $postSeasonEnd = strtotime(get_field('post-season_end_date'));
    $offSeasonStart = strtotime(get_field('off-season_start_date'));
    $offSeasonEnd = strtotime(get_field('off-season_end_date'));
    $holidaySeasonStart = strtotime(get_field('holiday_hours_start_date'));
    $holidaySeasonEnd = strtotime(get_field('holiday_hours_end_date'));
?>
<style type="text/css">.timing_sec h3.name:before {display: none;}</style>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
            <section class="main-gallery timing_sec">
                <div class="container">
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-12 text-center">
                            <h2 style="margin-top: 5px;"><?php echo 'Today is '.date('F d, Y'); ?></h2>
                            <h4>We Are In 
                                <?php if ($currentTime >= $offSeasonStart && $currentTime <= $offSeasonEnd): ?>
                                    <span class="seasons-text" style="text-transform: uppercase;">OFF-SEASON</span>
                                    <style type="text/css">.timing_sec .off-season {display: block;}</style>
                                <?php endif ?>
                                <?php if ($currentTime >= $postSeasonStart && $currentTime <= $postSeasonEnd): ?>
                                    <span class="seasons-text" style="text-transform: uppercase;">POST-SEASON</span>
                                    <style type="text/css">.timing_sec .post-season {display: block;}</style>
                                <?php endif ?>
                                <?php if ($currentTime >= $inSeasonStart && $currentTime <= $inSeasonEnd): ?>
                                    <span class="seasons-text" style="text-transform: uppercase;">IN-SEASON</span>
                                    <style type="text/css">.timing_sec .in-season {display: block;}</style>
                                <?php endif ?>
                                <?php if ($currentTime >= $preSeasonStart && $currentTime <= $preSeasonEnd): ?>
                                    <span class="seasons-text" style="text-transform: uppercase;">PRE-SEASON</span>
                                    <style type="text/css">.timing_sec .pre-season {display: block;}</style>
                                <?php endif ?>
                                <?php if ($currentTime >= $holidaySeasonStart && $currentTime <= $holidaySeasonEnd): ?>
                                    <span class="seasons-text" style="text-transform: uppercase;">HOLIDAY</span>
                                    <style type="text/css">.timing_sec .holidays-season {display: block;}</style>
                                <?php endif ?>
                             Hours</h4>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 40px;">
                        <div class="col-md-12 text-center">
                            <div class="seasons pre-season dont">
                            <?php $preod = get_field('pre-season_open_days');
                            if (in_array(date("l"), $preod)) { ?>
                                    <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                <?php } else { ?>
                                    <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                            <?php } ?>
                            </div>
                            <div class="seasons in-season dont">
                            <?php $inod = get_field('in-season_open_days');
                            if (in_array(date("l"), $inod)) { ?>
                                    <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                <?php } else { ?>
                                    <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                            <?php } ?>
                            </div>
                            <div class="seasons post-season dont">
                            <?php $postod = get_field('post-season_open_days');
                            if (in_array(date("l"), $postod)) { ?>
                                    <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                <?php } else { ?>
                                    <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                            <?php } ?>
                            </div>
                            <div class="seasons off-season dont">
                            <?php $offod = get_field('off-season_open_days');
                            if (in_array(date("l"), $offod)) { ?>
                                    <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                <?php } else { ?>
                                    <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                            <?php } ?>
                            </div>

                            <div class="seasons holidays dont">
                            <?php $holstd = get_field('holiday_hours_open_days');
                            if (in_array(date("l"), $holstd)) { ?>
                                    <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                <?php } else { ?>
                                    <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                            <?php } ?>
                            </div>
                        </div>
                    </div>



                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-md-12">
                            <div class="button-group filter-button-group timingFilter">
                                <button data-filter=".pre" style="outline: 0px;" 
                                    <?php if ($currentTime >= $preSeasonStart && $currentTime <= $preSeasonEnd): ?>
                                        class="active"
                                    <?php endif ?>
                                >PRE-SEASON</button>
                                <button data-filter=".in" style="outline: 0px;" 
                                    <?php if ($currentTime >= $inSeasonStart && $currentTime <= $inSeasonEnd): ?>
                                        class="active"
                                    <?php endif ?>
                                >IN-SEASON</button>
                                <button data-filter=".post" style="outline: 0px;" 
                                    <?php if ($currentTime >= $postSeasonStart && $currentTime <= $postSeasonEnd): ?>
                                        class="active"
                                    <?php endif ?>
                                >POST-SEASON</button>
                                <button data-filter=".off" style="outline: 0px;" 
                                    <?php if ($currentTime >= $offSeasonStart && $currentTime <= $offSeasonEnd): ?>
                                        class="active"
                                    <?php endif ?>
                                >OFF-SEASON</button>
                                <button data-filter=".holidays" style="outline: 0px;" 
                                    <?php if ($currentTime >= $holidaySeasonStart && $currentTime <= $holidaySeasonEnd): ?> 
                                        class="active"
                                    <?php endif ?>
                                >HOLIDAY</button>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 0px;">
                    <?php if( have_rows('pre-season') ): ?>
                        <?php while ( have_rows('pre-season') ) : the_row(); ?>
                            <div class="col-md-4 seasons text-center pre-season 
                            <?php if (get_sub_field('name') == 'DONUT RINGS'): ?>
                                donut-rings
                            <?php endif ?>">
                                <h3 class="name"><?php the_sub_field('name'); ?></h3>
                                <div class="timings">
                                    <?php the_sub_field('timing'); ?>
                                </div>
                                <h3><a href="tel:<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></a></h3>
                                <p class="email">email: <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
                                <!-- <?php if (get_sub_field('name') == 'CENTRAL' || get_sub_field('name') == 'SOUTH'): ?>
                                    <?php if (date("l") == 'Friday'): ?>
                                        <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                                    <?php else: ?>
                                        <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                    <?php endif ?>
                                <?php else : ?>
                                    <img style="<?php echo (get_sub_field('status') == 'open') ? 'margin-right: -1px;' : ''; ?>" src="<?php echo (get_sub_field('status') == 'closed') ? get_bloginfo('template_url').'/assets/images/closed-today.png' : get_bloginfo('template_url').'/assets/images/Open.gif'  ?>">
                                <?php endif ?> -->
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if( have_rows('in-season') ): ?>
                        <?php while ( have_rows('in-season') ) : the_row(); ?>
                            <div class="col-md-4 seasons text-center in-season 
                            <?php if (get_sub_field('name') == 'DONUT RINGS'): ?>
                                donut-rings
                            <?php endif ?>">
                                <h3 class="name"><?php the_sub_field('name'); ?></h3>
                                <div class="timings">
                                    <?php the_sub_field('timing'); ?>
                                </div>
                                <h3><a href="tel:<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></a></h3>
                                <p class="email">email: <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
                                <!-- <img style="<?php echo (get_sub_field('status') == 'open') ? 'margin-right: -1px;' : ''; ?>" src="<?php echo (get_sub_field('status') == 'closed') ? get_bloginfo('template_url').'/assets/images/closed-today.png' : get_bloginfo('template_url').'/assets/images/Open.gif'  ?>"> -->
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if( have_rows('post-season') ): ?>
                        <?php while ( have_rows('post-season') ) : the_row(); ?>
                            <div class="col-md-4 seasons text-center post-season 
                            <?php if (get_sub_field('name') == 'DONUT RINGS'): ?>
                                donut-rings
                            <?php endif ?>">
                                <h3 class="name"><?php the_sub_field('name'); ?></h3>
                                <div class="timings">
                                    <?php the_sub_field('timing'); ?>
                                </div>
                                <h3><a href="tel:<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></a></h3>
                                <p class="email">email: <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
                                <!-- <?php if (get_sub_field('name') == 'CENTRAL' || get_sub_field('name') == 'SOUTH'): ?>
                                    <?php if (date("l") == 'Friday'): ?>
                                        <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                                    <?php else: ?>
                                        <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                    <?php endif ?>
                                <?php else : ?>
                                    <img style="<?php echo (get_sub_field('status') == 'open') ? 'margin-right: -1px;' : ''; ?>" src="<?php echo (get_sub_field('status') == 'closed') ? get_bloginfo('template_url').'/assets/images/closed-today.png' : get_bloginfo('template_url').'/assets/images/Open.gif'  ?>">
                                <?php endif ?> -->
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if( have_rows('off-season') ): ?>
                        <?php while ( have_rows('off-season') ) : the_row(); ?>
                            <div class="col-md-4 seasons text-center off-season 
                            <?php if (get_sub_field('name') == 'DONUT RINGS'): ?>
                                donut-rings
                            <?php endif ?>">
                                <h3 class="name"><?php the_sub_field('name'); ?></h3>
                                <div class="timings">
                                    <?php the_sub_field('timing'); ?>
                                </div>
                                <h3><a href="tel:<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></a></h3>
                                <p class="email">email: <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
                                <!-- <?php if (get_sub_field('name') == 'CENTRAL'): ?>
                                    <?php if (date("l") == 'Friday'): ?>
                                        <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                                    <?php else: ?>
                                        <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                    <?php endif ?>
                                <?php else : ?>
                                    <img style="<?php echo (get_sub_field('status') == 'open') ? 'margin-right: -1px;' : ''; ?>" src="<?php echo (get_sub_field('status') == 'closed') ? get_bloginfo('template_url').'/assets/images/closed-today.png' : get_bloginfo('template_url').'/assets/images/Open.gif'  ?>">
                                <?php endif ?> -->
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if( have_rows('holiday_hours') ): ?>
                        <?php while ( have_rows('holiday_hours') ) : the_row(); ?>
                            <div class="col-md-4 seasons text-center holidays-season 
                            <?php if (get_sub_field('Name') == 'DONUT RINGS'): ?>
                                donut-rings
                            <?php endif ?>">
                                <h3 class="name"><?php the_sub_field('Name'); ?></h3>
                                <div class="timings">
                                    <?php the_sub_field('timing'); ?>
                                </div>
                                <h3><a href="tel:<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></a></h3>
                                <p class="email">email: <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
                                <!-- <?php if (get_sub_field('Name') == 'CENTRAL'): ?>
                                <?php if (date("l") == 'Friday'): ?>
                                    <img style="" src="<?php echo get_bloginfo('template_url').'/assets/images/closed-today.png'; ?>">
                                <?php else : ?>
                                    <img style="margin-right: -1px" src="<?php echo get_bloginfo('template_url').'/assets/images/Open.gif'; ?>">
                                <?php endif ?>
                            <?php else : ?>
                                <img style="<?php echo (get_sub_field('status') == 'open') ? 'margin-right: -1px;' : ''; ?>" src="<?php echo (get_sub_field('status') == 'closed') ? get_bloginfo('template_url').'/assets/images/closed-today.png' : get_bloginfo('template_url').'/assets/images/Open.gif'  ?>">
                            <?php endif ?> -->
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </section>
    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>