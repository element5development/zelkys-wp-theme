<?php
/*
** Template Name: Our Prizes
*/
 get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
        <section class="inner small_container" style="margin: 50px 0 150px 0;">
	        <div class="container">
	        	<div class="row prizes_sec">
	                <?php if( have_rows('prizes_images') ): ?>
	                    <?php $num = 1; while ( have_rows('prizes_images') ) : the_row(); ?>
			        		<div class="col-md-4 single_image_p text-center">
			        			<div id="preload-image-<?php echo $num; ?>" style="background-image: url(<?php the_sub_field('image_hover'); ?>);"></div>
			        			<img src="<?php the_sub_field('image'); ?>" onmouseover="this.src='<?php the_sub_field('image_hover'); ?>'" onmouseout="this.src='<?php the_sub_field('image'); ?>'" class="img-responsive">
			        		</div>
	                    <?php $num++; endwhile; ?>
	                <?php endif; ?>
	        	</div>
	        </div>
        </section>

    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>