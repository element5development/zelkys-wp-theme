<footer class="footer">
    <div class="container text-center">
        <div class="row footer-menu">
            <div class="col-md-12">
                <div class="footer-logo">
                    <!--<img src="<?php /*echo ot_get_option('footer_logo'); */?>">-->
                    <!-- <img src="<?php bloginfo('template_url'); ?>/assets/images/footer-logo.svg"> -->
                    <img src="<?php echo ot_get_option('footer_logo'); ?>">
                </div>

                <ul>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu',
                        'container' => '',
                        'items_wrap' => '%3$s'
                    ));
                    ?>
                </ul>

                <div class="social-icons">
                    <a href="<?php echo ot_get_option('facebook'); ?>" target="_blank"><i
                                class="fa fa-facebook"></i></a>
                    <a href="<?php echo ot_get_option('instagram'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>

                <div class="copyrights">
                    <?php echo ot_get_option('copyrights_text'); ?>
                </div>

                <div class="rocket">
                    <span class="rocket-made-by">Made by </span>
                    <a target="_blank" class="rocket-effect" href="https://rocketeffect.com">Rocket Effect</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="mobile-nav">
    <div class="close-btn">
        <img src="<?php bloginfo('template_url'); ?>/assets/images/cross.png">
    </div>

    <figure class="text-center">
        <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/mobile-logo.svg"></a>
    </figure>

    <?php wp_nav_menu(array(
            'theme_location' => 'main-menu',
            'depth' => 2,
            'container' => 'div',
            'container_class' => 'my-nav-bar',
            'container_id' => 'myNavbar',
            'menu_class' => 'nav navbar-nav navbar-right',
            'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
            'walker' => new WP_Bootstrap_Navwalker()
    )); ?>
</div>

<script src="<?php bloginfo('template_url'); ?>/assets/js/lib.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/isotope.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/parallax.js"></script>

<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="<?php bloginfo('template_url'); ?>/assets/js/main.js"></script>

<?php
$south_phone = ot_get_option('south_phone');
$central_phone = ot_get_option('central_phone');
$north_phone = ot_get_option('north_phone');
$south_address = ot_get_option('south_address');
$central_address = ot_get_option('central_address');
$north_address = ot_get_option('north_address');
$south_map_link = ot_get_option('south_map_link');
$central_map_link = ot_get_option('central_map_link');
$north_map_link = ot_get_option('north_map_link');
?>

<?php if (WC()->cart->get_cart_contents_count()): ?>
    <script type="text/javascript">
        jQuery(function($){
            $('#menu-main-menu li.phone').before('<li class="container-pop-up-card container-pop-up-card2"><a class="cart-customlocation card-btn" href="<?php echo wc_get_cart_url(); ?>"><span class="quantity"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a></li>');
            
//          $(document).on('click', '.product-remove .remove, button[name="update_cart"]', function(){
//              setTimeout(function(){
//                  alert();
//              var data = {
//                'action': 'cart_count_retriever'
//              };
//              jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(response) {alert(response);
//                  if(response == 0) {
//                      alert(response);
//              $('li.container-pop-up-card.container-pop-up-card2').remove();      
                  
//                  } else {
//              $('li.container-pop-up-card.container-pop-up-card2 .cart-customlocation.card-btn .quantity').html(response);        
                  
//                  }
//              });
//              }, 1500);
//          });
        });
    </script>
<?php endif ?>

<script type="text/javascript">
                
            $(document).on('click', '.woocommerce-shipping-calculator .shipping-calculator-button', function(){
                window.location.href = "<?php echo get_bloginfo('url'); ?>/checkout";
            });
    /* Animation after video banner on home page
 -------------------------------------------------------------------------------------------------- */
var $element = jQuery("#home-animation");
var imgPath = "<?php bloginfo('template_url'); ?>" + "/assets/images/";

var wordS = '';
if (jQuery(window).width() < 560) {
    var curlin = jQuery('#home-animation').attr('src');
    if (curlin) {
        var newlin = curlin.replace('Step1', 'StepM1');
        jQuery('#home-animation').attr('src', newlin);
    }
    var wordS = 'M';
}

var animationDuration = 5000;
var totalFrames = 9;
var timePerFrame = animationDuration / totalFrames;
var timeWhenLastUpdate = void 0;
var timeFromLastUpdate = void 0;
var frameNumber = 1;

function step(startTime) {
    if (!timeWhenLastUpdate) timeWhenLastUpdate = startTime;
    timeFromLastUpdate = startTime - timeWhenLastUpdate;

    if (timeFromLastUpdate > timePerFrame) {
        $element.attr('src', imgPath + ('Step' + wordS + frameNumber + '.png'));
        timeWhenLastUpdate = startTime;

        if (frameNumber >= totalFrames) {
            frameNumber = 1;
        } else {
            frameNumber = frameNumber + 1;
        }
    }

    requestAnimationFrame(step);
}

jQuery(document).ready(function () {
    for (var i = 1; i < totalFrames + 1; i++) {
        jQuery('body').append('<div id="preload-image-' + i + '" style="background-image: url(\'' + imgPath + 'Step' + wordS + i + '.png\');"></div>');
    }
});

jQuery(window).on('load', function () {
    requestAnimationFrame(step);
});

/* bx-sliders
 -------------------------------------------------------------------------------------------------- */
var maxSlidesGallerySlider,
    maxSlidesTestiSlider,
    width = $(window).width();

if (width < 600) {
    maxSlidesGallerySlider = 2;
} else {
    maxSlidesGallerySlider = 4;
}

if (width < 1030) {
    maxSlidesTestiSlider = 2;
} else {
    maxSlidesTestiSlider = 3;
}

jQuery(function () {
    $('.testi-slider').bxSlider({
        slideWidth: 330,
        minSlides: 1,
        maxSlides: maxSlidesTestiSlider,
        slideMargin: 20,
        pager: false
    });

    $('.gallery-slider').bxSlider({
        slideWidth: 230,
        minSlides: 2,
        maxSlides: maxSlidesGallerySlider,
        slideMargin: 20,
        pager: false,
        touchEnabled: false
    });

    $('.get_all_images_slider').bxSlider({
        slideWidth: 124,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10,
        controls: true,
        pager: false
    });

    $('.games-slider').bxSlider({
        pager: false,
        controls: true,
        infiniteLoop: false,
        touchEnabled: false
    });

    $('.testimonial-int-slider').bxSlider({
        pager: false,
        controls: true,
        infiniteLoop: false
    });


    //var $grid = initGallery();

    $(window).on("load", function () {
        function initGallery() {
            return $('.grid').isotope({
                itemSelector: '.element-item',
                masonry: {
                    columnWidth: 0
                }
            });
        }

        function loadGallery($button) {
            var text = $button.text();
            var page = $button.data("page");
            var count = $button.attr('data-count');
            var gallery = $button.attr('data-gallery');
            console.log(gallery);
            $button.text("Wait...");
            $button.attr('disabled', 'disabled');

            var gallery_data = {
                action: 'gallery_ajax',
                page: page,
                count: count,
                gallery: gallery
            };
            jQuery.post("/wp-admin/admin-ajax.php", gallery_data, function (response) {

                $('.grid').isotope('insert', $(response.data.page));

                //$('.grid').append( $(response.data.page) ).isotope( 'addItems', $(response.data.page) );
                //$('.grid').isotope( 'layout', null );
                $button.data("page", ++page);

                // setTimeout(function () {
                //     $('#newGallery').click();
                // }, 300);

                if (response.data.last_page == true) {
                    $button.hide();
                }

                console.log(response);
            }).always(function () {
                $button.text(text);
                $button.removeAttr('disabled');
            }).fail(function () {
                alert("error");
            });
        }

        var $grid = initGallery();

        $(document).on('click', '.btn-show-more', function (e) {
            e.preventDefault();

            loadGallery($(this));
                
            setInterval(function(){
                $('.main-gallery .btn-tab.active').click();
            }, 1000);
        });
        
        $('.btn-show-more').attr('data-gallery', $('.btn-tab.active').data('filter').replace('.', ''));
        $('.btn-show-more').attr('data-count', $('.grid > div:visible').length);

        
        $('.filter-button-group .btn-tab').click(function () {
            if ($(this).data('filter') == '*') {
                $('.btn-show-more').hide();
            }
            $('.filter-button-group .btn-tab').removeClass('active');
            $(this).addClass('active');
            $grid.isotope({filter: $(this).data('filter')});
            $('.btn-show-more').removeAttr('data-gallery');
            $('.btn-show-more').removeAttr('data-count');
            $('.btn-show-more').attr('data-gallery', $(this).data('filter').replace('.', ''));
            setTimeout(function(){
                $('.btn-show-more').attr('data-count', $('.grid > div:visible').length);
                if($('.grid > div:visible').length == 3 && $(this).data('filter') != '*') {
                   $('.btn-show-more').show();
                } else {
                    $('.btn-show-more').hide();
                }
            }, 500);
            $('.grid > div').each(function () {
                $('.grid > div[id="' + this.id + '"]:gt(0)').remove();
            });
        });
    });


    /*setTimeout(function () {
     $('#newGallery').click();
     }, 500);*/

    /*let numberGalleyItems = 12;

     function latestPhotos(numberGalleyItems) {
     var images_gallery = $('.main-gallery .element-item');
     var array_images = [];
     var count = numberGalleyItems + $('.main-gallery .ready_array').length;

     for (var i = 0; i < images_gallery.length; i++) {

     var date_val = new Date(images_gallery.eq(i).attr('data-created'));
     var month = (date_val.getMonth() + 1);
     if (month < 10) {
     month = '0' + month;
     }

     var hours = date_val.getHours();
     if (hours < 10) {
     hours = '0' + hours;
     }

     var minutes = date_val.getMinutes();
     if (minutes < 10) {
     minutes = '0' + minutes;
     }

     var seconds = date_val.getSeconds();
     if (seconds < 10) {
     seconds = '0' + seconds;
     }

     array_images[i] = date_val.getFullYear() + '-' + month + '-' + date_val.getDate() + ' ' + hours + ':' + minutes + ':' + seconds;
     }
     array_images = array_images.sort();

     for (j = 0; j < count; j++) {
     $('.main-gallery .element-item[data-created="' + array_images[j] + '"]').addClass('ready_array');
     }

     $grid.isotope({filter: '.ready_array'});

     if (count >= $('.main-gallery .element-item').length) {
     //$('.btn-show-more').hide();
     }
     }

     // filter gallery items on button click
     $('.filter-button-group').on('click', 'button', function () {
     var filterValue = $(this).attr('data-filter');

     if (filterValue !== '*') {
     $grid.isotope({filter: filterValue});
     } else {
     latestPhotos(numberGalleyItems);
     }

     $('button').removeClass('active');
     $(this).addClass('active');
     });*/

    // show more
    /*$('.btn-show-more').on('click', function () {
     latestPhotos(numberGalleyItems);
     });*/

    $('.game-vertical').bxSlider({
        mode: 'vertical',
        minSlides: 9,
        slideMargin: 10,
        auto: false,
        moveSlides: 1,
        controls: true,
        pager: false,
        infiniteLoop: false,
        hideControlOnEnd: true,
        touchEnabled: false
    });

    $('.timingFilter button').click(function () {
        $('.timingFilter button').removeClass('active');
        $(this).addClass('active');
        $('.seasons:not(.dont)').hide();
        var abc = $(this).attr('data-filter');
        $(abc + '-season:not(.dont)').show();
        // $('.seasons-text').html(abc.replace('.','')+'-season');
    });

    $('.parallax-window').parallax({
        speed: 0.8,
    });

    /* MAP ITEMS IN HEADER
     -------------------------------------------------------------------------------------------------- */
    $('.north-map a').click(function () {
        $('.nav-pills>li.active-north').addClass('active');
        $('.nav-pills>li.active-central').removeClass('active');
        $('.nav-pills>li.active-south').removeClass('active');
    });

    $('.central-map a').click(function () {
        $('.nav-pills>li.active-central').addClass('active');
        $('.nav-pills>li.active-north').removeClass('active');
        $('.nav-pills>li.active-south').removeClass('active');
    });

    $('.south-map a').click(function () {
        $('.nav-pills>li.active-south').addClass('active');
        $('.nav-pills>li.active-north').removeClass('active');
        $('.nav-pills>li.active-central').removeClass('active');
    });

    $('#my-nav').click(function () {
        $('.mobile-nav').fadeIn(400);
    });

    $('.close-btn').click(function () {
        $(this).parent('.mobile-nav').fadeOut(400);
    });

    $('#menu-item-75').html('<a title href="tel:<?php echo $south_phone; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link southAdd" id="menu-item-dropdown-75"><i class="phone-icon fa fa-phone"></i><font><?php echo $south_phone; ?></font></a> <a  href="tel:<?php echo $central_phone; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link centralAdd" id="menu-item-dropdown-75"><i class="phone-icon fa fa-phone"></i><font><?php echo $central_phone;; ?></font></a> <a href="tel:<?php echo $north_phone; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link northAdd" id="menu-item-dropdown-75"><i class="phone-icon fa fa-phone"></i><font><?php echo $north_phone; ?></font></a> <ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-75" role="menu"> <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-391" class="southAdd menu-item menu-item-type-custom menu-item-object-custom menu-item-391 nav-item" style="display: block;"><a target="_blank" href="<?php echo $south_map_link; ?>" class="dropdown-item"><div class="southAdd" style="display: block;"><table><tbody><tr><td>South address:</td><td><?php echo $south_address; ?></td></tr></tbody></table></div></a></li> <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-392" class="centralAdd menu-item menu-item-type-custom menu-item-object-custom menu-item-392 nav-item" style="display: none;"><a target="_blank" href="<?php echo $central_map_link; ?>" class="dropdown-item"><div class="centralAdd" style="display: none;"><table><tbody><tr><td>Central address:</td><td><?php echo $central_address; ?></td></tr></tbody></table></div></a></li> <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-393" class="northAdd menu-item menu-item-type-custom menu-item-object-custom menu-item-393 nav-item" style="display: none;"><a target="_blank" href="<?php echo $north_map_link; ?>" class="dropdown-item"><div class="northAdd" style="display: none;"><table><tbody><tr><td>North address:</td><td><?php echo $north_address; ?></td></tr></tbody></table></div></a></li> </ul>');

    $('.southAdd').show();
    $('.centralAdd').hide();
    $('.northAdd').hide();

    $('#menu-item-75 > a').on('click', function(){
        window.location.href = $(this).attr('href');
    });

    $('.locations ul li a').click(function () {
        $('.locations ul li a').removeClass('active');
        var htmlLo = $(this).html();
        $('#menu-item-75 > a font').html($(this).attr('dataCall'));
        $('#menu-item-75 > a').attr('href', 'tel:' + $(this).attr('dataCall'));
        if (htmlLo == 'NORTH') {
            $('.southAdd').hide();
            $('.centralAdd').hide();
            $('.northAdd').show();
        }
        if (htmlLo == 'SOUTH') {
            $('.southAdd').show();
            $('.centralAdd').hide();
            $('.northAdd').hide();
        }
        if (htmlLo == 'CENTRAL') {
            $('.southAdd').hide();
            $('.centralAdd').show();
            $('.northAdd').hide();
        }
        $(this).addClass('active');
    });

    $('.locationButton').click(function () {
        if ($(this).find('.dropdown-menu').is(":visible")) {
            $(this).find('.dropdown-menu').hide();
            $('.locationButton').removeClass('itsCurrent');
        } else {
            $('.locationButton .dropdown-menu').hide();
            $('.locationButton').removeClass('itsCurrent');
            $(this).find('.dropdown-menu').show();
            $(this).addClass('itsCurrent');
        }
    });

    $('li#menu-item-637[style="display: block !important;"]').attr('style', 'display: inline-block !important;');
    $('li#menu-item-714[style="display: block !important;"]').attr('style', 'display: inline-block !important;');

    if ($(window).width() < 800) {
        var abc = parseInt($(window).width() / 2)
        if (abc > 300) {
            var abc = parseInt(100);
        }
        $('.tab-content .tab-pane').scrollLeft(abc)
    }

});
</script>

<?php wp_footer(); ?>

</body>
</html>