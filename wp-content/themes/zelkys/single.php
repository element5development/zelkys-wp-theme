<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="container post_content">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<h3><?php the_title(); ?></h3>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	            	<?php the_content(); ?>
	        		</article>
				</div>
			</div>
        </div>
    <?php endwhile; endif; ?>

<?php get_footer(); ?>