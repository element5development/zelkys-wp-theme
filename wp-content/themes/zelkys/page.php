<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
        <section class="inner" style="margin: 50px 0 30px 0;">
	        <div class="container">
	        	<div class="row">
	        		<div class="col-md-12">
	        			<?php the_content(); ?>
	        		</div>
	        	</div>
	        </div>
        </section>
<style>
	body .game-point-component .profile-component .form-group:after {
		display: none !important;
	}
</style>
    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>