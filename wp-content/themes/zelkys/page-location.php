<?php
/* Template Name: Locations */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
            <section class="main-gallery location_sec">
                <div class="container">
                    <div class="row">
                    <?php if( have_rows('our_locations') ): ?>
                        <?php while ( have_rows('our_locations') ) : the_row(); ?>
                            <?php if(get_sub_field('name') != 'DONUT RINGS') { ?>
                                <div class="col-md-4 text-center">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/location-icon.png">
                                    <h3><?php the_sub_field('name'); ?></h3>
                                    <p><?php the_sub_field('address'); ?></p>
                                    <strong><a href="tel:<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></a></strong>
                                    <div class="dropdown locationButton">
                                        <button class="btn dropdown_text dropdown-toggle" type="button">HOW TO GET TO US
                                        <i class="fa fa-chevron-down"></i><i class="fa fa-chevron-up"></i></button>
                                        <div class="dropdown-menu">
                                            <?php the_sub_field('map_embed'); ?>
                                            <!-- <a href="https://maps.google.com/?q=<?php echo str_replace('<br>', ' ', get_sub_field('address'));  ?>" target="_blank"><img src="<?php the_sub_field('map_image'); ?>" class="img-responsive"></a> -->
                                        </div>
                                    </div>
                                    <img src="<?php the_sub_field('location_image'); ?>" class="img-responsive">
                                    <?php if(get_sub_field('name') == 'CENTRAL') { ?>
                                        <?php $our_locations = get_field('our_locations'); ?>
                                            <div class="text-center">
                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/location-icon.png">
                                                <h3><?php echo $our_locations[3]['name']; ?></h3>
                                                <p><?php echo $our_locations[3]['address']; ?></p>
                                                <strong><?php echo $our_locations[3]['number']; ?></strong>
                                                <div class="dropdown locationButton">
                                                    <button class="btn dropdown_text dropdown-toggle" type="button">HOW TO GET TO US
                                                    <i class="fa fa-chevron-down"></i><i class="fa fa-chevron-up"></i></button>
                                                    <div class="dropdown-menu">
                                                        <?php echo $our_locations[3]['map_embed']; ?>
                                                    </div>
                                                </div>
                                                <img src="<?php echo $our_locations[3]['location_image']; ?>" class="img-responsive">
                                            </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    </div>
                </div>
            </section>
    <?php endwhile; endif; ?>
<style>
    .fa-chevron-up {
        display: none;
    }
    .itsCurrent .fa-chevron-down {
        display: none;
    }
    .itsCurrent .fa-chevron-up {
        display: inline-block;
    }
</style>
<?php get_footer(); ?>