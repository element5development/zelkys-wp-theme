<?php
/* Template Name: Staff */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
        <?php get_template_part( 'template-parts/fancy', 'title' ); ?>

        <section class="price text-center">
              <div class="container">
                    <div class="row">
                          <div class="col-md-12">
                                <div class="section_heading">
                                    <h3><?php the_field('wwa_heading'); ?></h3> 
                                </div>
                          </div>
                    </div>
              </div>
        </section>

        <section class="tabs inner">
            <div class="container">
                <?php if (get_field('wwa_image')): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <center><img src="<?php echo get_field('wwa_image'); ?>" style="margin-top: -71px;" class="img-responsive"></center>
                        </div>
                    </div>
                <?php endif ?>
                <?php if (get_field('wwa_content')): ?>
                <div class="row">
                    <div class="col-md-12 price-sec big_text">
                        <?php the_field('wwa_content') ?>    
                    </div>
                </div>
                <?php endif ?>
            </div>
        </section>

    <section class="text-center staff_sec">
          <div class="container">
                <div class="price-sec">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/staff-icon.png">
                    <h3>STAFF</h3>
					<br><br>
                </div>

                <div class="staff_i">
                <?php if( have_rows('staff_members') ): ?>
                    <?php $num = 1; while ( have_rows('staff_members') ) : the_row(); ?>
                    <?php if ($num == 1): ?>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <img src="<?php echo (get_sub_field('image')) ? get_sub_field('image') : get_bloginfo('template_url').'/assets/images/staff-placeholder.png'; ?>" class="img-responsive">
                                <h4><?php the_sub_field('name'); ?></h4>
                                <h5><?php the_sub_field('title'); ?></h5>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    <?php elseif ($num == 2 || $num == 3): ?>
                        <?php if ($num == 2): ?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="row">
                                <?php endif ?>
                                    <div class="col-md-6">
                                        <img src="<?php echo (get_sub_field('image')) ? get_sub_field('image') : get_bloginfo('template_url').'/assets/images/staff-placeholder.png'; ?>" class="img-responsive">
                                        <h4><?php the_sub_field('name'); ?></h4>
                                        <h5><?php the_sub_field('title'); ?></h5>
                                    </div>
                                <?php if ($num == 3): ?>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <?php endif ?>
                        <?php else : ?>
                        <?php //if ($num == 10) echo 'col-md-offset-2'; ?>
                        <div class="col-md-4">
                            <img src="<?php echo (get_sub_field('image')) ? get_sub_field('image') : get_bloginfo('template_url').'/assets/images/staff-placeholder.png'; ?>" class="img-responsive">
                            <h4><?php the_sub_field('name'); ?></h4>
                            <h5><?php the_sub_field('title'); ?></h5>
                        </div>
                    <?php endif ?>
                    <?php $num++; endwhile; ?>
                <?php endif; ?>
                </div>
          </div>
    </section>

        <?php get_template_part( 'template-parts/section', 'buttons' ); ?>

    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>