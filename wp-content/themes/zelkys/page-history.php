<?php
/* Template Name: History */
get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
        <?php get_template_part( 'template-parts/fancy', 'title' ); ?>

        <section class="price text-center">
              <div class="container">
                    <div class="row">
                          <div class="col-md-12">
                                <div class="section_heading">
                                    <h3><?php the_field('first_section_heading'); ?></h3> 
                                </div>
                          </div>
                    </div>
              </div>
        </section>

        <section class="tabs inner first xscontent" style="display: block;">
            <div class="container">
                <?php if (get_field('first_section_image')): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <center><img src="<?php echo get_field('first_section_image'); ?>" style="margin-top: -71px;" class="img-responsive"></center>
                        </div>
                    </div>
                <?php endif ?>
                <?php //if (get_field('wwa_content')): ?>
                <div class="row small_container">
                    <div class="col-md-12 price-sec big_text" style="padding-bottom: 0;">
						<div class="row">
							<div class="col-xs-5">
								<img class="alignleft wp-image-520 " style="margin-top: 25px; margin-bottom: 25px;" src="<?php the_field('first_section_left_image'); ?>" alt="" width="302" height="195">
							</div>
							<div class="col-xs-7">
								<?php the_field('first_section_left_image_content'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?php the_field('first_section_content'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-5 images2 hideDesktopSec">
								<img class="alignright wp-image-520 " src="<?php the_field('first_section_right_image'); ?>" alt="" width="302" height="195">
								<img class="alignright wp-image-520 " src="<?php the_field('first_section_right_image_2'); ?>" alt="" width="302" height="195">
							</div>
							<div class="col-xs-7">
								<?php the_field('first_section_right_image_content'); ?>
							</div>
							<div class="col-xs-5 images2 showDesktopSec">
								<img class="alignright wp-image-520 " src="<?php the_field('first_section_right_image'); ?>" alt="" width="302" height="195">
								<img class="alignright wp-image-520 " src="<?php the_field('first_section_right_image_2'); ?>" alt="" width="302" height="195">
							</div>
						</div>
                    </div>
                </div>
                <?php //endif ?>
            </div>
        </section>

        <section class="price text-center">
              <div class="container">
                    <div class="row">
                          <div class="col-xs-12">
                                <div class="section_heading">
                                    <h3 style="margin-top: 0;"><?php the_field('second_section_heading'); ?></h3> 
                                </div>
                          </div>
                    </div>
              </div>
        </section>

        <section class="tabs inner xscontent">
            <div class="container">
                <?php if (get_field('second_section_image')): ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <center><img src="<?php echo get_field('second_section_image'); ?>" style="margin-top: -71px;" class="img-responsive"></center>
                        </div>
                    </div>
                <?php endif ?>
                <div class="row small_container">
                    <div class="col-md-12 price-sec big_text" style="padding-bottom: 0;">
						<div class="row">
							<div class="col-xs-5">
								<img class="alignleft wp-image-520 " style="margin-top: 30px; margin-bottom: 40px;" src="<?php the_field('second_section_left_image'); ?>" alt="" width="302" height="195">
							</div>
							<div class="col-xs-7">
								<?php the_field('second_section_left_image_content'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?php the_field('second_section_content'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-7">
								<?php the_field('second_section_right_image_content'); ?>
							</div>
							<div class="col-xs-5">
								<img class="alignright wp-image-520 " style="margin-left: 80px; margin-right: 80px; margin-bottom: 30px;" src="<?php the_field('second_section_right_image'); ?>" alt="" width="226" height="181">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-5">
								<img class="alignleft wp-image-520 " style="margin-top: 25px; margin-bottom: 25px;" src="<?php the_field('second_section_left_image_2'); ?>" alt="" width="302" height="195">
							</div>
							<div class="col-xs-7">
								<?php the_field('second_section_left_image_content_2'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<?php the_field('second_section_content_2'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-7">
								<?php the_field('second_section_right_image_content_2'); ?>
							</div>
							<div class="col-xs-5">
								<img class="alignright wp-image-520 " src="<?php the_field('second_section_right_image_2'); ?>" alt="" width="302" height="195">
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="price button_sec">
            <div class="container small_container">
                <div class="row price-sec big_text" style="padding: 30px 110px;">
                    <?php echo (get_field('h_heading')) ? '<h4 style="text-align: center; font-size: 34px;">'.get_field('h_heading').'</h4>' : ''; ?>
                  <div class="col-md-4" style="margin-bottom: 40px;">
                    <img src="<?php the_field(h_image); ?>" class="img-responsive makeCenterTab">
                  </div>
                  <div class="col-md-8">
                  </div>
                    <?php the_field('h_content'); ?>
                </div>
            </div>
        </section>
    <?php endwhile; endif; ?>
    
    <style type="text/css">
    @media (max-width: 620px) {
        .xscontent .col-xs-1, .xscontent .col-xs-10, .xscontent .col-xs-11, .xscontent .col-xs-12, .xscontent .col-xs-2, .xscontent .col-xs-3, .xscontent .col-xs-4, .xscontent .col-xs-5, .xscontent .col-xs-6, .xscontent .col-xs-7, .xscontent .col-xs-8, .xscontent .col-xs-9 {
            width: 100%;
        }
        .xscontent img {
            /*width: 100%;*/
            max-width: 100%;
            float: none;
            display: block;
            margin: 10px auto 10px auto !important;
        }
    }
    </style>

<?php get_footer(); ?>