<?php
/* Template Name: Who We Are */
get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>
        <?php get_template_part( 'template-parts/fancy', 'title' ); ?>

        <section class="price text-center">
              <div class="container">
                    <div class="row">
                          <div class="col-md-12">
                                <div class="section_heading">
                                    <h3><?php the_field('wwa_heading'); ?></h3> 
                                </div>
                          </div>
                    </div>
              </div>
        </section>

        <section class="tabs inner text-center">
            <div class="container">
                <?php if (get_field('wwa_image')): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <img src="<?php echo get_field('wwa_image'); ?>" style="margin-top: -71px;">
                        </div>
                    </div>
                <?php endif ?>
                <div class="row small_container">
                    <div class="col-md-12 price-sec big_text">
                        <?php the_field('wwa_content') ?>    
                    </div>
                </div>
            </div>
        </section>
        <?php get_template_part( 'template-parts/section', 'buttons' ); ?>
    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>