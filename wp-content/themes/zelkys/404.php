<?php get_header(); ?>

<style type="text/css">
div.logoText {
	margin: 0 auto;
}
div.bannerAreaFront {
	height: inherit;
	min-height: inherit;
	background: rgba(20,82,110,0.9);
}
section.bannerAreaBack {
	min-height: inherit;
}
</style>

<h1 style="text-align: center; margin-top:40px;">404 - Not Found</h1>
<?php get_footer(); ?>