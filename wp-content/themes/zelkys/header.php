<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="skLkCxk1_V_i5hLTAZRJnd4nFblQ9RfdTVn0CFSPUAw" />


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . '/style_woo.css'; ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if (is_user_logged_in()): ?>
	<style type="text/css">
	@media (min-width: 768px) {
		.main-wrapper > header {
/* 			top: 30px; */
		}
	}
	</style>
<?php endif ?>
<?php //if (is_page('coming-soon')) { ?>
	<style type="text/css">
	@media (min-width: 769px) {
		.main-wrapper {
			position: relative;
			min-height: 100%;
		}
	}
	</style>
	
<?php if (is_page('hot-games')) { ?>
	<style type="text/css">
	@media (min-width: 767px) and (max-width: 768px) {
		.main-wrapper {
			position: relative;
			min-height: 100%;
		}
		body, html {
			height: 100%;
		}
	}
	</style>
<?php } ?>
<?php //} ?>
<div class="main-wrapper">	
	<header class="desktop header-desktop">
		<div class="container header top-header">
			<div class="row">
				<div class="col-md-12 cleafix">
					<div class="logo pull-left">
						<a href="<?php echo site_url(); ?>">
							<img src="<?php echo (ot_get_option('header_logo')) ? ot_get_option('header_logo') : get_bloginfo('template_url').'/assets/images/logo.svg'; ?>">
						</a>
					</div>
					<div class="locations pull-right">
						<ul>
							<li><a href="javascript:void(0);" dataCall="<?php echo ot_get_option('north_phone'); ?>">NORTH</a></li>
							<li><a href="javascript:void(0);" dataCall="<?php echo ot_get_option('central_phone'); ?>">CENTRAL</a></li>
							<li><a class="active" href="javascript:void(0);" dataCall="<?php echo ot_get_option('south_phone'); ?>">SOUTH</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar">
			<div class="container header">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
				</div>

		        <?php
		        wp_nav_menu( array(
		            'theme_location'    => 'main-menu',
		            'depth'             => 2,
		            'container'         => 'div',
		            'container_class'   => 'collapse navbar-collapse',
		            'container_id'      => 'bs-example-navbar-collapse-1',
		            'menu_class'        => 'nav navbar-nav navbar-right',
		            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		            'walker'            => new WP_Bootstrap_Navwalker()
				) );
		        ?>
			</div>
		</nav>
	</header>
	<header class="mobile header-mobile">
		<div class="container header top-header">
			<div class="row">
				<div class="col-md-12 cleafix">
					<div class="logo pull-left">
						<a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo.svg"></a>
					</div>
					<div class="locations pull-right">
						<ul class="desktop header-desktop">
							<li><a href="javascript:void(0);" dataCall="<?php echo ot_get_option('north_phone'); ?>">NORTH</a></li>
							<li><a href="javascript:void(0);" dataCall="<?php echo ot_get_option('central_phone'); ?>">CENTRAL</a></li>
							<li><a class="active" href="javascript:void(0);" dataCall="<?php echo ot_get_option('south_phone'); ?>">SOUTH</a></li>
						</ul>
						<ul class="mobile header-mobile">
							<!-- <li><a href="<?php bloginfo('url'); ?>/sign-in/">sign in <img src="<?php bloginfo('template_url'); ?>/assets/images/signin.png"> </a></li>  -->
						</ul>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar">
			<div class="container header">
				<div class="navbar-header">
					<li class="phone mobile mobile-phone header-mobile locationButton">
						<a href="javascript:void(0);" class="dropdown-toggle"><i class="phone-icon fa fa-phone"></i></a>
						<div class="dropdown-menu pull-right" style="width: 120px; min-width: 120px;">
							<ul style="padding: 10px 20px; margin: 0; text-align: right; list-style: none;">
								<li><a href="tel:<?php echo ot_get_option('north_phone'); ?>">NORTH</a></li>
								<li><a href="tel:<?php echo ot_get_option('central_phone'); ?>">CENTRAL</a></li>
								<li><a href="tel:<?php echo ot_get_option('south_phone'); ?>">SOUTH</a></li>
							</ul>
						</div>
					</li>
					<button type="button" class="navbar-toggle" id="my-nav">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/mobile-burger.png">
					</button>
				</div>
			</div>
		</nav>
	</header>