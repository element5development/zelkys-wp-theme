<?php
/* Template Name: International Student Program */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
        <?php get_template_part( 'template-parts/inner', 'header' ); ?>

        <section class="price text-center">
              <div class="container">
                    <div class="row">
                          <div class="col-md-12">
                                <div class="section_heading">
                                    <h3><?php the_field('wwa_heading'); ?></h3> 
                                </div>
                          </div>
                    </div>
              </div>
        </section>

        <section class="tabs inner first">
            <div class="container">
                <?php if (get_field('wwa_image')): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <center><img src="<?php echo get_field('wwa_image'); ?>" style="margin-top: -71px;" class="img-responsive"></center>
                        </div>
                    </div>
                <?php endif ?>
                <?php if (get_field('wwa_content')): ?>
                <div class="row small_container">
                    <div class="col-md-12 price-sec big_text">
                        <?php the_field('wwa_content') ?>    
                    </div>
                </div>
                <?php endif ?>
            </div>
        </section>

	   	<section class="price text-center button_sec" style="display: block;">
	      	<div class="container">
	            <div class="price-sec" style="padding-top: 0;">
	            	<div class="row small_container">
	            		<div class="col-md-12 big_text">
	            			<div class="text-center">
		            			<h1 class="background_colored_heading"><?php the_field('background_colored_heading'); ?></h1>
	            			</div>
	            			<?php the_field('isp_content'); ?>
	            		</div>
	            	</div>
	            </div>
	      	</div>
	    </section>

	    <section class="int_testi_sec small_container">
	    	<div class="container">
	    		<ul class="testimonial-int-slider">
	            <?php if( have_rows('testimonial') ): ?>
	                <?php $num = 1; while ( have_rows('testimonial') ) : the_row(); ?>
	                <li>
			    		<div class="row">
			    			<div class="col-sm-6">
			    				<img src="<?php the_sub_field('image'); ?>"> <br>	
			    				<!-- <div class="testiIcons">
		    						<img src="<?php bloginfo('template_url') ?>/assets/images/small.png">
		    						<img src="<?php bloginfo('template_url') ?>/assets/images/small.png">
		    						<img src="<?php bloginfo('template_url') ?>/assets/images/small.png">
		    						<img src="<?php bloginfo('template_url') ?>/assets/images/small.png">
		    						<img src="<?php bloginfo('template_url') ?>/assets/images/small.png">
			    				</div> -->
			    			</div>
			    			<div class="col-sm-6">
			    				<h2><?php the_sub_field('name'); ?></h2>
			    				<h3><?php the_sub_field('title'); ?></h3>
			    				<p><?php the_sub_field('content'); ?></p>
			    			</div>
			    		</div>
	                </li>
	                <?php $num++; endwhile; ?>
	            <?php endif; ?>
	    		</ul>
	    	</div>
	    </section>

	   	<section class="price text-center button_sec" style="display: block; border-top: 10px solid #0a3747;">
	      	<div class="container">
	            <div class="price-sec small_container intbtn sideArrow">
	            	<div class="row">
	            		<div class="col-sm-6 text-left fullonMobile">
			                <?php echo (get_field('work_heading')) ? '<h4 style="font-weight: 900; margin-bottom: 20px;">'.get_field('work_heading').'</h4>' : ''; ?>
			                <?php echo (get_field('work_content')) ? '<p>'.get_field('work_content').'</p>' : ''; ?>
			                <?php if (get_field('work_button_text') && get_field('work_button_link')): ?>
			                <div class="purchase-button" style="margin-top: 10px;">
			                        <a style="margin-left: 0px;" href="<?php echo (get_field('work_button_link')) ? get_field('work_button_link') : ''; ?>" class="purchase-btn" target="_blank"><?php echo (get_field('work_button_text')) ? get_field('work_button_text') : ''; ?></a>
			                </div>
			                <?php endif ?>
	            		</div>
	            		<div class="col-sm-6 hideMeMobile">
	            			<img src="<?php the_field('work_image'); ?>" class="img-responsive">
	            		</div>
	            	</div>
	            </div>
	      	</div>
	    </section>

    <?php endwhile; endif; ?>
    
<?php get_footer(); ?>