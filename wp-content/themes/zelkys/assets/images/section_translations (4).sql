-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 04, 2018 at 11:34 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kertu_demo5`
--

-- --------------------------------------------------------

--
-- Table structure for table `section_translations`
--

CREATE TABLE `section_translations` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `language_slug` varchar(5) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `section_html` text,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_translations`
--

INSERT INTO `section_translations` (`id`, `section_id`, `language_slug`, `title`, `section_html`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2292, 22, 'es', 'Billeder med tekster 3 kolonner og overskrift', '\n<div class=\"contentbox  text-center\" >\n<div class=\"col-md-12\">\n<div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_127\" style=\"position: relative;\">\n<h2>WELCOME</h2>\n<h3>Dummy text dummy text dummy text dummy text dummy text <br>Dummy text dummy text dummy text dummy text dummy text dummy text dummy text</h3>\n </div>\n</div>\n</div>', 0, '2018-01-07 18:36:45', '2018-01-07 18:36:45', NULL),
(2293, 22, 'DA', 'Billeder med tekster 3 kolonner og overskrift', '\n<div class=\"contentbox  text-center\" >\n<div class=\"col-md-12\">\n<div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_127\" style=\"position: relative;\">\n<h2>WELCOME</h2>\n<h3>Dummy text dummy text dummy text dummy text dummy text <br>Dummy text dummy text dummy text dummy text dummy text dummy text dummy text</h3>\n </div>\n</div>\n</div>', 0, '2018-01-07 18:36:45', '2018-01-07 18:36:45', NULL),
(2294, 22, 'EST', 'Billeder med tekster 3 kolonner og overskrift', '\n<div class=\"contentbox  text-center\" >\n   <div class=\"col-md-12\">\n<div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_127\" style=\"position: relative;\">\n<h2>WELCOME</h2>\n<h3>Dummy text dummy text dummy text dummy text dummy text <br>Dummy text dummy text dummy text dummy text dummy text dummy text dummy text</h3>\n </div>\n</div>\n</div>', 0, '2018-01-07 18:36:45', '2018-01-07 18:36:45', NULL),
(2295, 22, 'en', 'Billeder med tekster 3 kolonner og overskrift', '\n<div class=\"contentbox  text-center\" >\n<div class=\"col-md-12\">\n<div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_127\" style=\"position: relative;\">\n<h2>WELCOME</h2>\n<h3>Dummy text dummy text dummy text dummy text dummy text <br>Dummy text dummy text dummy text dummy text dummy text dummy text dummy text</h3>\n </div>\n</div>\n</div>', 0, '2018-01-07 18:36:45', '2018-01-07 18:36:45', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `section_translations`
--
ALTER TABLE `section_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `section_translations`
--
ALTER TABLE `section_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2408;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
