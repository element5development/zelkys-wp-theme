-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2018 at 12:11 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acaao_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `section_translations`
--

CREATE TABLE `section_translations` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `language_slug` varchar(5) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `section_html` text,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_translations`
--

INSERT INTO `section_translations` (`id`, `section_id`, `language_slug`, `title`, `section_html`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(155, 6, 'DA', 'Content', '<div class=\"row\">    <div class=\"col-sm-12\">  <h3 class=\"ckedit\"  contenteditable=\"true\">Design selv</h3><figure class=\"col-sm-6 pull-right\">        <img src=\"http://borupbeauty.dk/assets/images/img123-.jpg\" alt=\"\" class=\"img-responsive\" oldsrc=\"http://borupbeauty.dk/assets/images/img545-.png\" style=\"display: block;\">    </figure>    <div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_focus cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_121\" style=\"position: relative;\"><div><p>Cras tellus ipsum, euismod et rutrum nec, mollis et lectus. Donec quis turpis nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus gravida nibh sem, ac auctor massa feugiat eget. Vestibulum mollis ipsum a elit imperdiet congue. Donec rutrum purus quis condimentum ultricies. Aliquam erat volutpat. Nulla vitae justo dignissim, luctus odio quis, tempus ligula. Suspendisse potenti. Nunc porta lorem sed nulla gravida, consectetur volutpat enim commodo. Maecenas non diam et risus consectetur efficitur id posuere mi. Nullam fringilla lacus volutpat sem condimentum, eu commodo ex placerat. Nullam vitae mauris venenatis, venenatis leo non, molestie lectus.</p><p>Quisque posuere, dolor sed tempor lacinia, felis odio dictum magna, ut sollicitudin risus leo in diam. Cras hendrerit rutrum metus, a porta nisi lacinia nec. Morbi dapibus vulputate dolor et sagittis. Donec interdum euismod nisl, sit amet lacinia massa hendrerit vel.</p><p>Nulla faucibus dui ex, quis porttitor mi dictum ut. Cras malesuada nec elit non sagittis. Suspendisse quis iaculis mauris. Aliquam feugiat at velit sit amet ornare. Pellentesque aliquam cursus ipsum. Donec malesuada eget odio eget euismod. Nunc tincidunt ex pretium elit finibus, sed bibendum libero dictum. Duis dapibus nulla id mauris auctor, aliquet consectetur enim placerat. Nulla sed urna at tortor pretium maximus. Duis tempus sagittis dui, ut tempor erat lobortis id.&nbsp;Donec suscipit, ipsum nec congue convallis, leo mauris ultricies erat, eu finibus est diam mattis metus. Suspendisse vulputate diam vel laoreet lobortis. Mauris aliquam augue id blandit malesuada. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vestibulum enim lacus, vitae convallis augue porta a. Pellentesque ut maximus magna. Morbi bibendum, nunc non fermentum aliquam, tellus justo venenatis velit, sed commodo magna est a libero. Nam imperdiet ligula eget rhoncus mattis. Donec quis quam at urna pellentesque lacinia condimentum eget mi. Nulla imperdiet enim malesuada aliquet viverra. Ut consequat velit mi, eu varius lacus rutrum a. Duis in lorem metus. Nulla sed magna vitae nisl molestie ullamcorper. Cras porttitor purus a malesuada scelerisque.</p></div></div></div></div>                            ', 1, '2016-07-09 18:41:16', '2016-09-23 13:58:10', NULL),
(1237, 6, 'en', 'Content', '<div class=\"row\">    <div class=\"col-sm-12\"> <h3 class=\"ckedit\" contenteditable=\"true\">Design selv</h3> <figure class=\"col-sm-6 pull-right\">        <img src=\"http://borupbeauty.dk/assets/images/img123-.jpg\" alt=\"\" class=\"img-responsive\" oldsrc=\"http://borupbeauty.dk/assets/images/img545-.png\" style=\"display: block;\">    </figure>    <div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_focus cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_121\" style=\"position: relative;\"><div><p>Cras tellus ipsum, euismod et rutrum nec, mollis et lectus. Donec quis turpis nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus gravida nibh sem, ac auctor massa feugiat eget. Vestibulum mollis ipsum a elit imperdiet congue. Donec rutrum purus quis condimentum ultricies. Aliquam erat volutpat. Nulla vitae justo dignissim, luctus odio quis, tempus ligula. Suspendisse potenti. Nunc porta lorem sed nulla gravida, consectetur volutpat enim commodo. Maecenas non diam et risus consectetur efficitur id posuere mi. Nullam fringilla lacus volutpat sem condimentum, eu commodo ex placerat. Nullam vitae mauris venenatis, venenatis leo non, molestie lectus.</p><p>Quisque posuere, dolor sed tempor lacinia, felis odio dictum magna, ut sollicitudin risus leo in diam. Cras hendrerit rutrum metus, a porta nisi lacinia nec. Morbi dapibus vulputate dolor et sagittis. Donec interdum euismod nisl, sit amet lacinia massa hendrerit vel.</p><p>Nulla faucibus dui ex, quis porttitor mi dictum ut. Cras malesuada nec elit non sagittis. Suspendisse quis iaculis mauris. Aliquam feugiat at velit sit amet ornare. Pellentesque aliquam cursus ipsum. Donec malesuada eget odio eget euismod. Nunc tincidunt ex pretium elit finibus, sed bibendum libero dictum. Duis dapibus nulla id mauris auctor, aliquet consectetur enim placerat. Nulla sed urna at tortor pretium maximus. Duis tempus sagittis dui, ut tempor erat lobortis id.&nbsp;Donec suscipit, ipsum nec congue convallis, leo mauris ultricies erat, eu finibus est diam mattis metus. Suspendisse vulputate diam vel laoreet lobortis. Mauris aliquam augue id blandit malesuada. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vestibulum enim lacus, vitae convallis augue porta a. Pellentesque ut maximus magna. Morbi bibendum, nunc non fermentum aliquam, tellus justo venenatis velit, sed commodo magna est a libero. Nam imperdiet ligula eget rhoncus mattis. Donec quis quam at urna pellentesque lacinia condimentum eget mi. Nulla imperdiet enim malesuada aliquet viverra. Ut consequat velit mi, eu varius lacus rutrum a. Duis in lorem metus. Nulla sed magna vitae nisl molestie ullamcorper. Cras porttitor purus a malesuada scelerisque.</p></div></div></div></div>                            ', 0, '2016-10-09 19:49:10', NULL, NULL),
(1483, 6, 'EST', 'Content', '<div class=\"row\">    <div class=\"col-sm-12\"> <h3 class=\"ckedit\" contenteditable=\"true\">Design selv</h3>  <figure class=\"col-sm-6 pull-right\">        <img src=\"http://borupbeauty.dk/assets/images/img123-.jpg\" alt=\"\" class=\"img-responsive\" oldsrc=\"http://borupbeauty.dk/assets/images/img545-.png\" style=\"display: block;\">    </figure>    <div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_focus cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_121\" style=\"position: relative;\"><div><p>Cras tellus ipsum, euismod et rutrum nec, mollis et lectus. Donec quis turpis nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus gravida nibh sem, ac auctor massa feugiat eget. Vestibulum mollis ipsum a elit imperdiet congue. Donec rutrum purus quis condimentum ultricies. Aliquam erat volutpat. Nulla vitae justo dignissim, luctus odio quis, tempus ligula. Suspendisse potenti. Nunc porta lorem sed nulla gravida, consectetur volutpat enim commodo. Maecenas non diam et risus consectetur efficitur id posuere mi. Nullam fringilla lacus volutpat sem condimentum, eu commodo ex placerat. Nullam vitae mauris venenatis, venenatis leo non, molestie lectus.</p><p>Quisque posuere, dolor sed tempor lacinia, felis odio dictum magna, ut sollicitudin risus leo in diam. Cras hendrerit rutrum metus, a porta nisi lacinia nec. Morbi dapibus vulputate dolor et sagittis. Donec interdum euismod nisl, sit amet lacinia massa hendrerit vel.</p><p>Nulla faucibus dui ex, quis porttitor mi dictum ut. Cras malesuada nec elit non sagittis. Suspendisse quis iaculis mauris. Aliquam feugiat at velit sit amet ornare. Pellentesque aliquam cursus ipsum. Donec malesuada eget odio eget euismod. Nunc tincidunt ex pretium elit finibus, sed bibendum libero dictum. Duis dapibus nulla id mauris auctor, aliquet consectetur enim placerat. Nulla sed urna at tortor pretium maximus. Duis tempus sagittis dui, ut tempor erat lobortis id.&nbsp;Donec suscipit, ipsum nec congue convallis, leo mauris ultricies erat, eu finibus est diam mattis metus. Suspendisse vulputate diam vel laoreet lobortis. Mauris aliquam augue id blandit malesuada. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vestibulum enim lacus, vitae convallis augue porta a. Pellentesque ut maximus magna. Morbi bibendum, nunc non fermentum aliquam, tellus justo venenatis velit, sed commodo magna est a libero. Nam imperdiet ligula eget rhoncus mattis. Donec quis quam at urna pellentesque lacinia condimentum eget mi. Nulla imperdiet enim malesuada aliquet viverra. Ut consequat velit mi, eu varius lacus rutrum a. Duis in lorem metus. Nulla sed magna vitae nisl molestie ullamcorper. Cras porttitor purus a malesuada scelerisque.</p></div></div></div></div>                            ', 0, '2016-10-10 20:07:32', NULL, NULL),
(1694, 6, 'es', 'Content', '<div class=\"row\">    <div class=\"col-sm-12\"> <h3 class=\"ckedit\" contenteditable=\"true\">Design selv</h3>  <figure class=\"col-sm-6 pull-right\">        <img src=\"http://borupbeauty.dk/assets/images/img123-.jpg\" alt=\"\" class=\"img-responsive\" oldsrc=\"http://borupbeauty.dk/assets/images/img545-.png\" style=\"display: block;\">    </figure>    <div class=\"ckedit cke_editable cke_editable_inline cke_contents_ltr cke_focus cke_show_borders\" contenteditable=\"true\" tabindex=\"0\" spellcheck=\"false\" role=\"textbox\" aria-label=\" \" title=\" \" aria-describedby=\"cke_121\" style=\"position: relative;\"><div><p>Cras tellus ipsum, euismod et rutrum nec, mollis et lectus. Donec quis turpis nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus gravida nibh sem, ac auctor massa feugiat eget. Vestibulum mollis ipsum a elit imperdiet congue. Donec rutrum purus quis condimentum ultricies. Aliquam erat volutpat. Nulla vitae justo dignissim, luctus odio quis, tempus ligula. Suspendisse potenti. Nunc porta lorem sed nulla gravida, consectetur volutpat enim commodo. Maecenas non diam et risus consectetur efficitur id posuere mi. Nullam fringilla lacus volutpat sem condimentum, eu commodo ex placerat. Nullam vitae mauris venenatis, venenatis leo non, molestie lectus.</p><p>Quisque posuere, dolor sed tempor lacinia, felis odio dictum magna, ut sollicitudin risus leo in diam. Cras hendrerit rutrum metus, a porta nisi lacinia nec. Morbi dapibus vulputate dolor et sagittis. Donec interdum euismod nisl, sit amet lacinia massa hendrerit vel.</p><p>Nulla faucibus dui ex, quis porttitor mi dictum ut. Cras malesuada nec elit non sagittis. Suspendisse quis iaculis mauris. Aliquam feugiat at velit sit amet ornare. Pellentesque aliquam cursus ipsum. Donec malesuada eget odio eget euismod. Nunc tincidunt ex pretium elit finibus, sed bibendum libero dictum. Duis dapibus nulla id mauris auctor, aliquet consectetur enim placerat. Nulla sed urna at tortor pretium maximus. Duis tempus sagittis dui, ut tempor erat lobortis id.&nbsp;Donec suscipit, ipsum nec congue convallis, leo mauris ultricies erat, eu finibus est diam mattis metus. Suspendisse vulputate diam vel laoreet lobortis. Mauris aliquam augue id blandit malesuada. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vestibulum enim lacus, vitae convallis augue porta a. Pellentesque ut maximus magna. Morbi bibendum, nunc non fermentum aliquam, tellus justo venenatis velit, sed commodo magna est a libero. Nam imperdiet ligula eget rhoncus mattis. Donec quis quam at urna pellentesque lacinia condimentum eget mi. Nulla imperdiet enim malesuada aliquet viverra. Ut consequat velit mi, eu varius lacus rutrum a. Duis in lorem metus. Nulla sed magna vitae nisl molestie ullamcorper. Cras porttitor purus a malesuada scelerisque.</p></div></div></div></div>                            ', 0, '2016-10-10 20:13:20', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `section_translations`
--
ALTER TABLE `section_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `section_translations`
--
ALTER TABLE `section_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2384;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
