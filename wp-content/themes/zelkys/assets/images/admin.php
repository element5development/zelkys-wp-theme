<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $page_title; ?></title>
    <!-- TEMPLATE STYLESHEETS -->
    <?php foreach ($styles as $style): ?>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/admin/' . $style); ?>" />
    <?php endforeach;?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.0/css/bootstrap-colorpicker.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>">
    <!-- <link rel="stylesheet" href="<?php echo base_url('assets/css/cropper.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>"> -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <body>
        <?php $site_url = site_url();?>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url('admin'); ?>">
                        <img src='<?php echo base_url('assets/images/logo-admin.png'); ?>' />
                    </a>
                    <h4 class="otherlogo"><a href="<?php echo base_url(); ?>"><img src='<?php echo base_url('assets/images/logo.png'); ?>' /></a></h4>
                    <h4 class='primary'><?php echo ucwords($auth_name); ?></h4>
                </div>
                <!-- /.navbar-header -->
                <ul class="nav navbar-top-links navbar-right">
                <?php if (count($langs) > 1 ) {?>
                    <li class="dropdown">
                        <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <?php echo @$current_lang['language_name']; ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <?php foreach ($langs as $key => $value) {?>
                                <?php if(strtolower($value['language_name']) != strtolower($current_lang['language_name'])) {?>
                                <li><a href="<?php echo '?language='.strtolower($value['language_name']); ?>"><?php echo $value['language_name'];?></a></li>
                                <?php }?>
                            <?php }?>
                        </ul>
                    </li>
                <?php }?>
                    <li><a class='primary-bg' href="<?php echo base_url();?>booking/src/index.php/backend/<?php echo $this->auth_user_id;?>" target='_blank'><?php echo $this->lang->line('calendar_menu');?></a></li>
                    <li><a class='primary-bg' id="pos_login" href="#_"><?php echo $this->lang->line('pos_menu');?></a></li>
                    <li><a class='danger' href="<?php echo base_url('admin/logout'); ?>"><?php echo $this->lang->line('logout_menu');?></a></li>
                </ul>
            </nav>
            <div id="page-wrapper" class="project-detail">
                <div class="navbar-default " role="navigation">
                    <div class="sidebar sidebar-nav navbar-collapse collapse">
                        <h5 class="primary-light-bg"><?php echo $this->lang->line('menu_heading');?></h5>
                        <ul class="nav" id="side-menu">
                            <li></li>
                            <?php if ($auth_role == 'admin') {?>
                            <li><a href="<?php echo base_url('admin/general'); ?>" <?php echo isset($general_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('general_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/pages'); ?>" <?php echo isset($page_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('pages_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/services'); ?>" <?php echo isset($service_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('services_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/categories'); ?>" <?php echo isset($category_active)? 'class="active"':'';?>><?php echo $this->lang->line('categories_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/subcategories'); ?>" <?php echo isset($subcategory_active)? 'class="active"':'';?>><?php echo $this->lang->line('subcategories_menu');?></a></li>
                            <!-- <li><a href="<?php echo base_url('admin/attributes'); ?>" <?php echo isset($attribute_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('attributes_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/manufacturers'); ?>" <?php echo isset($manufacturer_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('manufacturers_menu');?></a></li> -->
                            <li><a href="<?php echo base_url('admin/news'); ?>" <?php echo isset($news_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('news_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/openinghours'); ?>" <?php echo isset($openinghours_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('openinghours_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/holidays'); ?>" <?php echo isset($holidays_active)? 'class="active"':'';?>><?php echo $this->lang->line('holidays_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/socials'); ?>" <?php echo isset($social_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('socialmedia_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/seo'); ?>" <?php echo isset($seo_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('seo_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/newsletters'); ?>" <?php echo isset($newsletter_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('newsletters_menu');?></a></li>

                            <li><a href="<?php echo base_url('admin/newsletter_subscriber'); ?>" <?php echo isset($newsletter_subscriber_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('newsletter_subscriber_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/employees'); ?>" <?php echo isset($employee_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('employees_menu');?></a></li>
                            <li><a href="<?php echo base_url('admin/employee_booking_plans'); ?>" <?php echo isset($employee_booking_plan_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('employee_booking_plan_menu');?></a></li>
                            <?php }?>
                            <li><a href="<?php echo base_url('admin/customers'); ?>" <?php echo isset($customer_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('customers_menu');?></a></li>
                            <?php if ($this->session->userdata('auth_data')->user_id == 5) {?>
                            <li><a href="<?php echo base_url('admin/activity_logs'); ?>" <?php echo isset($logs_active) ? 'class="active"' : ''; ?>><?php echo $this->lang->line('activity_logs_menu');?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="right-sec">
                    <?php $this->load->view('layouts/page_error_list');?>
                    <?php $this->load->view($view);?>
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/css-toggle-switch/latest/toggle-switch.css" />
        <?php foreach ($scripts as $script): ?>
            <script type="text/javascript" src="<?php echo (strpos('--' . $script, 'http:') || strpos('--' . $script, 'https:')) ? $script : base_url('assets/js/' . $script); ?>"></script>
        <?php endforeach;?>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- start: Inline JavaScript-->
        <?php if (isset($scripts['inline'])): ?>
            <?php foreach ($scripts['inline'] as $script): ?>
                <script type="text/javascript"><?php print $script;?></script>
            <?php endforeach;?>
        <?php endif;?>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.inlineedit.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.js"></script>
        <script type="text/javascript">
        function comparecolor (colortocheck){
            noalpha = 0;

            colortocheck = colortocheck.toString();
            colortocheck1 = colortocheck.split('a(');
            colortocheck1 = colortocheck1[1];
            if (colortocheck1 == undefined) {
                colortocheck1 = colortocheck.split('b(');
                colortocheck1 = colortocheck1[1];
                noalpha = 1;
            }
            if (!noalpha) {
                lastIndex = colortocheck1.toString().lastIndexOf(",");
                colortocheck = colortocheck1.substring(0, lastIndex);
            }else{
                lastIndex = colortocheck1.toString().lastIndexOf(")");
                colortocheck = colortocheck1.substring(0, lastIndex);
            }
            return colortocheck;
        }

        $('.removeimage').on('click', function(event){
            if(!confirm('Are you sure you want to delete this image?')){
                return false;
            }
            me = $(this);
            imagetype = me.next().find('label').attr('for').split('-')[1];
            imageid = me.next().find('img').attr('id').split('-')[1];
             $.ajax({
                data: {'image_id': imageid},
                type: 'POST',
                url: base_url+'image/remove/'+imagetype,
                success: function(data, textStatus, jqXHR){
                    if (me.parents('.feature-box').html() == undefined) {
                        me.parent().remove();
                        $('.scrub-'+imageid).parent().remove();
                        location.reload();
                    }else{
                        me.parents('.feature-box').remove();
                    }
                }
            });
        });

        $('.removevideo').on('click', function (event) {
            if(!confirm('Are you sure you want to delete this video?')){
                return false;
            }
            me = $(this);
            var id = this.id;
            $.ajax({
                data: {'id': id},
                type: 'POST',
                url: base_url+'video/remove/',
                success: function(data, textStatus, jqXHR){
                    if (me.parents('.feature-box').html() == undefined) {
                        me.parent().remove();
                        location.reload();
                    }else{
                        me.parents('.feature-box').remove();
                    }
                }
            });
        });

        $(document).ready(function(){
            $('.bigbutton').attr('href', $('.bigbutton').attr('oldhref'))
            pageid = "<?php echo isset($translation->page_id)? $translation->page_id:0; ?>";
            $(".owl-theme").owlCarousel({
                center: true,
                items:2,
                lazyLoad : true,
                loop: true,
                margin: 10,
            });

            /*$('.bx-slider').bxSlider({
                pager: ($(".bx-slider li").length > 1) ? true: false,
            });

            $('.bx-slider-fade').bxSlider({
                pager: false,
                mode: 'fade',
                speed: 2500,
                slideMargin:1,
                infiniteLoop: true,
                preloadImages:'visible',
                controls:false,
                auto:true,
                easing: 'ease-in-out',
            });*/

        $('.bx-slider').each(function(index, el){
          $(this).bxSlider({
              mode: ($(this).attr('type')==0)? 'horizontal':'fade',
              infiniteLoop: true,
              auto: true,
              autoStart: true,
              autoDirection: 'next',
              autoHover: true,
              pager: ($(this).attr('type')==0)? (($(this).find("li").length > 1) ? true: false):false,
              pagerType: 'full',
              controls: ($(this).attr('type')==0)? true:false,
              captions: true,
              speed: ($(this).attr('type')==0)? 500:2500,
              slideMargin:1,
              preloadImages:'visible',
          });

        });

        $('.bx-slider-fade').each(function(index, el){
          $(this).bxSlider({
            mode: ($(this).attr('type')==0)? 'horizontal':'fade',
            infiniteLoop: true,
            auto: true,
            autoStart: true,
            autoDirection: 'next',
            // autoHover: true,
            pager: ($(this).attr('type')==0)? (($(this).find("li").length > 1) ? true: false):false,
            pagerType: 'full',
            controls: ($(this).attr('type')==0)? true:false,
            captions: true,
            speed: ($(this).attr('type')==0)? 500:2500,
            slideMargin:1,
            preloadImages:'visible',
          });
        });
            if (jQuery(window).width() < 767) {
                $('.service-slider').bxSlider({
                  minSlides: 3,
                  maxSlides: 1,
                  moveSlides: 1,
                  slideWidth: 320,
                  pager: false,
                  responsive: true
                });
            } else {
                $('.service-slider').bxSlider({
                  minSlides: 3,
                  maxSlides: 3,
                  moveSlides: 1,
                  slideWidth: 360,
                  slideMargin: 30,
                  pager: false,
                  responsive: true
                });
            }

            $('#addsection').removeClass('in');
            $('#addsection .in').removeClass('in');



            html ='';
            <?php if (isset($newslist)) {foreach ($newslist as $key => $value) {?>
                    html+=   '<li class="clearfix"><h2 class="date"><?php echo date('d', strtotime($value['publication_date'])); ?>. <small class="month"><?php echo date('F', strtotime($value['publication_date'])); ?></small></h2><div class="detail pull-right"><p><?php echo $value['appetizer_text']; ?>... <strong class="pull-right"><a href="<?php echo base_url(strtolower(preg_replace('/(?=\P{Nd})\P{L}/u' , '', $value['header_text'] ))   );?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  <?php echo $this->lang->line('readmore');?></a></strong></p></div></li>';

            <?php }}?>
            $('.news').html(html);

            //opaning hours
            html ='';
            <?php if (isset($openinghours)) {
                foreach ($openinghours as $key => $value) {
                    if ($value["description"]->opening) {?>
                html+=   '<li class="clearfix"><p class="pull-left"><?php echo $this->lang->line($value["day"]);?></p><p class="pull-right"><?php echo $value["description"]->opening . " - " . $value["description"]->closing;?></p></li>';
            <?php }else{?>
                html+=   '<li class="clearfix"><p class="pull-left"><?php echo $this->lang->line($value["day"]);?></p><p class="pull-right"><?php echo $this->lang->line("closed");?></p></li>';
            <?php }}}?>
            $('.timing').html(html);

            footernav = <?php echo (isset($footermenu)) ? 1 : 0; ?>;
            if (footernav) {
                html ='';
                <?php if (isset($footermenu)) {foreach ($footermenu as $key => $value) {?>
                    html+=   '<li  id="<?php echo 'item-' . $key; ?>"><a class="nav-handle" href="#"><i class="fa fa-arrows" aria-hidden="true"></i></a><a href="<?php echo $value['page_title']; ?>" class="editable" pageid="<?php echo $key; ?>"><?php echo $value['menu_title']; ?></a></li>'
                <?php }}?>
                $('#footernav').html(html);
            }

             $('#colors .input-group').colorpicker({
                component:".colorbox",
                format: "rgb",
                customClass: 'colorpicker-2x',
                sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                },
                alpha: {
                    maxTop: 200
                }
            },
                colorSelectors: {
                    'default': '#777777',
                    'primary': '#337ab7',
                    'success': '#5cb85c',
                    'info': '#5bc0de',
                    'warning': '#f0ad4e',
                    'danger': '#d9534f'
               }
            }).on('changeColor', function(e) {
                $('.themeupdate').hide();
                $(this).parent().parent().find('.themeupdate').show();

                $(this).find('.colorbox')[0].style.backgroundColor = e.color.toHex();
                oldcolor = $(this).parent().find('.oldcolor').val();
                $(this).parent().find('.oldcolor').val(e.color);
                sectionclass = $(this).parent().find('.sectionclass').val();
                var find = '';
                if (sectionclass == 'color0') {
                    find = 'header';
                } else if (sectionclass == 'color1') {
                    find = 'nav, input, section, .sliderboxes .grey-bg, .socialicon a';
                } else if (sectionclass == 'color2') {
                    find = 'li, p, section, a, h3, input[type=submit], .socialicon a';
                } else if (sectionclass == 'color3') {
                    find = 'li, p, section, a, h3, input[type=text], input[type=email], .newsletter, .widget .col-sm-4';
                } else if (sectionclass == 'color4') {
                    find = 'h3, h2, section p, b, label, .news a, .month, .news-sec, .col-md-4 .editable';
                } else if (sectionclass == 'color5') {
                    find = 'section, figcaption';
                } else if (sectionclass == 'color6') {
                    find = 'section, figcaption';
                } else if (sectionclass == 'color7') {
                    find = 'form';
                }

                colortocheck = comparecolor(oldcolor).split(" ").join("").toString();
               // $(".front").find('header, div, nav, li, section, input, form, p, h1, h2, h3, h4, small, a, b, label, figcaption').each(function(index, el) {
               $(".front").find(find).each(function(index, el) {
                    curelemcolor = comparecolor($(this).css('background-color'));
                    newelemcolor = comparecolor(e.color);
                    curelemcolor = curelemcolor.split(" ").join("").toString();
                    if ( curelemcolor  == colortocheck) {
                        currentcolor =  $(this).css('background-color').split(" ").join("").toString();
                        currentcolor = currentcolor.replace(curelemcolor,newelemcolor)
                        $(this).css('background-color', currentcolor);
                    }

                    curelemcolor = comparecolor($(this).css('border-color'));
                    newelemcolor = comparecolor(e.color);
                    curelemcolor = curelemcolor.split(" ").join("").toString();
                    if ( curelemcolor  == colortocheck) {
                        currentcolor =  $(this).css('border-color').split(" ").join("").toString();
                        currentcolor = currentcolor.replace(curelemcolor,newelemcolor)
                        $(this).css('border-color', currentcolor);
                    }

                    curelemcolor = comparecolor($(this).css('color'));
                    newelemcolor = comparecolor(e.color);
                    curelemcolor = curelemcolor.split(" ").join("").toString();
                    if ( curelemcolor  == colortocheck) {
                        currentcolor =  $(this).css('color').split(" ").join("").toString();
                        currentcolor = currentcolor.replace(curelemcolor,newelemcolor)
                        $(this).css('color', currentcolor);
                    }
                });
           });

            $('#fonts').on('change', function () {
                $('.themeupdate').hide();
                $(this).parent().parent().find('.themeupdate').show();
            });
            $('.themeupdate').hide();

            $('.previewme').click(function(e){
                if (!$(this).hasClass('active')) {
                    $('.themeupdate').hide();
                     $(this).next().show();
                    $(this).parents('.btn-group').find('.previewme').removeClass('active');
                    $(this).addClass('active');
                    ul = $(this).parent().parent().find('ul');

                    $('#theme'+$('#active_theme').val()+' ul li').each(function(index, el) {
                        fromInput = $(this).find('.input-group > input');
                        input = ul.find("input[name="+fromInput.attr('name')+"]");
                        input.parent().parent().find('.oldcolor').val($(this).find('.oldcolor').val().split(" ").join("").toString());
                        me = input.parent();
                        oldcolor = me.parent().find('.oldcolor').val();
                        me.parent().find('.oldcolor').val(input.val().split(" ").join("").toString());
                        colortocheck = comparecolor(oldcolor).split(" ").join("").toString();

                        $(".front").find('header, div, nav, li, section, input, form, p, h1, h2, h3, h4, small, a, b, label, figcaption').each(function(index, el) {
                            curelemcolor = comparecolor($(this).css('background-color').split(" ").join("").toString());
                            newelemcolor = comparecolor(input.val().split(" ").join("").toString());
                            curelemcolor = curelemcolor.split(" ").join("").toString();
                            if ( curelemcolor  == colortocheck) {
                                currentcolor =  $(this).css('background-color').split(" ").join("").toString();
                                currentcolor = currentcolor.replace(curelemcolor,newelemcolor)
                                $(this).css('background-color', currentcolor);
                            }

                            curelemcolor = comparecolor($(this).css('border-color'));
                            newelemcolor = comparecolor(input.val().split(" ").join("").toString());
                            curelemcolor = curelemcolor.split(" ").join("").toString();
                            if ( curelemcolor  == colortocheck) {
                                currentcolor =  $(this).css('border-color').split(" ").join("").toString();
                                currentcolor = currentcolor.replace(curelemcolor,newelemcolor)
                                $(this).css('border-color', currentcolor);
                            }

                            curelemcolor = comparecolor($(this).css('color'));
                            newelemcolor = comparecolor(input.val().split(" ").join("").toString());
                            curelemcolor = curelemcolor.split(" ").join("").toString();
                            if ( curelemcolor  == colortocheck) {
                                currentcolor =  $(this).css('color').split(" ").join("").toString();
                                currentcolor = currentcolor.replace(curelemcolor,newelemcolor)
                                $(this).css('color', currentcolor);
                            }
                        });

                    });
                    $('#active_theme').val($(this).parents('form').find('.theme_id').val());
                }
            });



            $('.cmsfront .editable').inlineEdit({
                buttons: '<a href="#" class="save"><i class="fa fa-check" aria-hidden="true"></i></a> <a href="#" class="cancel"><i class="fa fa-times" aria-hidden="true"></i></a>',
                buttonsTag: 'a',
                save: function(e, data) {
                    var pageId = $(this).parents('a').data('page');
                    if ($(this).attr('pageid') != undefined || pageId != undefined) {
                        $.ajax({
                            data: {'page_id':pageId, 'menu_title':data.value, 'language_slug':language_slug},
                            type: 'POST',
                            url: base_url+'menu/rename',
                            success: function(data, textStatus, jqXHR){
                            }
                        });
                    } else {
                        if (!$(this).parents('.cmsedit ').find('.insider').hasClass('in') ) {
                            $(this).parents('.cmsedit ').find('.cmseditbtn').trigger('click');
                                $(this).parents('.cmsedit ').find('.cmsupdatebtn').show();
                        }
                    }
              }
            });

        $('.cmsfront .ckedit').on('click', function(e){
             if (!$(this).parents('.cmsedit ').find('.insider').hasClass('in') ) {
                $(this).parents('.cmsedit ').find('.cmseditbtn').trigger('click');
                    $(this).parents('.cmsedit ').find('.cmsupdatebtn').show();
            }
        });


        });
        $('body').on('keyup, focus', '.edit-in-progress textarea', function(){
            this.style.height = "1px";
            this.style.height = (this.scrollHeight)+"px";
        });
            function resizeInput() {
                if ($(this).parents('.navlinks').html() != undefined) {
                    this.style.width = ($(this).val().length*11)+"px";
                }

                this.style.height = (this.scrollHeight)+"px";

                // this.style.height = (this.scrollHeight)+"px";
            }
        </script>


        <script type="text/javascript">
            // var language_slug = "<?php echo isset($current_lang['language_code']) ? $current_lang['language_code'] : 'en'; ?>";
            var language_slug = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
            var page_id = "<?php echo isset($translation->page_id) ? $translation->page_id : ''; ?>";

            var base_url = '<?php print base_url();?>';
            var site_url = '<?php print site_url();?>';
            var csrf_cookie_name = '<?php print $this->config->item("csrf_cookie_name");?>';
            var csrf_token_name = '<?php print $this->config->item("csrf_token_name");?>';
            var CKEDITOR_BASEPATH = '<?php print base_url();?>/assets/ckeditor';

            $('#confirm-delete').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
                $(this).find('.btn-ok').attr('sectionid', $(e.relatedTarget).data('sectionid'));
                $(this).find('.btn-ok').attr('pageid', $(e.relatedTarget).data('pageid'));
                $(this).find('.btn-ok').attr('languageslug', $(e.relatedTarget).data('languageslug'));

                title   = $(e.relatedTarget).data('title');
                message = $(e.relatedTarget).data('message');
                type    = $(e.relatedTarget).data('type');
                $(this).find('.modal-header h1').html(title)
                $(this).find('.modal-body p').html(message)
                //remove footernav section
                $('.btn-ok').on('click', function (event) {
                    if ($(this).attr('id') != 'activate-ok') {
                        event.preventDefault();
                    }
                    $(this).html('<i class="fa fa-cog fa-spin fa-fw"></i>');
                    sectionid   = $(this).attr('sectionid');
                    pageid      = $(this).attr('pageid');
                    prvsections = $('input[name=sections]').val();
                    if ($(this).attr('id') == 'footernav-ok') {
                        $.ajax({
                            data: {'prvsections': prvsections, 'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug},
                            type: 'POST',
                            url: base_url+'section/remove',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                    if ($(this).attr('id') == 'sectionpub-ok') {
                        status = $(this).attr('href');
                        $.ajax({
                            data: {'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug, 'status':status},
                            type: 'POST',
                            url: base_url+'page/section/publish',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                    if ($(this).attr('id') == 'publish-ok') {
                        $.ajax({
                                data: {'page_id': pageid, 'language_slug':language_slug, 'publish':$(e.relatedTarget).data('publish')},
                                type: 'POST',
                                url: base_url+'page/publish',
                                success: function(data, textStatus, jqXHR){
                                    location.reload();
                                }
                            });
                    }
               });

            });


            $('#confirm-delete-page').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok-page').attr('href', $(e.relatedTarget).data('href'));
                $(this).find('.btn-ok-page').attr('id', $(e.relatedTarget).data('id'));
                $(this).find('.btn-ok-page').attr('sectionid', $(e.relatedTarget).data('sectionid'));
                $(this).find('.btn-ok-page').attr('pageid', $(e.relatedTarget).data('pageid'));
                $(this).find('.btn-ok-page').attr('languageslug', $(e.relatedTarget).data('languageslug'));

                $(this).find('.btn-all-page').attr('href', $(e.relatedTarget).data('all'));
                $(this).find('.btn-all-page').attr('id', $(e.relatedTarget).data('id'));
                $(this).find('.btn-all-page').attr('sectionid', $(e.relatedTarget).data('sectionid'));
                $(this).find('.btn-all-page').attr('pageid', $(e.relatedTarget).data('pageid'));
                $(this).find('.btn-all-page').attr('languageslug', $(e.relatedTarget).data('languageslug'));

                title   = $(e.relatedTarget).data('title');
                message = $(e.relatedTarget).data('message');
                type    = $(e.relatedTarget).data('type');
                $(this).find('.modal-header h1').html(title)
                $(this).find('.modal-body p').html(message)
                //remove footernav section
                $('.btn-ok-page').on('click', function (event) {
                    if ($(this).attr('id') != 'activate-ok') {
                        event.preventDefault();
                    }
                    $(this).html('<i class="fa fa-cog fa-spin fa-fw"></i>');
                    sectionid   = $(this).attr('sectionid');
                    pageid      = $(this).attr('pageid');
                    prvsections = $('input[name=sections]').val();
                    if ($(this).attr('id') == 'footernav-ok') {
                        $.ajax({
                            data: {'prvsections': prvsections, 'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug},
                            type: 'POST',
                            url: base_url+'section/remove',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                    if ($(this).attr('id') == 'sectionpub-ok') {
                        status = $(this).attr('href');
                        $.ajax({
                            data: {'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug, 'status':status},
                            type: 'POST',
                            url: base_url+'page/section/publish',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                    if ($(this).attr('id') == 'publish-ok') {
                        $.ajax({
                                data: {'page_id': pageid, 'language_slug':language_slug, 'publish':$(e.relatedTarget).data('publish')},
                                type: 'POST',
                                url: base_url+'page/publish',
                                success: function(data, textStatus, jqXHR){
                                    location.reload();
                                }
                            });
                    }
                });

                $('.btn-all-page').on('click', function (event) {
                    if ($(this).attr('id') != 'activate-ok') {
                        event.preventDefault();
                    }
                    $(this).html('<i class="fa fa-cog fa-spin fa-fw"></i>');
                    sectionid   = $(this).attr('sectionid');
                    pageid      = $(this).attr('pageid');
                    prvsections = $('input[name=sections]').val();
                    if ($(this).attr('id') == 'footernav-ok') {
                        $.ajax({
                            data: {'prvsections': prvsections, 'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug},
                            type: 'POST',
                            url: base_url+'section/remove',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                    if ($(this).attr('id') == 'sectionpub-ok') {
                        status = $(this).attr('href');
                        $.ajax({
                            data: {'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug, 'status':status},
                            type: 'POST',
                            url: base_url+'page/section/publish',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                    if ($(this).attr('id') == 'publish-ok') {
                        $.ajax({
                            data: {'page_id': pageid, 'language_slug':language_slug, 'publish':$(e.relatedTarget).data('publish')},
                            type: 'POST',
                            url: base_url+'page/publish',
                            success: function(data, textStatus, jqXHR){
                                location.reload();
                            }
                        });
                    }
                });
            });


            $('#selectAll').click(function (e) {
                $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
            });

            $('document').ready(function(){
                $('.addsectionbtn').on('click', function(){
                    me=$(this);
                    me.html('<i class="fa fa-cog fa-spin fa-fw"></i>');
                    sectionid = $(this).attr('sectionid');
                    pageid = $(this).attr('pageid');
                    prvsections = $('input[name=sections]').val();
                    $.ajax({
                            data: {'prvsections': prvsections, 'section_id': sectionid,'page_id': pageid, 'language_slug':language_slug},
                            type: 'POST',
                            url: base_url+'section/add',
                            success: function(data, textStatus, jqXHR){
                                $('input[name=sections]').val(data+','+prvsections);
                                location.reload();
                            }
                        });
                });

                $('.allsections .insider').remove();

                $('#currentsections').sortable({
                    handle: ".cmsmovebtn",
                    stop: function(event, ui) {
                        sections = '';
                        $('#currentsections .cmsedit').each(function(index, el) {
                            if ($(this).attr('sectionid') !=undefined) {
                                sections += $(this).attr('sectionid')+',';
                            }else{
                                $(this).remove();
                            }

                            if ($(this).attr('basesectionid')==4) {
                                $(this).after("<div class='cmsedit'></div>");
                            }
                        });
                        sections=sections.replace(/^,|,$/g,'');
                        $('input[name=sections]').val(sections);

                        $.ajax({
                            type: 'POST',
                            url: base_url+'pages/resortsections',
                            data: {'page_id':page_id, 'sections':sections},
                            success: function(data, textStatus, jqXHR){
                                $('.cmsupdatebtn').hide();
                                // $('.restore').hide();
                                if (me.parents('.cmsedit').find('.insider').hasClass('in')) {
                                    me.parents('.cmsedit').find('.cmseditbtn').trigger('click');
                                }
                            }
                        });
                  }
                });

                $('.cmsfront .navbar-nav').sortable({
                    // revert: true,
                    handle: ".nav-handle",
                    placeholder: "ui-state-highlight",
                    connectWith: ".connectedSortable, .connectedFooterSortable, .dropdown",
                    cancel: ".unsortable",
                    start: function(e, ui){
                        // console.log(ui.item.height());
                        ui.placeholder.height(ui.item.height());
                        ui.placeholder.width(ui.item.width());
                    },
                    stop: function(event, ui) {
                        $('#nav1 a').removeClass('editable');
                        $('#nav1 a').removeClass('ui-state-hover');
                        $('#nav li').each(function(index, el) {
                            if (!$(this).find('a').hasClass('editable')) {
                                $(this).find('a').addClass('editable');
                            }
                        // $(this).parents('.cmsedit').find('.restore').fadeIn(100);
                        });

                        $('#navfooter1 a').removeClass('editable');
                        $('#navfooter1 a').removeClass('ui-state-hover');
                        $('#footernav li').each(function(index, el) {
                            if (!$(this).find('a').hasClass('editable')) {
                                $(this).find('a').addClass('editable');
                            }
                        });

                        $(this).parents('.cmsedit').find('.cmsupdatebtn').fadeIn(100);
                                if (!$(this).parents('.cmsedit').find('.insider').hasClass('in')) {
                                    $(this).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                                }
                   }
                }).disableSelection();

                $('.galleryimages, .slidelist').sortable({
                  // placeholder: "gallery-placeholder",
                  cancel: ".unsortable",
                  start: function(e, ui){
                        ui.placeholder.height(ui.item.height());
                        ui.placeholder.width(ui.item.width()/1.5);
                    },
                  stop: function(event, ui) {
                        $(this).parents('.cmsedit').find('.cmsupdatebtn').fadeIn(100);
                        if (!$(this).parents('.cmsedit').find('.insider').hasClass('in')) {
                            $(this).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                        }
                   }
                });

                $('.referenceimages').sortable({
                    handle: ".cmsmovebtn",
                    stop: function(event, ui) {
                        $(this).parents('.cmsedit').find('.cmsupdatebtn').fadeIn(100);
                        if (!$(this).parents('.cmsedit').find('.insider').hasClass('in')) {
                            $(this).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                        }
                   }
                })

                $('.serviceimages').sortable({
                    handle: ".servicehandle",
                    cancel: ".unsortable",
                    stop: function(event, ui) {
                        $(this).parents('.cmsedit').find('.cmsupdatebtn').fadeIn(100);
                        // $(this).parents('.cmsedit').find('.restore').fadeIn(100);
                            if (!$(this).parents('.cmsedit').find('.insider').hasClass('in')) {
                                $(this).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                            }
                   }
                }).disableSelection();

                $('.subs').sortable({
                    // handle: ".nav-handle",
                    placeholder: "ui-state-highlight",
                    connectWith: ".secondlast",
                    cancel: ".unsortable",
                    stop: function(event, ui) {
                        $(this).parents('.cmsedit').find('.cmsupdatebtn').fadeIn(100);
                            if (!$(this).parents('.cmsedit').find('.insider').hasClass('in')) {
                                $(this).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                                $(this).parents('.mainsub').prev().trigger('click');
                            }
                                $(this).parents('.mainsub').removeClass('ischanged');
                                $(this).parents('.mainsub').addClass('ischanged');
                   }
                }).disableSelection();

                $('.addsubmenucol').on('click', function(e){
                    e.stopPropagation();
                    $(this).before("<ul  class='subs secondlast ui-sortable'><a class='removecolumn' href='javascript:void(0);'><i class='fa fa-times' aria-hidden='true'></i></a></u>");

                    $('.subs').sortable({
                        placeholder: "ui-state-highlight",
                    connectWith: ".secondlast",
                    });
                });

                $(document).on('click', 'span.editable', function(e){
                    e.stopPropagation();
                });

                $(document).on('click', 'ul.subs a.removecolumn', function(e){
                    e.stopPropagation();
                    if ($(this).parent().find('li').length < 1) {
                        $(this).parent().remove();
                    }
                });

                $('.front').find('a').each(function(){
                    me = $(this);
                    if (me.attr('href') != undefined) {
                        if(me.attr('href').indexOf("#") < 0){
                            me.attr('href', 'javascript:void(0);');
                        }
                    }
                });

                //Capture Initial List
                  var initialinsider = $('#currentsections #nav1').html();
                  var initial = $('#currentsections #nav').html();
                  //Restore default list
                  $('body').on('click', '.restore', function(){
                      location.reload();
                      // $('#currentsections #nav1').html( initialinsider);
                      // $('#currentsections #nav').html(initial);
                      // $('.cmsupdatebtn').hide();
                      // $('.restore').hide();
                      // $(this).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                  });

                var oldfilename ='nothing';
                var newfilename ='nothing';
                var uniquename ='nothing';
                var imgext;
                var files = {};
                var newimage = 0;
                $('.cmsfront .cmsedit  figure img').each(function(index, el) {
                    uniquename = 'img'+index+Math.floor((Math.random() * 100) + 1)+"-"+$(this).parents('.cmsedit').attr('basesectionid');
                    figure = $(this).parents('figure');
                    $(this).removeAttr('uploading');
                    img = $(this).parent().html();
                    var bsid = $(this).parents('.cmsedit').attr('basesectionid');

                    // if (bsid == 7 || bsid == 8 || bsid == 9 || bsid == 10 || bsid == 11 || bsid == 12 || bsid == 13 || bsid == 137 || bsid == 147 || bsid == 194 || bsid == 198 || bsid == 196 || bsid == 200) {
                    if (bsid != 0 && bsid != 13) {
                        if (figure.context.currentSrc != '') {
                            // uniquename = '';
                            // var index = figure.context.currentSrc.lastIndexOf("/") + 1;
                            // var filename = figure.context.currentSrc.substr(index);
                            // var res = filename.split(".");
                            // uniquename = res[0];
                        }
// console.log(filename);
// console.log($(this).parents('.cmsedit').attr('basesectionid'));
                        html = '<label for="'+uniquename+'" id="'+uniquename+'" class="lblimgpreviewable" data-toggle="modal" data-target="#cropper-div">'+img+'</label>';
                        figure.after('<?php echo trim(preg_replace('/\s+/', ' ', form_open_multipart("admin/file/upload/'+uniquename+'", array("class" => "fileupload", "id" => "form'+uniquename+'")))); ?><input type="file" name="uploadable" id="'+uniquename+'" class="imgpreviewable" style="display:none;">');
                    } else {
                        html = '<label for="'+uniquename+'">'+img+'</label>';
                        figure.after('<?php echo trim(preg_replace('/\s+/', ' ', form_open_multipart("admin/file/upload/'+uniquename+'", array("class" => "fileupload", "id" => "form'+uniquename+'")))); ?><input type="file" name="uploadable" id="'+uniquename+'" class="imgpreviewable" style="display:none;">');
                    }
                    figure.html(html);
                });

                $('.cmsupdatebtn').click(function(e){
                    me = $(this);
                    sectionid = me.parents('.cmsedit').attr('sectionid');
                    isgallery = $(this).parents('.cmsedit').find('.galleryimages');
                    isslider = $(this).parents('.cmsedit').find('.slidelist');
                    videogallery = $(this).parents('.cmsedit').find('.video-gallery');
                    if (videogallery.html() != undefined) {
                            me = $(this);
                            sectionid = me.parents('.cmsedit').attr('sectionid');
                            var captions     = {},
                                paths = {},
                                descriptions = {};
                            $('.front .video-gallery .imgbox').each(function(index, el) {
                                var id = this.id;
                                path = '';
                                if ($(this).find('figure').html() != undefined) {
                                    path = $(this).find('figure').html();
                                }
                                paths[id] = path;
                                phototitle = '';
                                if ($(this).find('.phototitle').html() != undefined) {
                                    phototitle = $(this).find('.phototitle').html();
                                }
                                captions[id] = phototitle;
                                photodescription = '';
                                if ($(this).find('.photodescription').html() != undefined) {
                                    photodescription = $(this).find('.photodescription').html();
                                }
                                descriptions[id] = photodescription;
                            });
                            $.ajax({
                                data: {'section_id':sectionid, 'path':paths, 'captions':captions, 'descriptions':descriptions, 'language_slug':language_slug},
                                type: 'POST',
                                url: base_url+'gallery/videos/save',
                                success: function(data, textStatus, jqXHR){
                                    htmltosave = me.parents('.cmsedit').find('.contentbox').html();
                                    $.ajax({
                                        data: {'section_id':sectionid, 'section_html':htmltosave, 'language_slug':language_slug},
                                        type: 'POST',
                                        url: base_url+'page/htmltosave',
                                        success: function(data, textStatus, jqXHR){
                                            location.reload();
                                        }
                                    });
                                }
                            });
                            $('.cmsupdatebtn').hide();
                            me.parents('.cmsedit').find('.cmseditbtn').trigger('click');

                    } else if (isgallery.html() == undefined && isslider.html() == undefined) {
                        uploadto = oldfilename.split(/[^/]*$/.exec(oldfilename)[0])[0];
                        me =$(this);
                        var data = $(this).parents('.cmsedit').find('.navlinks').sortable('serialize');
                        if (data.length > 0) {
                            var flagHtmlToSave = true;
                            position = 'header';
                            if($(this).parents('.cmsedit').find('#footernav').html() !=undefined){
                                position = 'footer';
                            }
                            if($(this).parents('.cmsedit').find('.topnav').html() !=undefined){
                                position = 'top';
                            }
                            if ($(this).parents('.cmsedit').find('#menu2header').html() != undefined && $(this).parents('.cmsedit').find('#menu2nav').html() != undefined) {
                                flagHtmlToSave = false;
                                var email = $("#menu2header").find('#email').text();
                                $("#menu2header").find('#email').attr("href", 'mailto:'+email);
                                var telephone = $("#menu2header").find('#telephone').text();
                                $("#menu2header").find('#telephone').attr("href", 'tel:'+telephone.replace(/ /g, ''));
                                var header = $(this).parents('.cmsedit').find('#menu2header').html();
                                var nav = $(this).parents('.cmsedit').find('#menu2nav div.navbar-header').html();
                                $.ajax({
                                    data: {'header': header, 'nav':nav, 'language_slug':language_slug, section_id:20},
                                    type: 'POST',
                                    url: base_url+'menu/menu2',
                                    success: function(data, textStatus, jqXHR){
                                    }
                                });
                            }

                            if ($(this).parents('.cmsedit').find('#menu3header').html() != undefined && $(this).parents('.cmsedit').find('#menu3nav').html() != undefined) {
                                flagHtmlToSave = false;
                                var email = $("#menu3header").find('#email').text();
                                $("#menu3header").find('#email').attr("href", 'mailto:'+email);
                                var telephone = $("#menu3header").find('#telephone').text();
                                $("#menu3header").find('#telephone').attr("href", 'tel:'+telephone.replace(/ /g, ''));
                                var header = $(this).parents('.cmsedit').find('#menu3header').html();
                                var nav = $(this).parents('.cmsedit').find('#menu3nav div.navbar-header').html();
                                $.ajax({
                                    data: {'header': header, 'nav':nav, 'language_slug':language_slug, section_id:34},
                                    type: 'POST',
                                    url: base_url+'menu/menu2',
                                    success: function(data, textStatus, jqXHR){
                                    }
                                });
                            }
                            $.ajax({
                                data: data,
                                type: 'POST',
                                url: base_url+'pages/resort/'+position,
                                success: function(data, textStatus, jqXHR){
                                    //for dynamic footer nav
                                    beforenav = me.parents('.cmsedit').find('#footersection').find('#footernav').html();
                                    me.parents('.cmsedit').find('#footersection').find('#footernav').html('');
                                    htmltosave = me.parents('.cmsedit').find('section .container').html();
                                    if (htmltosave===undefined) {
                                        htmltosave = me.parents('.cmsedit').find('.contentbox').html();
                                    }
                                    if (flagHtmlToSave == true) {
                                        $.ajax({
                                            data: {'section_id':sectionid, 'section_html':htmltosave, 'language_slug':language_slug},
                                            type: 'POST',
                                            url: base_url+'page/htmltosave',
                                            success: function(data, textStatus, jqXHR){
                                                $('.cmsupdatebtn').hide();
                                                $('.restore').hide();
                                                me.parents('.cmsedit').find('#footersection').find('#footernav').html(beforenav);
                                            }
                                        });
                                    }

                                    //for submenu sorting
                                    me.parents('.cmsedit').find('.navlinks li.subli').each(function(index, el){
                                        $(this).find('ul.mainsub > li >ul').each(function(pos, el2){
                                            datasubs = $(this).sortable('serialize');
                                            $.ajax({
                                                data: datasubs,
                                                type: 'POST',
                                                url: base_url+'pages/colresort/'+pos,
                                                async:true,
                                                success: function(data, textStatus, jqXHR){
                                                }
                                            });
                                        });
                                    });

                                    if (me.parents('.cmsedit').find('.insider').hasClass('in')) {
                                        me.parents('.cmsedit').find('.cmseditbtn').trigger('click');
                                    }
                                }
                            });
                        }else{
                            if (imgext !=undefined) {
                                nip = uploadto+me.parents('.cmsedit').find('form').find("input[name=uploadable]").attr('id')+'.'+imgext;
                                newimagepath = '{base_url}'+nip.substr(nip.indexOf('/assets/')+1);
                            }
                            htmltobefore = me.parents('.cmsedit').find('section .container').html();

                            beforenav = me.parents('.cmsedit').find('#footersection').find('#footernav').html();
                            beforenews= me.parents('.cmsedit').find('.news').html();
                            me.parents('.cmsedit').find('.news').html('');
                            //for dynamic footer nav
                            me.parents('.cmsedit').find('#footersection').find('#footernav').html('');

                            //backup temporary 64bit image src
                            oldsrc = me.parents('.cmsedit').find('figure').find('img').attr('src');

                            //add new image url to save in section`s html
                            if (imgext !=undefined) {
                            me.parents('.cmsedit').find('figure').find('img').attr('src', newimagepath);
                            }
                            imgwithlable = me.parents('.cmsedit').find('figure').html();
                            imgonly = me.parents('.cmsedit').find('figure').find('label').html();
                            me.parents('.cmsedit').find('figure').html(imgonly);
                            htmltosave = me.parents('.cmsedit').find('section .container').html();
                            if (htmltobefore===undefined) {
                                htmltosave = me.parents('.cmsedit').find('.contentbox').html();
                            }
                            htmltosave = removeElements(htmltosave, 'form.fileupload');
                            me.parents('.cmsedit').find('figure').html(imgwithlable);
                            me.parents('.cmsedit').find('#footersection').find('#footernav').html(beforenav);
                            me.parents('.cmsedit').find('.news').html(beforenews)

                            if (oldsrc !== undefined) {
                                me.parents('.cmsedit').find('figure').find('img').attr('src', oldsrc);
                            }
                            $.ajax({
                                data: {'section_id':sectionid, 'section_html':htmltosave, 'language_slug':language_slug},
                                type: 'POST',
                                url: base_url+'page/htmltosave',
                                success: function(data, textStatus, jqXHR){
                                    form = me.parents('.cmsedit').find('.fileupload');
                                    form.submit();
                                    location.reload();
                                }
                            });
                            $('.cmsupdatebtn').hide();
                            me.parents('.cmsedit').find('.cmseditbtn').trigger('click');
                        }
                    } else if (isgallery.html() != undefined && isslider.html() == undefined) {
                        // for gallery images --------------------------------------
                            me = $(this);
                            sectionid = me.parents('.cmsedit').attr('sectionid');
                            var images        = {},
                                captions      = {},
                                descriptions  = {},
                                category      = {},
                                links         = {},
                                internal_link = {},
                                sorter        = {},
                                cat_ids       = {};
                            $('.front .galleryimages input.imgpreviewable').each(function(index, el) {
                                if ($(this).val()) {
                                    images[$(this).parent().prev().find('img').attr('id').split('-')[1]] = $(this).attr('id')+'.'+ $(this).val().split('.')[1];
                                }
                                if ($($('#'+$('#idimg').val())).val() && $(this).val() == '') {
                                    images[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] = $('#'+$('#idimg').val()).attr('id')+'.'+ $($('#'+$('#idimg').val())).val().split('.')[1];
                                }

                                phototitle = '';
                                if ($(this).parents('.imgbox').find('.phototitle').find('span').html() == undefined) {
                                phototitle = $(this).parents('.imgbox').find('.phototitle').html();
                                }
                                captions[$(this).parent().prev().find('img').attr('id').split('-')[1]] = phototitle;

                                photodescription = '';
                                if ($(this).parents('.imgbox').find('.photodescription').find('span').html() == undefined) {
                                    photodescription = $(this).parents('.imgbox').find('.photodescription').html();
                                }
                                descriptions[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photodescription;

                                photocat = '';
                                if ($(this).parents('.imgbox').find('.photocat').find('span').html() == undefined) {
                                    photocat = $(this).parents('.imgbox').find('.photocat').html();
                                }
                                category[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photocat;

                                photolink = '';
                                if ($(this).parents('.imgbox').find('.photolink').find('span').html() == undefined) {
                                     photolink = $(this).parents('.imgbox').find('.photolink').html();
                                }
                                links[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photolink;

                                photoilink = '';
                                if ($(this).parents('.imgbox').find('.photoilink').find('span').html() == undefined) {
                                     photoilink = $(this).parents('.imgbox').find('.photoilink').html();
                                }
                                internal_link[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photoilink;


                                sorter[$(this).parent().prev().find('img').attr('id').split('-')[1]] =index;
                                if ($(this).parent().parent().find('select').val() != undefined) {
                                    cat_ids[$(this).parent().prev().find('img').attr('id').split('-')[1]] = $(this).parent().parent().find('select').val();
                                }
                            });
                            $.ajax({
                                data: {'section_id':sectionid, 'images':images, 'captions':captions, 'descriptions':descriptions, 'category':category, 'links':links, 'internal_link':internal_link, 'sorter':sorter, 'cat_ids':cat_ids, 'language_slug':language_slug},
                                type: 'POST',
                                url: base_url+'gallery/photos/save',
                                success: function(data, textStatus, jqXHR){
                                    htmltosave = me.parents('.cmsedit').find('.contentbox').html();
                                    htmltosave = '<div class="contentbox">'+htmltosave+'</div>';
                                    $.ajax({
                                        data: {'section_id':sectionid, 'section_html':htmltosave, 'language_slug':language_slug},
                                        type: 'POST',
                                        url: base_url+'page/htmltosave',
                                        success: function(data, textStatus, jqXHR){
                                             if (newimage) {
                                             // if ($('#'+$('#idimg').val()).parent().parent().hasClass('addnewphoto')) {
                                                location.reload();
                                            }
                                        }
                                    });
                                }
                            });
                            $('.cmsupdatebtn').hide();
                            me.parents('.cmsedit').find('.cmseditbtn').trigger('click');
                    } else {
                        //slider images
                        me            = $(this);
                        sectionid     = me.parents('.cmsedit').attr('sectionid');
                        type          = 0;
                        type          = me.parents('.cmsedit').find('#type').val();
                        slider_speed  = me.parents('.cmsedit').find('#slider_speed').val();
                        var images    = {},
                        sorter        = {},
                        captions      = {},
                        descriptions  = {},
                        phototitle    = {},
                        links         = {};
                        internal_link = {};
                        $('.front .slidelist input.imgpreviewable').each(function(index, el) {
                            if ($(this).val()) {
                                images[$(this).parent().prev().find('img').attr('id').split('-')[1]] = $(this).attr('id')+'.'+ $(this).val().split('.')[1];
                            }

                            phototitle = '';
                            if ($(this).parents('.galleryimages').find('.phototitle').find('span').html() == undefined) {
                            phototitle = $(this).parents('.galleryimages').find('.phototitle').html();

                            }
                            captions[$(this).parent().prev().find('img').attr('id').split('-')[1]] = phototitle;

                            photodescription = '';
                            if ($(this).parents('.galleryimages').find('.photodescription').find('span').html() == undefined) {
                                photodescription = $(this).parents('.galleryimages').find('.photodescription').html();
                            }
                            descriptions[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photodescription;

                            photolink = '';
                            if ($(this).parents('.galleryimages').find('.photolink').find('span').html() == undefined) {
                                 photolink = $(this).parents('.galleryimages').find('.photolink').html();
                            }
                            links[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photolink;

                            photoilink = '';
                            if ($(this).parents('.galleryimages').find('.photoilink').find('span').html() == undefined) {
                                 photoilink = $(this).parents('.galleryimages').find('.photoilink').html();
                            }
                            internal_link[$(this).parent().prev().find('img').attr('id').split('-')[1]] = photoilink;

                            sorter[$(this).parent().prev().find('img').attr('id').split('-')[1]] =index;
                        });
                        if (phototitle == undefined) {
                            $('.front .slidelist .imagescontent').each(function(index, el) {
                                var id     = $(this).find('figcaption').attr('id');
                                allId      = id.split('-');

                                phototitle         = $(this).find('.phototitle').html();
                                captions[allId[1]] = phototitle;

                                photodescription       = $(this).find('.photodescription').html();
                                descriptions[allId[1]] = photodescription;

                                photoilink              = $(this).find('.photoilink').html();
                                internal_link[allId[1]] = photoilink;

                                photolink        = $(this).find('.photolink').html();
                                links[allId[1]]  = photolink;
                                sorter[allId[1]] = index;
                            });
                        }
                        form = me.parents('.cmsedit').find('.fileupload');
                        form.submit();
                        $.ajax({
                            data: {'section_id':sectionid, 'language_slug':language_slug, 'images':images, 'captions':captions, 'descriptions':descriptions, 'links':links, 'internal_link':internal_link, 'sorter':sorter, 'type':type, 'slider_speed':slider_speed},
                            type: 'POST',
                            url: base_url+'slider/photos/save',
                            success: function(data, textStatus, jqXHR){
                                htmltosave = me.parents('.cmsedit').find('.sliderboxes').html();
                                $.ajax({
                                    data: {'section_id':sectionid, 'section_html':htmltosave, 'language_slug':language_slug},
                                    type: 'POST',
                                    url: base_url+'page/htmltosave',
                                    success: function(data, textStatus, jqXHR) {
                                        location.reload();
                                    }
                                });
                            }
                        });
                        $('.cmsupdatebtn').hide();
                        me.parents('.cmsedit').find('.cmseditbtn').trigger('click');
                    }
                });

                //add separater for gallery of icon with desription
                $('.addgroups').on('click', function(){
                    var me = $(this);
                    var newid =  parseInt( $(this).attr('id').split('-')[1] );
                    var sorter = {};
                    var images = {};
                    var captions = {};
                    var sortorder = 0;
                   me.parent().prev().find('input.imgpreviewable').each(function(index, el) {
                        sortorder = index;
                    });
                    var sectionid =me.parents('.cmsedit').attr('sectionid');
                    sorter[newid] = sortorder+1;
                    images[newid] = 'separater-for-group';
                    captions[newid] = $(this).parents('.galleryimages').find('.phototitle').html();
                    $.ajax({
                        data: {'section_id':sectionid, 'images':images, 'captions':captions, 'sorter':sorter, 'language_slug':language_slug},
                        type: 'POST',
                        url: base_url+'gallery/photos/save',
                        success: function(data, textStatus, jqXHR){
                            location.reload();

                        }
                    });
                });

                var removeElements = function(text, selector) {
                    var wrapped = $("<div>" + text + "</div>");
                    wrapped.find(selector).remove();
                    return wrapped.html();
                }

                $('.themeupdate').click(function(e){
                    e.preventDefault();
                    form = $(this).parent().parent();
                    url = form.attr('action');
                    if ($(this).hasClass('savebtn')) {
                        url = url+'/create';
                    }
                    if ($(this).hasClass('deletebtn')) {
                        url = url+'/delete';
                    }

                     if ($(this).hasClass('settheme')) {
                        $(this).parents('form').find('.previewme').trigger('click');
                    }

                    if ($(this).hasClass('resetbtn')) {
                        location.reload();
                    }else{
                        if (confirm("Are you sure you want to proceed?")) {
                            $.ajax({
                                data: form.serialize(),
                                type: 'POST',
                                url: url,
                                success: function(data, textStatus, jqXHR){
                                    if (data!='updated') {
                                        location.reload();
                                    }
                                    location.reload();
                                }
                            });
                        }
                    }

                });

                $('.submitbtn').click(function(e){
                    e.preventDefault();
                    $('#submitform').submit();
                });

                $('form.fileupload').on('submit', function(e){
                    e.preventDefault();
                    // START A LOADING SPINNER HERE

                    // Create a formdata object and add the files
                    var data = new FormData();
                    id = $(this).find('.imgpreviewable').attr('id');
                    if (files[0] != undefined) {
                        data.append(0, document.getElementById(id).files[0]);
                    }
                    // data.append(0, document.getElementById(id).files[0]);
                    url = $(this).attr('action');
                    me = $(this);
                    $.ajax({
                        url: url+'?files',
                        type: 'POST',
                        data: data,
                        async: false,
                        cache: false,
                        dataType: 'json',
                        processData: false, // Don't process the files
                        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR){
                        if(typeof data.error !== 'undefined'){
                            // Handle errors here
                        }else{
                            me.parents('.cmsedit').find('.cmsupdatebtn').fadeOut();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        // Handle errors here
                        console.log('ERRORS: ' + textStatus);
                        // STOP LOADING SPINNER
                    }

                    });
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        imgext = input.files[0].name.split('.')[1];
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            // img = $(input).parent().prev().find('img');

                            if ( $(input).hasClass('hide-file-input') ) {
                                img = $(input).prev().find('img');
                            }else{
                                img = $(input).parent().prev().find('img');
                            }
                            var src = e.target.result;

                            img.fadeOut(150, function(){
                                oldsrc = $(this).attr('src');
                                if ($(this).parent().parent().parent().hasClass('addnewphoto')) {
                                    $(this).parents('.cmsedit ').find('.cmsupdatebtn').trigger('click');
                                    newimage = 1;
                                }
                                if ($(this).attr('uploading') != 1) {
                                    $(this).attr('uploading', 1);
                                    $(this).attr('oldsrc', oldsrc);
                                }
                                $(this).attr('src', e.target.result);
                                oldfilename = $(this).attr('oldsrc');
                                uploadto = oldfilename.split(/[^/]*$/.exec(oldfilename)[0])[0];
                                newfilename = /[^/]*$/.exec($(input).val())[0];
                            });
                            img.fadeIn();
                            if (!$(input).parents('.cmsedit').find('.insider').hasClass('in')) {
                                $(input).parents('.cmsedit').find('.cmseditbtn').trigger('click');
                            }
                            $(input).parents('.cmsedit').find('.cmsupdatebtn').fadeIn(100);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }else{
                        $(input).parents('.cmsedit').find('.cmsupdatebtn').fadeOut();
                    }
                }

                $(".imgpreviewable").change(function(e){
                    readURL(this);
                    uploadfilename=files[$(this).attr('id').split('-')[1]] = e.target.files;
                });

                $(".lblimgpreviewable").click(function(e){
                    var unq = $(this).attr('id');
                    $('#idimg').val(unq);
                    var sec = unq.split('-');
                    $('#idbasesection').val(sec[1]);
                    if (sec[1] == 12) {
                        $('.image-editor').cropit('previewSize', { width: 800, height: 600 });
                    } else if (sec[1] == 13) {
                        $('.image-editor').cropit('previewSize', { width: 360, height: 300 });
                    } else if (sec[1] == 9) {
                        $('.image-editor').cropit('previewSize', { width: 520, height: 520 });
                    } else if (sec[1] == 194 || sec[1] == 200) {
                        $('.image-editor').cropit('previewSize', { width: 330, height: 300 });
                    } else if (sec[1] == 196) {
                        $('.image-editor').cropit('previewSize', { width: 320, height: 300 });
                    } else if (sec[1] == 198) {
                        $('.image-editor').cropit('previewSize', { width: 163, height: 163 });
                    } else if (sec[1] == 137) {
                        $('.image-editor').cropit('previewSize', { width: 960, height: 95 });
                        $('.image-editor').cropit('exportZoom',2);
                    } else if (sec[1] == 43) {
                        $('.image-editor').cropit('previewSize', { width: 358, height: 500 });
                        // $('.image-editor').cropit('exportZoom',2);
                    } else if (sec[1] == 21 || sec[1] == 22 || sec[1] == 40 || sec[1] == 41 || sec[1] == 37 ||  sec[1] == 38 || sec[1] == 25 || sec[1] == 42) {
                        $('.image-editor').cropit('previewSize', { width: 358, height: 234 });
                    } else if (sec[1] == 23 || sec[1] == 24 || sec[1] == 39) {
                        $('.image-editor').cropit('previewSize', { width: 555, height: 363 });
                    } else if (sec[1] == 6 || sec[1] == 127 || sec[1] == 189 || sec[1] == 216 || sec[1] == 217) {
                        $('.image-editor').cropit('previewSize', { width: 600, height: 320 });
                    }  else if (sec[1] == 35 || sec[1] == 127 || sec[1] == 189 || sec[1] == 216 || sec[1] == 217) {
                        $('.image-editor').cropit('previewSize', { width: 570, height: 600 });
                    }  else if (sec[1] == 36 || sec[1] == 127 || sec[1] == 189 || sec[1] == 216 || sec[1] == 217) {
                        $('.image-editor').cropit('previewSize', { width: 570, height: 600 });
                    } else if (sec[1] == 7 || sec[1] == 8 || sec[1] == 10 || sec[1] == 11) {
                        $('.image-editor').cropit('previewSize', { width: 960, height: 325 });
                        $('.image-editor').cropit('exportZoom',2);
                    } else {
                        $('.image-editor').cropit('previewSize', { width: 600, height: 320 });
                    }
                });

                $(".crpcanvas").click(function(e) {
                    // var src = $('.image-editor').cropit('export');
                    var src = $('.image-editor').cropit('export', {
                        type: 'image/jpeg',
                        quality: 0.9,
                        // originalSize: true,
                    });
                    var id = $('#idimg').val();
                    $('#'+id).find('img').attr('src', src);
                    $('#'+id).find('img').attr('uploading', 1);
                    $('#'+id).val(src);
                    var recordId = 0;
                    if  ($('#'+id).find('img').attr('id') != undefined) {
                         recordId = $('#'+id).find('img').attr('id').match(/\d+/);
                     }
                    var bsid = $('#idbasesection').val();
                    var sectionid = $('#'+id).find('img').data('section-id');
                    var frmData = {
                            'image': src,
                            'base_section_id': bsid,
                            'idimg': id,
                            'record_id' : recordId[0],
                            'section_id' : sectionid
                        };
                    $('#overlayloader').show();
                    var postURL = base_url+'/admin/file/upload2';
                    $.ajax({
                        data: frmData,
                        type: 'POST',
                        url: postURL,
                        success: function(data, textStatus, jqXHR){
                            $('#overlayloader').hide();
                            $('.cmsupdatebtn').show();
                            var url = data.replace(/\\/g, '');
                            var url = url.replace('.', '');
                            var url = url.replace('"', '');
                            var url = url.replace('"', '');
                            $('#'+id).find('img').attr('src', base_url+url);
                            $('#'+id).parents('.cmsedit').find('.cmseditbtn').trigger('click');

                            if ($('#'+id).parent().parent().hasClass('addnewphoto') || $('#'+id).closest('li').children('a').hasClass('removeimage')) {
                                me            = $('#'+$('#idimg').val());
                                sectionid     = me.parents('.cmsedit').attr('sectionid');
                                type          = 0;
                                type          = me.parents('.cmsedit').find('#type').val();
                                slider_speed  = me.parents('.cmsedit').find('#slider_speed').val();
                                var images    = {},
                                sorter        = {},
                                captions      = {},
                                descriptions  = {},
                                phototitle    = {},
                                links         = {};
                                internal_link = {};
                                cat_ids       = {};
                                $('.front .slidelist input.imgpreviewable').each(function(index, el) {
                                    if ($($('#'+$('#idimg').val())).val()) {
                                        images[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] = $($('#'+$('#idimg').val())).attr('id')+'.'+ $($('#'+$('#idimg').val())).val().split('.')[1];
                                    }

                                    sorter[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] =index;
                                    if ($($('#'+$('#idimg').val())).parents('.galleryimages').find('select').val() != undefined) {
                                        cat_ids[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] = $('#'+$('#idimg').val()).parents('.addnewphoto').find('select').val();
                                    }
                                });

                                $('.front .galleryimages input.imgpreviewable').each(function(index, el) {
                                    if ($($('#'+$('#idimg').val())).val()) {
                                        images[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] = $($('#'+$('#idimg').val())).attr('id')+'.'+ $($('#'+$('#idimg').val())).val().split('.')[1];
                                    }

                                    sorter[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] =index;
                                    if ($($('#'+$('#idimg').val())).parents('.galleryimages').find('select').val() != undefined) {
                                        cat_ids[$('#'+$('#idimg').val()).find('img').attr('id').split('-')[1]] = $('#'+$('#idimg').val()).parents('.addnewphoto').find('select').val();
                                    }
                                });
                                if (bsid == 12 || bsid == 13 || bsid == 147 || bsid == 21 || bsid == 22 || bsid == 23 || bsid == 24 || bsid == 25 || bsid == 26 || bsid == 27 || bsid == 28 || bsid == 29 || bsid == 30 || bsid == 31 || bsid == 32 || bsid == 33 || bsid == 34 || bsid == 35 || bsid == 36 || bsid == 37 || bsid == 38 || bsid == 39 || bsid == 40 || bsid == 41 || bsid == 42 || bsid == 43 || bsid == 194 || bsid == 196 || bsid == 198 || bsid == 200) {
                                    var photoUrl = base_url+'gallery/photos/save';
                                } else {
                                    var photoUrl = base_url+'slider/photos/save';
                                }
                                $.ajax({
                                    data: {'section_id':sectionid, 'language_slug':language_slug, 'images':images, 'captions':captions, 'descriptions':descriptions, 'links':links, 'internal_link':internal_link, 'sorter':sorter, 'type':type, 'slider_speed':slider_speed, 'cat_ids':cat_ids},
                                    type: 'POST',
                                    url: photoUrl,
                                    success: function(data, textStatus, jqXHR){
                                        htmltosave = me.parents('.cmsedit').find('.sliderboxes').html();
                                        htmltosave = (htmltosave == undefined) ? '' : htmltosave;
                                        $.ajax({
                                            data: {'section_id':sectionid, 'section_html':htmltosave, 'language_slug':language_slug},
                                            type: 'POST',
                                            url: base_url+'page/htmltosave',
                                            success: function(data, textStatus, jqXHR) {
                                                location.reload();
                                            }
                                        });
                                    }
                                });
                                $('.cmsupdatebtn').hide();
                                return false;
                            }
                        }
                    });
                    $('#cropper-div').modal('hide');
                });

                urltype = "<?php echo isset($urltype) ? $urltype : ''; ?>";

                if ( urltype != ''){
                    sendFilterPaginate(1);
                }
                $('#search').click(function(e){
                    e.preventDefault();
                    $('.table-contianer').slideUp();
                    sendFilterPaginate(1);
                    $('.table-contianer').slideDown('slow');
                });

                if ($('input[name=childholder]').val() !='') {
                    $(".customerChildren").html($('input[name=childholder]').val());
                }

            });

            $('#customerForm').on('submit', function(e){
                $('input[name=childholder]').val($(".customerChildren").html());
            });

            $('.addCustomerChild').on('click', function(e){
                key = $('.field-sep').last().attr('key');
                if(key==undefined){
                    key=0;
                }else{
                    key=parseInt(key)+1;
                }

                html =$('.customerChildren').html();

                html += '<label for="child_first_name'+key+'" class="required"><span class="col-lg-2">First name</span><input type="text" name="child['+key+'][first_name]" id="child_first_name'+key+'" value="" placeholder="<?php echo $this->lang->line('first_name_placeholder'); ?>" required autocomplete="off" maxlength="255" class ="form-control col-lg-10"></label><label for="child_last_name'+key+'" class="required"><span class="col-lg-2">Last name</span><input type="text" name="child['+key+'][last_name]" id="child_last_name'+key+'" value="" placeholder="<?php echo $this->lang->line('last_name_placeholder'); ?>" required autocomplete="off" maxlength="255" class ="form-control col-lg-10"></label><label for="child_dob'+key+'"><span class="col-lg-2">Date of birth</span><input type="text" name="child['+key+'][dob]" id="child_dob'+key+'" value="" placeholder="<?php echo $this->lang->line("dob_placeholder"); ?>" required autocomplete="off" maxlength="255" class ="form-control default col-lg-3 datepicker "></label><label><span class="col-lg-2">Gender</span><div class="switch-toggle well col-lg-3"><input id="child_gender_male'+key+'" name="child['+key+'][gender]" type="radio" checked value=1><label for="child_gender_male'+key+'" onclick="" class="col-lg-2">Male</label><input id="child_gender_female'+key+'" name="child['+key+'][gender]" type="radio" value=0><label for="child_gender_female'+key+'" onclick="" class="col-lg-2">Female</label><a class="btn btn-primary"></a></div></label><label for="relation"><span class="col-lg-2">Relation</span><input type="text" name="child['+key+'][relation]" id="relation" value="" placeholder="<?php echo $this->lang->line("relation_placeholder"); ?>" required autocomplete="off" maxlength="255" class ="form-control col-lg-10"></label><label><span class="col-lg-2">Taxable</span><div class="switch-toggle well col-lg-1"><input id="child_taxable_yes'+key+'" name="child['+key+'][taxable]" type="radio" checked value=1><label for="child_taxable_yes'+key+'" onclick="" class="col-lg-2">Yes</label><input id="child_taxable_no'+key+'" name="child['+key+'][taxable]" type="radio" value=0><label for="child_taxable_no'+key+'" onclick="" class="col-lg-2">No</label><a class="btn btn-primary"></a></div></label><label><span class="col-lg-2">Active</span><div class="switch-toggle well col-lg-1"><input id="child_active_yes'+key+'" name="child['+key+'][active]" type="radio" checked value=1><label for="child_active_yes'+key+'" onclick="" class="col-lg-2">Yes</label><input id="child_active_no'+key+'" name="child['+key+'][active]" type="radio" value=0><label for="child_active_no'+key+'" onclick="" class="col-lg-2">No</label><a class="btn btn-primary"></a></div></label><span key="'+key+'" class="field-sep"></span>';


                $(".customerChildren").html(html);
                $('input[name=childholder]').val(html);
            });

            $('#search-form').submit(function(event) {
                event.preventDefault();
                $('.table-contianer').slideUp();
                    sendFilterPaginate(1);
                    $('.table-contianer').slideDown('slow');
            });

            function sendFilterPaginate(paginate){
                apiPostUrl = base_url+"api/"+urltype+"/getlist";

                $.ajax({
                    type: "POST",
                    url: apiPostUrl,
                    data: $('#search-form').serialize()+"&paginate="+paginate
                }).done(function (msg) {
                    var msg = $.parseJSON(msg);
                    if (msg.success) {
                        var html = '<p>No page found please add page.</p>';

                        if (msg.pages) {
                            html = '';
                            currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                            if (urltype=='customer') {
                                $.each(msg.pages,function(customer_id, page) {
                                    activate = (page['active'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['first_name']+'</td>';
                                        html += '<td>'+page['last_name']+'</td>';
                                        html += '<td>'+page['email']+'</td>';
                                        html += '<td>'+page['phone']+'</td>';
                                        html += '<td>'+page['mobile_number']+'</td>';
                                        html += '<td class="action">';
                                            html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+page["id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            html += '<a class="icon-btn col-lg-4" title="<?php echo $this->lang->line('edit');?>" href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+page["id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            }else if(urltype=='page' || urltype=='seo'){
                                showlang = 'en';
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                $.each(msg.pages,function(page_id, page) {
                                    html += '<tr>';
                                        html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';

                                        if (page['children'].length == undefined  ) {
                                            html += '<td class="center nopadding"><a data-toggle="collapse" data-target=".'+page['page_title']+page_id+'"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a></td>';
                                        }else{
                                            html += '<td></td>';
                                        }

                                        html += '<td>'+page['menu_title']+'</td>';
                                        html += '<td>'+page['page_title']+'</td>';
                                        html += '<td>'+page['page_keywords']+'</td>';
                                        html += '<td>'+page['page_description']+'</td>';
                                        html += '<td>'+page['last_update']+'</td>';
                                        html += '<td class="action">';
                                            status = (page['status'] ==0)? 'times':'check';
                                            statustext = (status !== 'check')? "<?php echo $this->lang->line('unpublished');?>": "<?php echo $this->lang->line('published');?>";
                                            statusmodaltext = (status !== 'check')? "<?php echo strtolower($this->lang->line('publish'));?>":"<?php echo strtolower($this->lang->line('unpublish'));?>";
                                            datamessage = (status !== 'check')? "<?php echo $this->lang->line('publish_confirm');?>":"<?php echo $this->lang->line('unpublish_confirm');?>";
                                            iconcolor = (status !== 'check')? 'icon-red':'icon-blue';
                                            publish = (status == 'check')? 0:1;
                                            html += '<a class="icon-btn '+iconcolor+' col-lg-4" href="javascript:void(0);" title="'+statustext+'" data-href="<?php echo base_url(); ?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page_id+'" data-toggle="modal" data-target="#confirm-delete" data-id="publish-ok" data-publish="'+publish+'" data-pageid="'+page_id+'" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+datamessage+'" data-type="confirm"><i class="fa fa-'+status+'" aria-hidden="true"></i></a>';
                                            html += '<a class="icon-btn col-lg-4" title="<?php echo $this->lang->line('edit');?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page_id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a class="icon-btn icon-red col-lg-4" title="<?php echo $this->lang->line('delete');?>" href="javascript:void(0);"  data-href="<?php echo base_url(); ?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page_id+'" data-all="<?php echo base_url(); ?>admin/'+urltype+'/delete/all/'+page_id+'"  data-toggle="modal" data-target="#confirm-delete-page" data-id="activate-ok" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm_all');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                    if (page['children'].length == undefined  ) {
                                            $.each(page["children"], function(child_id, child) {
                                                html += '<tr class="sublist collapse '+page['page_title']+page_id+'">';
                                                html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                                if (child['children'].length == undefined  ) {
                                                    html += '<td class="center nopadding"><a data-toggle="collapse" data-target=".'+child['page_title']+child_id+'"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a></td>';
                                                } else {
                                                    html += '<td></td>';
                                                }
                                                html += '<td>&nbsp;&nbsp;&nbsp;<i class="fa fa-level-down" aria-hidden="true"></i>&nbsp;&nbsp;'+child['menu_title']+'</td>';
                                                html += '<td>'+child['page_title']+'</td>';
                                                html += '<td>'+child['page_keywords']+'</td>';
                                                html += '<td>'+child['page_description']+'</td>';
                                                html += '<td>'+child['last_update']+'</td>';
                                                html += '<td class="action">';
                                                status = (child['status'] ==0)? 'times':'check';
                                                statustext = (status !== 'check')? "<?php echo $this->lang->line('unpublished');?>": "<?php echo $this->lang->line('published');?>";
                                                statusmodaltext = (status !== 'check')? "<?php echo strtolower($this->lang->line('publish'));?>":"<?php echo strtolower($this->lang->line('unpublish'));?>";
                                                datamessage = (status !== 'check')? "<?php echo $this->lang->line('publish_confirm');?>":"<?php echo $this->lang->line('unpublish_confirm');?>";
                                                iconcolor = (status !== 'check')? 'icon-red':'icon-blue';
                                                publish = (status == 'check')? 0:1;
                                                html += '<a class="icon-btn '+iconcolor+' col-lg-4" href="javascript:void(0);" title="'+statustext+'" data-href="<?php echo base_url(); ?>admin/'+urltype+'/delete/'+currentLangCode+'/'+child_id+'" data-toggle="modal" data-target="#confirm-delete" data-id="publish-ok" data-publish="'+publish+'" data-pageid="'+child_id+'" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+datamessage+'" data-type="confirm"><i class="fa fa-'+status+'" aria-hidden="true"></i></a>';
                                                html += '<a class="icon-btn col-lg-4" title="<?php echo $this->lang->line('edit');?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+child_id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                                html += '<a class="icon-btn icon-red col-lg-4" title="<?php echo $this->lang->line('delete');?>" href="javascript:void(0);" data-href="<?php echo base_url(); ?>admin/'+urltype+'/delete/'+currentLangCode+'/'+child_id+'" data-all="<?php echo base_url(); ?>admin/'+urltype+'/delete/all/'+child_id+'"  data-toggle="modal" data-target="#confirm-delete-page" data-id="activate-ok" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm_all');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                                    html += '</td>';
                                                html += '</tr>';

                                                if (child['children'].length == undefined  ) {
                                                     $.each(child["children"], function(gchild_id, gchild) {
                                                        html += '<tr class="subsublist  collapse '+child['page_title']+child_id+'">';
                                                            html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                                            html += '<td></td>';
                                                            html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-level-down" aria-hidden="true"></i>&nbsp;&nbsp; '+gchild['menu_title']+'</td>';
                                                            html += '<td>'+gchild['page_title']+'</td>';
                                                            html += '<td>'+gchild['page_keywords']+'</td>';
                                                            html += '<td>'+gchild['page_description']+'</td>';
                                                            html += '<td>'+gchild['last_update']+'</td>';
                                                            html += '<td class="action">';
                                                                status = (gchild['status'] ==0)? 'times':'check';
                                                                statustext = (status !== 'check')? "<?php echo $this->lang->line('unpublished');?>": "<?php echo $this->lang->line('published');?>";
                                                                statusmodaltext = (status !== 'check')? "<?php echo strtolower($this->lang->line('publish'));?>":"<?php echo strtolower($this->lang->line('unpublish'));?>";
                                                                datamessage = (status !== 'check')? "<?php echo $this->lang->line('publish_confirm');?>":"<?php echo $this->lang->line('unpublish_confirm');?>";
                                                                iconcolor = (status !== 'check')? 'icon-red':'icon-blue';
                                                                publish = (status == 'check')? 0:1;
                                                                html += '<a class="icon-btn '+iconcolor+' col-lg-4" href="javascript:void(0);" title="'+statustext+'" data-href="<?php echo base_url(); ?>admin/'+urltype+'/delete/'+currentLangCode+'/'+gchild_id+'" data-toggle="modal" data-target="#confirm-delete" data-id="publish-ok" data-publish="'+publish+'" data-pageid="'+gchild_id+'" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+datamessage+'" data-type="confirm"><i class="fa fa-'+status+'" aria-hidden="true"></i></a>';
                                                                html += '<a class="icon-btn col-lg-4" title="<?php echo $this->lang->line('edit');?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+gchild_id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                                                html += '<a class="icon-btn icon-red col-lg-4" title="<?php echo $this->lang->line('delete');?>" href="javascript:void(0);" data-href="<?php echo base_url(); ?>admin/'+urltype+'/delete/all/'+gchild_id+'"  data-all="<?php echo base_url(); ?>admin/'+urltype+'/delete/all/'+gchild_id+'"  data-toggle="modal" data-target="#confirm-delete-page" data-id="activate-ok" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm_all');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                                            html += '</td>';
                                                        html += '</tr>';
                                                    });
                                                    // html += '<tr class="form-horizontal   collapse  '+child['page_title']+child_id+'" style="float:none;"><td colspan="8" style="padding: 5px;"></td></tr>';
                                                }

                                             });
                                            // html += '<tr class="form-horizontal" style="float:none;"><td colspan="8" style="padding: 5px;"></td></tr>';
                                        // html += '</tbody>';
                                    }
                                });
                            }else if(urltype=='employee'){
                                $.each(msg.pages,function(customer_id, page) {
                                    activate = (page['active'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['first_name']+'</td>';
                                        html += '<td>'+page['last_name']+'</td>';
                                        html += '<td>'+page['email']+'</td>';
                                        html += '<td>'+page['phone']+'</td>';
                                        html += '<td>'+page['job_title']+'</td>';
                                        html += '<td>'+page['id']+'</td>';
                                        html += '<td class="action">';
                                            html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+page["id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit');?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+page["id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                            html += '</td>';
                                    html += '</tr>';
                                });
                            }else if(urltype=='social'){
                                $.each(msg.pages,function(customer_id, page) {
                                    activate = (page['active'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['name']+'</td>';
                                        html += '<td>'+((page['url'] !==null)? page['url']:'')+'</td>';
                                        html += '<td>'+((page['updated_at'] !==null)? page['updated_at']:'')+'</td>';
                                        html += '<td class="action">';
                                            html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-6" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+page["id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            html += '<a class="icon-btn col-lg-6"  title="<?php echo $this->lang->line('edit');?>" href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            }else if(urltype=='service'){
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                catname = '';
                                count = 0;
                                $.each(msg.pages,function(page_id, page) {
                                    activate     = (page['status'] == 1) ? 0 : 1;
                                    show         = (page['showinfront'] == 1) ? 0 : 1;
                                    is_clubservice = (page['is_clubservice'] == 1) ? 0 : 1;
                                    // is_blocked_for_customer = (page['is_blocked_for_customer'] == 1) ? 0 : 1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    showicon     = (show == 1)? 'eye-slash':'eye';
                                    showtitle    = (show == 1)? " <?php echo $this->lang->line('showinfront');?>":"<?php echo $this->lang->line('hideinfront');?>";
                                    activatetitle   = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    showmessage = (show == 1)? "<?php echo $this->lang->line('show_confirm');?>":"<?php echo $this->lang->line('hide_confirm');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    clubservicemessage = (is_clubservice == 0) ? "<?php echo $this->lang->line('clubservice_deactive');?>" : "<?php echo $this->lang->line('clubservice_active');?>";
                                    clubtitle    = (is_clubservice == 0) ? "<?php echo $this->lang->line('removeclub');?>" : "<?php echo $this->lang->line('addclub');?>";
                                    sactivateicon = (is_clubservice == 1)? 'times':'check';
                                    // lockservicemessage = (is_blocked_for_customer == 0) ? "<?php echo $this->lang->line('service_blocked_active');?>" : "<?php echo $this->lang->line('service_blocked_deactive');?>";
                                    // lockservicetitle    = (is_blocked_for_customer == 0) ? "<?php echo $this->lang->line('addlockservice');?>" : "<?php echo $this->lang->line('removelockservice');?>";
                                    // lockserviceactivateicon = (is_blocked_for_customer == 1) ? 'unlock-alt' : 'lock';
                                    if (count !==0 && page['category'] !== catname) {
                                        html += '<tr class="form-horizontal" style="float:none;"><td colspan="6" style="padding: 5px;"></td></tr>';
                                    }
                                    catname = page['category'];
                                    count++;
                                    html += '<tr>';
                                        html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['name']+'</td>';
                                        html += '<td>'+page['category'];
                                        if (page['subcategory'] != '') {
                                            html += ' / '+page['subcategory'];
                                        }
                                        html += '</td>';
                                        html += '<td>'+page['created_at']+'</td>';
                                        html += '<td class="action">';
                                            // html += '<a href="#" title="'+lockservicetitle+'" class="icon-btn '+lockserviceactivateicon+' col-lg-3" data-href="<?php echo base_url();?>admin/'+urltype+'/service_lock_unlock/'+page["service_id"]+'/'+is_blocked_for_customer+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+lockservicemessage+'" data-type="confirm"><i class="fa fa-'+lockserviceactivateicon+'" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="'+clubtitle+'" class="icon-btn '+sactivateicon+' col-lg-3" data-href="<?php echo base_url();?>admin/'+urltype+'/club/'+page["service_id"]+'/'+is_clubservice+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+clubservicemessage+'" data-type="confirm"><i class="fa fa-'+sactivateicon+'" aria-hidden="true"></i></a>';

                                            html += '<a href="#" title="'+showtitle+'" class="icon-btn '+showicon+' col-lg-3" data-href="<?php echo base_url();?>admin/'+urltype+'/show/'+currentLangCode+'/'+page["service_id"]+'/'+show+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+showmessage+'" data-type="confirm"><i class="fa fa-'+showicon+'" aria-hidden="true"></i></a>';


                                            if (page['status'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-3" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page["service_id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['status'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-3" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-3"  title="<?php echo $this->lang->line('edit');?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page['service_id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-3" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page["service_id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype=='news') {
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                $.each(msg.pages,function(page_id, page) {
                                    activate = (page['active'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    html += '<tr>';
                                        html += '<td class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['header_text']+'</td>';
                                        html += '<td>'+page['publication_date']+'</td>';
                                        html += '<td>'+page['updated_at']+'</td>';
                                        html += '<td class="action">';
                                            if (page['active'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page["id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['active'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit');?>" href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page["id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            }else if(urltype=='newsletter') {
                                $.each(msg.pages,function(page_id, page) {
                                    activate = (page['status'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['header_text']+'</td>';
                                        html += '<td>'+page['subs_number']+'</td>';
                                        html += '<td>'+page['publication_date']+'</td>';
                                        html += '<td class="action">';
                                            // html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page["newsletter_id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                        if (page['edit'] == true) {
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page["newsletter_id"]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                        }
                                            // html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page["newsletter_id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            }else if(urltype=='newsletter_subscriber') {
                                $.each(msg.pages,function(page_id, page) {
                                    activate = (page['status'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['name']+'</td>';
                                        html += '<td>'+page['email']+'</td>';
                                        html += '<td>'+page['created_at']+'</td>';
                                        html += '<td class="action">';
                                        if (page['status'] != 2) {
                                            html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+page["id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                        } else if (page['status'] == 2) {
                                            html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                        }
                                        html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit');?>" href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                        html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+page["id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype == 'category') {
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                $.each(msg.pages, function(page_id, page) {
                                    var cstatus = page['status'];
                                    var status = cstatus.substr(cstatus.length -1);
                                    activate        = (status == 1) ? 0 : 1;
                                    activateicon    = (activate == 1) ? 'times' : 'check';
                                    activatetitle   = (activate == 1) ? "<?php echo $this->lang->line('activate');?>" : "<?php echo $this->lang->line('deactivate');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['name']+'</td>';
                                        html += '<td>'+((page['updated_at'] == null) ? page['created_at'] : page['updated_at'])+'</td>';
                                        html += '<td class="action center">';
                                            if (page['status'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page['category_id']+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['status'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page['category_id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page['category_id']+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype == 'subcategory') {
                                $.each(msg.pages, function(page_id, page) {
                                    activate        = (page['status'] == 1) ? 0 : 1;
                                    activateicon    = (activate == 1) ? 'times':'check';
                                    activatetitle   = (activate == 1) ? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['category']+'</td>';
                                        html += '<td>'+page['subcategory']+'</td>';
                                        html += '<td>'+((page['updated_at'] == null) ? page['created_at'] : page['updated_at'])+'</td>';
                                        html += '<td class="action center">';
                                            if (page['status'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page['sub_category_id']+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['status'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page['sub_category_id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page['sub_category_id']+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype=='openinghours') {
                                $.each(msg.pages,function(page_id, page) {
                                    activate      = (page['is_closed'] == 1) ? 0 : 1;
                                    activateicon  = (activate == 0) ? 'times' : 'check';
                                    activatetitle = (activate == 0) ? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['day']+'</td>';
                                        html += '<td>';
                                        if (page['is_closed'] == 0) {
                                            html += page['description']['opening'];
                                        } else {
                                            html += 'Closed';
                                        }
                                        html += '</td>';
                                        html += '<td>';
                                        if (page['is_closed'] == 0) {
                                            html += page['description']['closing'];
                                        } else {
                                            html += 'Closed';
                                        }
                                        html += '</td>';
                                        html += '<td>';
                                        if (page['is_closed'] == 0) {
                                            html += page['description']['split_opening'];
                                        } else {
                                            html += 'Closed';
                                        }
                                        html += '</td>';
                                        html += '<td>';
                                        if (page['is_closed'] == 0) {
                                            html += page['description']['split_closing'];
                                        } else {
                                            html += 'Closed';
                                        }
                                        html += '</td>';
                                        html += '<td>'+page['last_update']+'</td>';
                                        html += '<td class="action center">';
                                            html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-6" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page['id']+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            html += '<a class="icon-btn col-lg-6 pull-right"   title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype=='holidays') {
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                $.each(msg.pages,function(page_id, page) {
                                    activate = (page['status'] == 1)? 0:1;
                                    activateicon = (activate == 1)? 'times':'check';
                                    activatetitle = (activate == 1)? "<?php echo $this->lang->line('activate');?>":"<?php echo $this->lang->line('deactivate');?>";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['title']+'</td>';
                                        html += '<td>';
                                        if (page['is_closed'] == 0) {
                                            html += page['description']['opening'];
                                        } else {
                                            html += 'Closed';
                                        }
                                        html += '</td>';
                                        html += '<td>';
                                        if (page['is_closed'] == 0) {
                                            html += page['description']['closing'];
                                        } else {
                                            html += 'Closed';
                                        }
                                        html += '</td>';
                                        html += '<td>'+page['description']['split_opening']+'</td>';
                                        html += '<td>'+page['description']['split_closing']+'</td>';
                                        html += '<td>'+page['holiday_date']+'</td>';
                                        html += '<td class="action center">';
                                            if (page['status'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page["id"]+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['status'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage.replace('%a', notcorrect)+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page['id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page["id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype=='employee_booking_plans') {
                                $.each(msg.pages,function(page_id, page) {
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['employee']+'</td>';
                                        html += '<td>'+page['start_date']+'</td>';
                                        html += '<td>'+page['end_date']+'</td>';
                                        html += '<td class="action center">';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+page["id"]+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype == 'attribute') {
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                $.each(msg.pages, function(page_id, page) {
                                    activate        = (page['status'] == 1) ? 0 : 1;
                                    activateicon    = (activate == 1) ? 'times' : 'check';
                                    activatetitle   = (activate == 1) ? "<?php echo $this->lang->line('activate');?>" : "<?php echo $this->lang->line('deactivate');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['name']+'</td>';
                                        html += '<td>'+((page['updated_at'] == null) ? page['created_at'] : page['updated_at'])+'</td>';
                                        html += '<td class="action center">';
                                            if (page['status'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+currentLangCode+'/'+page['attribute_id']+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['status'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+currentLangCode+'/'+page['attribute_id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+currentLangCode+'/'+page['attribute_id']+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype == 'manufacturer') {
                                currentLangCode = "<?php echo $contentlangcode?  $contentlangcode : 'en'; ?>";
                                $.each(msg.pages, function(page_id, page) {
                                    activate        = (page['status'] == 1) ? 0 : 1;
                                    activateicon    = (activate == 1) ? 'times' : 'check';
                                    activatetitle   = (activate == 1) ? "<?php echo $this->lang->line('activate');?>" : "<?php echo $this->lang->line('deactivate');?>";
                                    notcorrect      = "<?php echo $this->lang->line('notcorrect');?>";
                                    notcorrecticon  = "exclamation-triangle";
                                    activatemessage = (activate == 1)? "<?php echo $this->lang->line('activate_confirm');?>":"<?php echo $this->lang->line('deactivate_confirm');?>";
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['name']+'</td>';
                                        html += '<td>'+((page['updated_at'] == null) ? page['created_at'] : page['updated_at'])+'</td>';
                                        html += '<td class="action center">';
                                            if (page['status'] != 2) {
                                                html += '<a href="#" title="'+activatetitle+'" class="icon-btn '+activateicon+' col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/activate/'+page['manufacturer_id']+'/'+activate+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="'+activatemessage+'" data-type="confirm"><i class="fa fa-'+activateicon+'" aria-hidden="true"></i></a>';
                                            } else if (page['status'] == 2) {
                                                html += '<a href="#" title="'+notcorrect+'" class="icon-btn '+notcorrecticon+' col-lg-4" data-title="<?php echo $this->lang->line('notcorrect');?>" data-message="'+activatemessage+'" ><i class="fa fa-'+notcorrecticon+'" aria-hidden="true"></i></a>';
                                            }
                                            html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('edit'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/edit/'+page['manufacturer_id']+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                                            html += '<a href="#" title="<?php echo $this->lang->line('delete'); ?>" class="icon-btn icon-red col-lg-4" data-href="<?php echo base_url();?>admin/'+urltype+'/delete/'+page['manufacturer_id']+'" data-id="activate-ok" data-toggle="modal" data-target="#confirm-delete" data-title="<?php echo $this->lang->line('confirm');?>" data-message="<?php echo $this->lang->line('delete_confirm');?>" data-type="confirm"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            } else if(urltype == 'activity_logs') {
                                $.each(msg.pages, function(page_id, page) {
                                    html += '<tr>';
                                        html += '<td width="40" class="checkbox-container"><input type="checkbox" id="1" /><label for=""></label></td>';
                                        html += '<td>'+page['section']+'</td>';
                                        html += '<td>'+page['action']+'</td>';
                                        html += '<td>'+page['action_by_name']+'</td>';
                                        html += '<td>'+page['date_time']+'</td>';
                                        html += '<td class="action center">';
                                        html += '<a class="icon-btn col-lg-4"  title="<?php echo $this->lang->line('view'); ?>"  href="<?php echo base_url(); ?>admin/'+urltype+'/view/'+page['id']+'"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                                        html += '</td>';
                                    html += '</tr>';
                                });
                            }
                        }

                        $('.table-contianer table tbody').html('');
                        $('.table-contianer table tbody').html(html);
                        $('.pagination').html(msg.next_previous_pages);
                        $('.pagination li a').on('click', function (e) {
                            var page = $(this).attr('page_num');
                            e.preventDefault();
                            $('.table-contianer').slideUp();
                            sendFilterPaginate(page);
                            $('.table-contianer').slideDown('slow');
                        });
                    }
                });
            }

<?php
$datetimeformat = isset($app_settings['dateformat'])? $app_settings['dateformat']:'d-m-Y';
$d = explode('-', strtolower($datetimeformat));
$datetimeformat = $d[0].$d[0].'-'.$d[1].$d[1].'-'.$d[2].$d[2];
?>
var datetimeformat = "<?php echo $datetimeformat;?>";
$( "#publication_date" ).datepicker({
    firstDay : 1,
    dateFormat: datetimeformat
});
$('body').on('focus',".datepicker", function(){
    $(this).datepicker({
        firstDay : 1,
        dateFormat: datetimeformat
    });
})

$('.cktxteditor').each( function () {
    CKEDITOR.replace( this.id , {
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
});
$('.timeselector').timepicker({
    showPeriodLabels: false,
});
$('.addbreak').on('click', function(e){
    e.preventDefault();
    unique = Math.floor(Math.random() * 100) + 1 ;
    html = $('.breaks-table tbody tr:last-child').html();
    html = html.replace(/\{unique}/g, unique);
    $('.breaks-table tbody').prepend('<tr>'+html+'</tr>');
});

$('.addoffday').on('click', function(e){
    e.preventDefault();
    unique = Math.floor(Math.random() * 100) + 1 ;
    html = $('.offdays-table tbody tr:last-child').html();
    html = html.replace(/\{unique}/g, unique);
    $('.offdays-table tbody').prepend('<tr>'+html+'</tr>');
});

$('.addworkplan').on('click', function(e){
    e.preventDefault();
    // var new_row = $('#working-plan').clone();
    // unique = Math.floor(Math.random() * 100) + 1 ;
    // new_row.html(function(i, oldHTML) {
    //     var d = oldHTML.replace(/\working_yesmonday/g, 'working_yesmonday'+unique);
    //     var b = d.replace(/\working_nomonday/g, 'working_nomonday'+unique);
    //     var d = b.replace("fa-plus", "fa-remove");
    //     return d.replace("addworkplan", "removeworkplan");
    // });
    // var str = new_row.replace("fa-plus", "fa-remove");
    // console.log(str);
    // $('.workplanclone').append(new_row);
    // html   = data.toString().replace('active_yesmonday', 'active_yesmonday2');
    // $('.workplanclone').append(html);
    var postURL = base_url+'admin/employee/workplan';
    var selectedWeeks = $('#week0').val();
    var frmData = {
        'selected_weeks': selectedWeeks
    };
    $.ajax({
        data: frmData,
        type: 'POST',
        url: postURL,
        success: function(data, textStatus, jqXHR){
            $('.workplanclone').append(data);
        }
    });
});

$(document).on('click', '.removebreak', function(e){
    e.preventDefault();
    if (confirm("<?php echo $this->lang->line('remove_confirm');?>")) {
        $(this).parents('tr').remove();
    }
});

$('#nav li').each(function(index, val) {
    if ($(this).hasClass('subli')) {
        if ($(this).offset().left > ($(window).width()/2) ) {
            $(this).find('.dropdown-menu').removeClass('dropdown-menu').addClass('dropdown-menu dropdown-menu-right');
        }
    }
});

$('.socialicon').html('');
html = '';
<?php if (isset($socialicons)) {
 foreach ($socialicons as $key => $value) {?>
html += '<a href="<?php echo $value->url;?>" title="<?php echo $value->name;?>" class="<?php echo $value->template;?>"><img src="<?php echo base_url();?>assets/images/blank.png"></a>';
<?php }}?>
$('.socialicon').html(html);

$('.opening').html('');
html = 'Closed';
<?php if (isset($description->opening) && $description->opening !='' ) {?>
html = "<?php echo $description->opening;?>";
//
<?php //if (isset($description->opening) && $description->opening !='' ) {?>
// html = "<?php echo $description->opening;?>";
<?php }?>
$('.opening').html(html);

$('.servicecontent').on('change', function(){
     $.ajax({
        data: {'page_id':pageid, 'service_id':$(this).val()},
        type: 'POST',
        url: base_url+'service/content',
        success: function(data, textStatus, jqXHR){
            location.reload();
        }
    });
});

$('.workingchecked').on('click', function(){
    if ($(this).is(':checked')){
      tr = $(this).parents('tr');
      tr.find('td').each(function(index, el) {
        $(this).find('input[type=text]').val(($(this).find('input[type=text]').attr('val') != undefined)? $(this).find('input').attr('val'):'');
        });
    }
});

$('.workingunchecked').on('click', function(){
    if ($(this).is(':checked')){
      tr = $(this).parents('tr');
      tr.find('td').each(function(index, el) {
             $(this).find('input[type=text]').val("");
        if (index == 1) {
             $(this).find('input[type=text]').val("<?php echo $this->lang->line('not_working');?>");
        }
        });
    }
});

$('.urlforabove').on('keyup', function(){
    $(this).prev().attr('href', $(this).val() );
    $(this).prev().attr('oldhref', $(this).val() );
     if (!$(this).parents('.cmsedit ').find('.insider').hasClass('in') ) {
            $(this).parents('.cmsedit ').find('.cmseditbtn').trigger('click');
            $(this).parents('.cmsedit ').find('.cmsupdatebtn').show();
    }
});

$('.urlforabove').each(function(index, el) {
    $(this).val($(this).prev().attr('oldhref'));
});
$('.pricelist2-list').html($('.frompricelist2-list').html());

$('.selectsection').on('mouseup', function(){
    $('.selectsection').removeClass('active');
    $(this).addClass('active');
    addsec = $(this).parents('.cmsedit').find('.addsectionbtn');
    addsec.attr('sectionid', $(this).attr('sectionid')).removeClass('disabled');

});
$('#addsection div.cmsedit').each(function(index, el) {
    if ($.trim($(this).find('.fade').html())=="") {
        $(this).addClass('deactivated');
        $(this).find('.btn').addClass('disabled');
    }

    $(this).find('.addsectionbtn').attr('sectionid', $(this).find('.fade>a').attr('sectionid') );
    $(this).find('.fade>a:first-child').addClass('active');
});
/*$('figcaption').on('mousedown', function(e){
    e.stopPropagation();
});*/
</script>
<!-- Important Owl stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.css'); ?>">

<!-- Default Theme -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.css'); ?>">

<!-- Include js plugin -->
<script src="<?php echo base_url('assets/js/modernizr.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dlmenu.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>"></script>
<script>
    $(function() {
        // $( '#dl-menu' ).dlmenu();
        $('#timepicker').timepicker();
    });
</script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/admin/css/custom2.css'); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('assets/admin/css/custom.css'); ?>" />
<!-- <script src="<?php echo base_url('assets/js/cropper.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/vue.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/cropper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script> -->
<script src="<?php echo base_url('assets/cropping/js/jquery.cropit.js'); ?>"></script>
    <script>
      $(function() {
        $('.image-editor').cropit({
            smallImage : 'stretch',
            onImageError: function(error) {
                // console.log('Image error');
                alert(error);
            }
        });

        $('.rotate-cw').click(function() {
          $('.image-editor').cropit('rotateCW');
        });

        $('.rotate-ccw').click(function() {
          $('.image-editor').cropit('rotateCCW');
        });

        $('.export').click(function() {
            var imageData = $('.image-editor').cropit('export');
            window.open(imageData);
        });

        $('.yearweeks span').click(function () {
            var div = $(this).parent('div').attr('id');
            var clickedId = $(this).data('value');
            var tflag = true;
            $('.yearweeks span').each(function(e) {
                if ($(this).data('value') == clickedId &&
                    $(this).hasClass('active') &&
                    $(this).parent('div').attr('id') != div) {
                    tflag = false;
                }
            });
            if (tflag) {
                $( this ).toggleClass( "active");
                var value = '';
                $('#'+div+' span').each(function(e){
                    if ($(this).hasClass('active')) {
                        value += (!value ? '' : ',') + $(this).data('value');
                    }
                });
                var id = div.replace ( /[^\d.]/g, '' );
                $('#week'+id).val(value);
            }
        });

        $('.removeworkplan').on('click', function(e){
            e.preventDefault();
            $(this).parents('.yearweeks').remove();
        });
        $('#pos_login').click(function(event){
            event.preventDefault();
            var newForm = jQuery('<form>', {
                'action': '<?php echo $this->config->item('pos_url'); ?>/authenticate',
                'target': '_blank',
                'method': 'POST'
            }).append($('<input>', {
                'name': 'user_id',
                'value': '<?php echo $this->session->userdata('auth_data')->user_id?>',
                'type': 'hidden'
            }));
            newForm.appendTo(document.body).submit();
        });
      });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
</body>
</html>
