<style>
.top-header {
    /*background: #596c7a !important;*/
    position: relative;
    width: 100%;
    top: 0px;
    z-index: 999;
    min-height: 30px;
}
.navbar-default .navbar-nav > li > a {
    color: #596c7a !important;
}
.top-header ul {
    padding: 0px;
    display: inline-block;
    margin: 0px;
    vertical-align: top;
}
.top-header ul li {
    list-style-type: none;
    padding: 0px 15px;
    float: left;
    border-left: 1px solid #72828e;
    border-right: 0px;
    margin-top: 7px;
}
/*.front .navbar-nav > li {
    border-right: 1px solid #72828e;
}*/
.front #menu2nav .navbar-nav > li {
    border-right: 0px !important;
}
.front #menu2nav .navbar-nav > li:last-child {
    padding-right: 0px;
}
#menu2nav .navbar-navigation > li:first-child {
    border-left: 0px !important;
}
.top-header ul li:first-child {
    border-left: 0px;
    padding-left: 0px;
}
.top-header ul li:last-child {
    border-right: 0px;
}
.top-header ul li a {
    color: #fff;
    font-size: 12px;
    font-weight: 400;
    text-decoration: none;
}
.top-header ul li {
    color: #fff;
    font-size: 12px;
    font-weight: 400;
    text-decoration: none;
}
.top-header .member-btn {
    background: #a18350;
    color: #f4f4f4;
    font-size: 14px;
    font-weight: 600;
    width: 234px;
    display: inline-block;
    text-align: center;
    height: 30px;
    padding-top: 4px;
}
.top-header .member-btn:hover {
    text-decoration: none;
}
.navbar {
    background: rgba(255,255,255, 0.85) !important;
    border: 0px;
    border-radius: 0px;
    margin-bottom: 0px;
    position: absolute;
    width: 100%;
    z-index: 99;
    padding: 5px 0px 7px;
    margin-top: 0px;
}
.navbar.sticky {
    position: fixed;
    top: 0px;
    background: #fff;
    margin-top: 0px;
    z-index: 99999;
    border-bottom: 2px solid #596c7a;
    transition: all 0.3s;
}
.front .navbar-default .navbar-navigation>li>a:focus, .front .navbar-default .navbar-navigation>li>a:hover {
    color: #a18350;
}
.navbar-default .container {
    position: relative;
}
.front .navbar-navigation {
    padding: 0px;
    margin-top: 0px;
    display: inline-block !important;
    width: auto;
    min-height: auto;
    position: inherit;
}
.nav>li>a:focus, .nav>li>a:hover {
    background: none;
}
.front .navbar-navigation > li {
    border-left: 1px solid #bdbdbe;
    padding: 0px 20px;
    float: left;
}
.front .navbar-navigation > li > a {
    padding-top: 0px;
    padding-bottom: 0px;
}
.front .navbar-navigation > li.dropdown {
    position: relative !important;
}
.front .navbar-navigation > li:first-child {
    border-left: 0px;
}
.front .navbar-default .navbar-navigation > li > a {
    font-size: 14px;
    font-weight: 400;
    color: #596c7a;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    padding: 0px;
    line-height: 14px;
}
.front .navbar-default .navbar-navigation > li.dropdown:hover .dropdown-menu {
    display: block;
}
.front .navbar-navigation > li > .dropdown-menu {
    background: none;
    padding-top: 20px;
    padding-left: 10px;
    border: 0px;
    box-shadow: 0px 0px 0px;
    width: auto;
    top: 94%;
    z-index: 99999999;
}
.front .navbar-navigation > li > .dropdown-menu li {
    background: #fff;
    color: #596c7a;
    font-size: 14px;
    font-weight: 600;
    padding: 0px 0px;
    padding-left: 10px;
    padding-right: 10px;
}
.front .navbar-navigation > li > .dropdown-menu li a {
    border-top: 1px solid #dbdbdd;
    padding: 10px;
    color: #586b79;
}
.front .navbar-navigation > li > .dropdown-menu li:first-child a {
    border-top: 0px;
}
.front .navbar-navigation > li > .dropdown-menu li:hover {
    background: #fff;
}
.front .navbar-navigation > li > .dropdown-menu li:hover a {
    color: #a18350;
    background: none;
}
.front .cmsedit .navbar {
    position: relative;
    z-index: 99;
}
a.cmsmovebtn {
    z-index: 999;
}
.front .navbar-navigation .dropdown a:after {
    content: '';
}
.navbar-nav > li {
    position: inherit;
}
.navbar-nav > li i {
    position: absolute;
    top: 18px;
    right: 10px;
}
.service-menu {
    background: #fff;
    box-shadow: 0 5px 4px #383331;
    padding: 20px ;
}

.service-menu h4 {
    margin: 0px;
    padding: 10px;
    border: 1px solid #ccc;
    text-align: center;
    border-left: 0px;
    font-size: 18px;
    font-weight: bold;
}
.navbar-nav > li > .dropdown-menu {
    border-radius: 0px 0px 0px;
}
.service-menu ul {
    list-style-type: none;
    margin: 0px;
    padding: 0px 0px 0px 5px;
}
.service-menu ul li ul {
    padding-bottom: 10px;
}
.service-menu .col-sm-3 {
    padding: 0px;
}
.service-menu ul li ul:after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
}
.service-menu ul li {
    padding: 5px 0px 0px;
    border: 0px !important;
    width: 100% !important;
}
.service-menu ul li {
    padding: 5px 0px 0px;
    border: 0px !important;
    width: 100%;
}
.service-menu ul.menu-service-ul li {
    width: 32% !important;
    display: inline-block;
    vertical-align: top;
    padding: 0px !important;
    margin-bottom: 7px;
    background: #fff;
    margin-right: 5px;
    display: flex;
    align-items: center;
}
.service-menu ul.menu-service-ul li a {
    display: flex;
    align-items: center;
    width: 100%;
    padding: 8px 12px;
    min-height: 60px;
}
.service-menu ul li strong {
    text-transform: uppercase;
    font-size: 16px;
}
.service-menu ul li span {
    font-weight: 600;
}
.service-menu ul li a {
    color: #625d5b;
    font-weight: 300;
    font-size: 15px;
    display: block;
    text-align: left;
    padding: 0px;
    text-transform: none;
}
.service-menu ul li a:hover {
    text-decoration: underline;
}
.service-dropdown-menu {
    left: 0;
    position: relative;
    right: 0;
    z-index: 999999999;
}
.sub-nav .service-dropdown-menu .sub-menu li {
    background: transparent;
}
.sub-nav .service-dropdown-menu .sub-menu li .menu-service-ul li {
    background: #fff;
}
.service-dropdown-menu .container {
    width: 100%;
    max-width: 100%;
}

.dropdown-menu {
    width: 100%;
    padding:    0px;
    border-radius: 0px 0px 0px;
}
.dropdown-menu li {
    border-top: 1px solid #696969;
}
.dropdown-menu li:first-child {
    border-top: 0px;
}
.dropdown-menu > li > a {
    padding: 7px 20px;
}
.dropdown-submenu {
    position: relative;
}

.dropdown-menu > li {
    position: relative;
}
.dropdown-menu > li i {
    position: absolute;
    right: 10px;
    top: 10px;
}

.dropdown-submenu .dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -1px;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover {
    background: none;
    color: #fff;
}

.sub-nav, .sub-nav2 {
    position: absolute;
    left: 0;
    right: 0;
    z-index: 999999999999;
    text-align: left;
    background: #625d5b;
    border-left: 1px solid #383331;
    border-right: 1px solid #383331;
    border-bottom: 1px solid #383331;
    border-top: 1px solid #383331;
    padding: 0px;
    display: none;
    top: 101px;
}
.sub-nav li, .sub-nav2 li {
    float: left;
    list-style-type: none;
    width: 24% !important;
    border: 0px;
    position: relative;
    margin: 0.5%;
    background: #fff;
}
.sub-nav li a, .sub-nav2 li a {
    padding: 10px 25px;
    font-size: 15px;
    font-weight: 700;
    text-transform: uppercase;
    display: block;
    text-align: center;
    color: #333;
    transition: all 0.3s;
}
.sub-nav li a:hover, .sub-nav2 li a:hover {
    text-decoration: none;
    background: #74abd7;
    color: #fff;
}
#menu3nav .service-dropdown > .sub-nav > li > a:hover,
#menu3nav .service-dropdown > .sub-nav > li > a.current {
    color: #fff !important;
}
#menu3nav .service-dropdown-menu .sub-menu > ul > li > a {
    background: none !important;
}
#menu3nav .service-dropdown-menu .sub-menu > ul > li > .menu-service-ul > li > a:hover {
    color: #fff !importa
}
.sub-nav li .service-dropdown-menu {
    top: 54px;
}
.sub-nav li a:hover, .sub-nav2 li a:hover {
    text-decoration: none;
}
.service-dropdown-menu {
    display: none;
    transition: all ease 0.3s;
}
.sub-nav2 li:hover .service-dropdown-menu {
    transition: all ease 0.3s;
    display: block;
}
.sub-nav li a.current {
    color: #fff;
}
.sub-nav li a.current + .service-dropdown-menu {
    display: block;
}
#menu3nav .navbar-header {
    display: inline-block;
    width: 24%;
    vertical-align: middle;
    float: none;
}
#menu3nav .service-dropdown > .sub-nav > li > a:hover,
#menu3nav .service-dropdown > .sub-nav > li > a.current {
    color: #fff !important;
}
#menu3nav .navbar-header + div {
    /*position: absolute;*/
    /*right: 0px;*/
    display: inline-block;
    vertical-align: middle;
    width: 75%;
    /*align-items: center;*/
    /*height: 100%;*/
}
.navbar {
    padding: 5px 0px !important;
}
#menu2nav .navbar-header {
    float: none;
    display: inline-block;
    width: 24%;
}
#menu2nav .navbar-header + div {
    display: inline-block;
    vertical-align: middle;
    width: 75%;
}
.navbar {
    padding: 5px 0px !important;
}
.sub-nav li a.current + .service-dropdown-menu {
    display: block;
}
.service-dropdown-menu {
    z-index: 9999;
}

@media (max-width: 990px) and (min-width: 0px) {
    .logo {
        width: auto;
        max-width: 250px;
        margin: 0 auto
    }
    .column {
        display: block;
    }
    .navbar-toggle.collapsed {
        display: none;
    }
    .top-header ul.socialiconheader {
        display: none;
    }
    .top-header ul li:last-child {
        display: none;
    }
    #menu3nav .navbar-header + div {
        width: 100%;
        display: none;
    }
    .navbar-toggle {
        margin-top: 20px;
        margin-right: 0px;
    }
    nav.navbar.navbar-default {
        display: block;
    }
    #menu3nav .navbar-header {
        width: 100%;
        margin: 0px;
    }
    .front .navbar-navigation.collapse {
        display: none !important;
    }
    .front .navbar-navigation.collapse.in {
        display: block !important;
    }
    .front .navbar-navigation > li {
        border-left: 0px;
    }
    .front .navbar-navigation > li {
        padding: 0px;
    }
}
</style>
<script src="<?php echo base_url('assets/js/modernizr.custom.js'); ?>"></script>
<div class="collapse insider" id="menu3-inside<?php echo $secid; ?>">
    <?php $this->load->view('sections/sectioncontrols');?>
    <ul id="nav2" class="nav navbar-nav connectedSortable">
        <?php foreach ($notInmenu as $key => $value) {?>
            <li id="<?php echo 'item-' . $key; ?>"  class="ui-state-default">
            <a class="nav-handle" href='#'><i class="fa fa-arrows" aria-hidden="true"></i></a>
            <a href="<?php echo $value['page_title']; ?>" pageid="<?php echo $key; ?>" data-page="<?php echo $key; ?>"><?php echo $value['menu_title']; ?></a>
            </li>
        <?php }?>
    </ul>
</div>
<?php echo $section['section_html']; ?>
            <ul id="nav" class="navlinks nav navbar-nav navbar-navigation navbar-collapse collapse pull-right dropdown">
            <?php $cols=0;
                $CI =& get_instance();
                foreach ($headermenu as $key => $value) {
                    $cols++;
                    if (isset($value['menu_title'])) {?>
                <li class="<?php if ($CI->multiKeyExists($value['children'], 'categories')) {
                            echo 'service-dropdown';
                        } elseif (count($value['children'])) {
                            echo 'service-dropdown';
                        }?>" id="<?php echo 'item-' . $key; ?>">
                    <a class="nav-handle" href='#'><i class="fa fa-arrows" aria-hidden="true"></i></a>
                    <?php if (count($value['children'])) {?>
                    <a href="javascript:;" ondragstart="return false;" ondrop="return false;" pageid="<?php echo $key; ?>" data-page="<?php echo $key; ?>"><span class="editable"><?php echo $value['menu_title']; ?></span></a>
                    <?php } else {?>
                    <a href="<?php echo ($value['external_url'] != '') ? $value['external_url'] : $value['page_title']; ?>" ondragstart="return false;" ondrop="return false;" pageid="<?php echo $key; ?>" data-page="<?php echo $key; ?>" <?php echo ($value['external_url'] != '') ? 'target="_blank"' : ''; ?>><span class="editable" ><?php echo $value['menu_title']; ?></span></a>
                    <?php }
                    if (count($value['children'])) {
                        $isService = $CI->multiKeyExists($value['children'], 'categories');?>
                    <ul class="<?php echo ($isService == true) ? 'sub-nav' : 'sub-nav2'?>">
                    <?php
                        if ($isService) {
                            foreach ($value['children']['categories'] as $keyCat => $cat) { ?>
                        <li><a href="javascript:void();"><?php echo $cat['name']; ?></a>
                            <?php if (isset($cat['subcategories'])) {?>
                            <div class="service-dropdown-menu">
                                <div class="container">
                                    <div class="row">
                                        <div class="clearfix service-menu">
                                            <div class="sub-menu">
                                                <ul>
                                                    <li>
                                                    <?php foreach ($cat['subcategories'] as $subkey => $subcat) {?>
                                                        <a href="javascript:void(0);"><strong><?php echo $subcat['name'];?></strong></a>
                                                        <?php
                                                            if (isset($subcat['services'])) {?>
                                                        <ul class="menu-service-ul">
                                                            <?php
                                                                foreach ($subcat['services'] as $serkey => $serv) { ?>
                                                            <li>
                                                                <a ondragstart="return false;" ondrop="return false;" href="<?php echo  $serv->page_title; ?>" pageid="<?php echo $serv->page_id; ?>" data-page="<?php echo $serv->page_id; ?>">
                                                                    <?php echo $serv->name;?>
                                                                </a>
                                                            </li>
                                                        <?php       }?>
                                                        </ul>
                                                        <?php
                                                                }
                                                            }?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </li>
                            <?php
                            }
                        } else {
                            foreach ($value['children'] as $catkey => $cat) {
                                if (isset($cat['name'])) { ?>
                        <li><a href="<?php echo $cat['page_title']; ?>"><?php echo $cat['name']; ?></a></li>
                            <?php
                                }
                            }
                        } ?>
                    </ul>
                        <?php
                    } ?>
                    </li>
                <?php  }
                }?>
            </ul>
        </div>
    </div>
</nav>
<div class="column">
    <div id="dl-menu" class="dl-menuwrapper">
        <button class="dl-trigger">Open Menu</button>
        <?php if (isset($headermenu)) { ?>
        <ul class="dl-menu"><?php
        foreach ($headermenu as $key => $value) {
            if(isset($value['menu_title'])){?>
            <li>
                <?php if(count($value['children'])) {?>
                <a href="javascript:;"><?php echo $value['menu_title']; ?></a>
                <?php } else {  ?>
                <a href="<?php echo ($value['external_url'] != '') ? $value['external_url'] : $value['page_title']; ?>" <?php echo ($value['external_url'] != '') ? 'target="_blank"' : ''; ?>><?php echo $value['menu_title']; ?></a>
                <?php } ?>
                <?php if(count($value['children'])) {
                    $isService = $CI->multiKeyExists($value['children'], 'categories');?>
                <?php
                if ($isService) {
                    foreach ($value['children']['categories'] as $keyCat => $cat) { ?>
                <ul class="dl-submenu">
                    <li><a href="#"><?php echo $cat['name']; ?></a>
                    <?php if (isset($cat['subcategories'])) {
                        foreach ($cat['subcategories'] as $subkey => $subcat) { ?>
                        <ul class="dl-submenu subdiv">
                            <li><a href="#"><?php echo $subcat['name']; ?></a>
                                <?php if (isset($subcat['services'])) { ?>
                                <ul class="dl-submenu srvdiv">
                                <?php foreach ($subcat['services'] as $serkey => $serv) {?>
                                    <li><a href="<?php echo  $serv->page_title; ?>"><?php echo $serv->name;?></a></li>
                                    <?php
                                    }?>
                                </ul>
                                <?php } ?>
                            </li>
                        </ul>
                    <?php }
                    } else if (isset($cat['services'])) { ?>
                    <ul class="dl-submenu srvdiv">
                        <?php foreach ($cat['services'] as $serkey => $serv) {?>
                            <li><a href="<?php echo  $serv->page_title; ?>"><?php echo $serv->name;?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                    </li>
                </ul>
                <?php
                    }
                } else { ?>
                <?php foreach ($value['children'] as $catkey => $cat) {
                if (isset($cat['name'])) { ?>
                <ul class="dl-submenu">
                    <li><a href="<?php echo $cat['page_title']; ?>"><?php echo $cat['name']; ?></a>
                    <?php if (count($cat['children'])) {
                        foreach ($cat['children'] as $catkey2 => $cat2) {
                            if (isset($cat2['name'])) {?>
                        <ul class="dl-submenu subdiv">
                            <li><a href="<?php echo $cat2['page_title']; ?>"><?php echo $cat2['name']; ?></a></li>
                        </ul>
                    <?php } }
                    } ?>
                    </li>
                </ul>
                <?php }}}} ?>
            </li>
        <?php }
        } ?>
        </ul>
        <?php } ?>
    </div><!-- /dl-menuwrapper -->
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url('assets/js/jquery.dlmenu.js'); ?>"></script>
<script>
    $(document).ready(function(){
        $( '#dl-menu' ).dlmenu({
            animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
        });

        $('.dl-submenu').each(function () {
            $('ul li.dl-back').not(':eq(0)').hide();
        });

        $(".subdiv").each(function () {
            $(this).eq(0).css("display", "block");
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('.service-dropdown a').on("click", function(e){
            $(this).next('ul').toggle();
            // e.stopPropagation();
            // e.preventDefault();
        });
        $('.page-dropdown a').on("click", function(e){
            $(this).next('ul').toggle();
        });
        $('.sub-nav > li > a').on('click', function(){
            $('.sub-nav > li > a.current').removeClass('current');
            $(this).addClass('current');
        });
    });
</script>
