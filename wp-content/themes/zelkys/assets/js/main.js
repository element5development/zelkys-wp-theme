/* ---- start DROPDOWN LIST ----- */
jQuery.fn.dropdownlist = function() { 
    console.log('dropdownlist')      
    var select = jQuery(this)
    var label = select.find('label')
    var icon = select.find('label i, label svg')
    var list = select.find('ul')
    var item = select.find('li')
    var item_choice = undefined
    var input = select.find('[type="hidden"]')

    function hide () {
        jQuery('body').off("click")
        jQuery('body').off('keyup')
        item.off('click', choice)
        label.on('click', show)
        list.slideUp(function() {
            icon.removeClass('icon_active')
        })
    }

    function show() {
        label.off('click', show)
        jQuery(this).closest(select).find(list).slideDown(function() {
            jQuery(this).closest(select).find(item).on('click', choice)
            jQuery(this).closest(select).find(icon).addClass('icon_active')
            jQuery('body').on("click", hide)
            jQuery('body').on('keyup', function(event){
                if(event.which != 27)return
                    hide()
            })
        })          
    }

    function choice () {
        event.stopPropagation()
        jQuery(this).closest(select).find(input).val((jQuery(this).data('val')) ? jQuery(this).data('val') : jQuery(this).text()).trigger('change')
        jQuery(this).closest(select).find(label).find('span').text(jQuery(this).text())
        hide()
    }

    label.on('click', show)
}
/* ---- end DROPDOWN LIST ----- */
$('.filter-category').dropdownlist();
$('.filter-ordering').dropdownlist();
$('#sorting-order').on('change', function(e) {
    // console.log($(this).closest('ul').find('.icon_active').parent());
    $('select[name="orderby"]').val($(this).val());
    // console.log($('select[name="orderby"]').val());
    // console.log($('select[name="orderby"]'));
    $('select[name="orderby"]').trigger('change');
    $('form.woocommerce-orderin').submit();
});

/* ---- start COLOR, SIZE, QUANTITY PRODUCT ----- */
jQuery(document).ready(function () {

    function updateQuantity(shift) {
        let val = $('.quantity .qty').val()
        if(Number(val)+shift < 0) return
        $('.quantity .qty').val(Number(val)+shift)       
    }

    $('.quantity-minus').on('click', () => updateQuantity(-1))
    $('.quantity-plus').on('click', () => updateQuantity(+1))

    let selector = $('#pa_color, #pa_size')
    let options = selector.siblings('.attribute-option')
    
    options.on('click', function() {
            console.log($(this).siblings(selector).val())
            $(this).addClass('active-option')
            $(this).siblings(options).not($(this)).removeClass('active-option')
            $(this).siblings(selector).val($(this).data('value')).trigger('change')
    })

    var $colorLis = $('.container-card').find('ul.list-color').find('li');
    $colorLis.on('click', function(e) {
        $colorLis.removeClass('active');
        $(this).addClass('active');
        var card = $(this).parents('.card');
        var variation = getVariation($(this).data('value'), card.find('ul.list-size').find('li.active').data('value'), card.data('product'));
        updateCardWithVariation(card, variation);
    });
    var $sizeLis = $('.container-card').find('ul.list-size').find('li');
    $sizeLis.on('click', function(e) {
        $sizeLis.removeClass('active');
        $(this).addClass('active');
        var card = $(this).parents('.card');
        var variation = getVariation(card.find('ul.list-color').find('li.active').data('value'), $(this).data('value'), card.data('product'));
        updateCardWithVariation(card, variation);
    });

    var cards = $('.container-card').find('.card');
    cards.each(function(index, item) {
        var variation = getVariation($(item).find('ul.list-color').find('li.active').data('value'), $(item).find('ul.list-size').find('li.active').data('value'), $(item).data('product'));
        updateCardWithVariation($(item), variation);
    });
    $('.container-card').find('.card-btn').on('click', function(e) {
        e.preventDefault();
        // console.log($(this).parents('.container-card').find('form'));
        var form = $(this).parents('.container-card').find('form');
        // console.log(form.attr('action'));
        // form.attr('action', form.attr('action') + '&variation_id=' + form.find('input[name="variation_id"]').val());
        // console.log(form.attr('action'));
        var carturl = 'https://www.zelkys.com/?post_type=product&add-to-cart=' + form.find('input[name="product_id"]').val();
        if (form.find('input[name="variation_id"]').val()) {
            var carturl = carturl+'&variation_id='+form.find('input[name="variation_id"]').val();
        }
        $.get(carturl, function() {
            window.location.reload();
        });
        
//         form.submit();
    });
})
/* ---- end COLOR, SIZE, QUANTITY PRODUCT ----- */

function getVariation(color, size, variations) {
    // console.log(color);
    // console.log(size);
    // console.log(variations);
    var result = false;
    $(variations).each(function(index, item) {
        if (item.attributes.attribute_pa_color == color && item.attributes.attribute_pa_size == size) {
            result = item;
        }
    });
    return result;
}

function updateCardWithVariation(card, variation) {
    var form = card.parents('.container-card').find('form');
    if (variation) {
        card.find('img').attr('src', variation.image.url);
        form.find('input[name="variation_id"]').val(variation.variation_id);
        if (variation.price_html) {
            card.find('p.prise').html(variation.price_html);
        }
    }
}

var observeDOM = (function(){
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    return function( obj, callback ){
        if( !obj || !obj.nodeType === 1 ) return; // validation
        if( MutationObserver ){
            // define a new observer
            var obs = new MutationObserver(function(mutations, observer){
                callback(mutations);
            });
            // have the observer observe foo for changes in children
            obs.observe( obj, { childList:true, subtree:true });
        }
        else if( window.addEventListener ){
            obj.addEventListener('DOMNodeInserted', callback, false);
            obj.addEventListener('DOMNodeRemoved', callback, false);
        }
    }
})();
observeDOM($('.check-balance-component')[0], function(m) {
    // console.log(m[0].addedNodes[0]);
    var row = $(m[0].addedNodes[0]).find('.row:nth-of-type(2)');
    var col1 = row.find('label.col-md-6:nth-of-type(1)');
    // console.log(col1);
    var col2 = row.find('div.col-md-6:nth-of-type(1)');
    // console.log(col2);
    col1.removeClass('col-md-6').addClass('col-md-5');
    col2.removeClass('col-md-6').addClass('col-md-7');
});