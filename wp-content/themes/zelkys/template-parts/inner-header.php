
<?php 
$selectedPage = get_field('select_page');
?>
<section class="sub-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-bottom: 15px;">
                <?php if (empty($selectedPage) || count($selectedPage) <= 1) { ?>
                    <h3 style="margin-bottom: 0; margin-top: 25px;">
                        <figure class="arrow-left"><img class="animated-arrow" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-left.png"></figure>
                        <?php if (is_home()): ?>
                        <?php echo 'BLOG'; ?>
                        <?php else : ?>
                        <?php the_title(); ?>
                        <?php endif ?>
                        <figure class="arrow-right">
                            <img class="animated-arrow-right" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-right.png">
                        </figure>
                    </h3>
                <?php } else { ?>
                <?php foreach ($selectedPage as $key => $value) {
                    if (get_the_title() == $value->post_title) { ?>
                        <h3 style="margin-bottom: 0;">
                            <figure class="arrow-left"><img class="animated-arrow" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-left.png"></figure>
                            <?php the_title(); ?>
                            <figure class="arrow-right">
                                <img class="animated-arrow-right" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-right.png">
                            </figure>
                        </h3>
                    <?php } else { 
                        echo '<p style="margin-bottom: 0;"><a href="'.get_permalink($value->ID).'" style="text-decoration: none; color: #0a3847;">'.$value->post_title.'</a></p>';
                    }
                } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>