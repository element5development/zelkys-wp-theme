    <section class="price text-center button_sec">
          <div class="container">
                <div class="price-sec">
                    <?php echo (get_field('heading')) ? '<h4>'.get_field('heading').'</h4>' : ''; ?>
                    <?php echo (get_field('content')) ? '<p>'.get_field('content').'</p>' : ''; ?>
                    <?php if (get_field('button_text') && get_field('button_link')): ?>
                    <div class="purchase-button text-center mt-40">
                        <figure class="arrow-left"><img class="animated-arrow" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-left.png"></figure>
                            <a href="<?php echo (get_field('button_link')) ? get_field('button_link') : ''; ?>" class="purchase-btn" <?php if(is_page('staff')) { echo 'target="_blank"'; } ?>><?php echo (get_field('button_text')) ? get_field('button_text') : ''; ?></a>
                        <figure class="arrow-right">
                            <img class="animated-arrow-right" src="<?php bloginfo('template_url'); ?>/assets/images/arrow-right.png">
                        </figure>
                    </div>
                    <?php endif ?>
                </div>
          </div>
    </section>