<?php if (get_field('fancy_title_image')): ?>
<section class="text-center" style="margin-top: 40px;">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">
                        <img src="<?php echo get_field('fancy_title_image'); ?>"> 
                  </div>
            </div>
      </div>
</section>
<?php endif ?>