<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<form class="woocommerce-orderin" method="get">
	<p class="label-ordering">Sort by</p>
	<select name="orderby" class="orderby" aria-label="<?php esc_attr_e( 'Shop order', 'woocommerce' ); ?>" style="display: none;">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<input type="hidden" name="paged" value="1" />
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
	<div class="select filter-ordering" style="margin-left: 0; margin-right: 60px; min-width: 200px; max-width: 250px">
		<label>
			<?php
			// var_dump($catalog_orderby_options);
			$selected_val = 'Default sorting';
			$selected_text = 'Default sorting';
			$orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : false;
			if ( $orderby_value ) {
				$selected_val = $orderby;
				$selected_text = $catalog_orderby_options[$orderby];
			}
			?>
			<span><?php echo $selected_text; ?></span>
			<input type="hidden" name="sorting-order" id="sorting-order" readonly value="<?php echo $selected_val; ?>">
			<i class="icon"></i>
		</label>
		<ul class="list">
			<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
				<li class="item" data-val="<?php echo esc_attr( $id ); ?>">
					<?php //var_dump(selected( $orderby, $id ));/selected( $orderby, $id ); ?>
					<a href="#"><?php echo esc_html( $name ); ?></a>
				</li>
			<?php endforeach; ?>
		</ul>	
	</div>
</form>
