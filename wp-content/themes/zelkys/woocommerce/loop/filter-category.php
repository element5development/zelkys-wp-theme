<?php
/**
 * Show options for ordering by category
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$cat_args = array(
    'orderby'    => 'name',
    'order'      => 'asc',
    'hide_empty' => false,
);
$categories = get_terms( 'product_cat', $cat_args );
?>
<div class="container-filter-category">
    <p>Category</p>
	<div class="select filter-category">
		<label>
			<span>All</span>
			<input type="hidden" name="party-package" id="party-package" readonly value="all">
			<i class="icon"></i>
		</label>
		<ul class="list">
			<?php foreach ( $categories as $category ) : ?>
				<li class="item">
					<a href="<?php echo home_url() . '/product-category/' . $category->slug ?>"><?php echo $category->name; ?></a>
				</li>
			<?php endforeach; ?>
		</ul>	
	</div>
</div>