<?php
/**
 * The template for displaying product widget entries.
 */

if ( ! function_exists( 'woocommerce_category_filter' ) ) {

	function woocommerce_category_filter() {
		global $woocommerce;
		$product_categories = woocommerce_get_product_subcategories();

		wc_get_template(
			'loop/filter-category.php',
			array(
			 	'categories' => $product_categories,
			// 	'orderby'                 => $orderby,
			// 	'show_default_orderby'    => $show_default_orderby,
			)
		);
	}
}

function custom_woocommerce_datails_tabs() {

}

function custom_woocommerce_product_tabs($tabs) {
	//print_var($tabs);
	$tabs['reviews']['title'] = 'Feedback';

	$details = [
	    'details' => [
	            'title' => 'Details',
	            'priority' => 20,
	            'callback' => 'custom_woocommerce_datails_tabs'
	    ]
	];

	$tabs = array_merge($tabs, $details);


	return $tabs;
}

function card_content($a, $b) {
	//print_var($a);
}

if ( ! function_exists( 'woocommerce_variable_add_to_cart' ) ) {

	/**
	 * Output the variable product add to cart area.
	 */
	function custom_variable_add_to_cart() {
		global $product;

		// Enqueue variation scripts.
		wp_enqueue_script( 'wc-add-to-cart-variation' );

		// Get Available variations?
		$get_variations = count( $product->get_children() ) <= apply_filters( 'woocommerce_ajax_variation_threshold', 30, $product );

		// Load the template.
		wc_get_template(
			'single-product/add-to-cart/color-variable.php',
			array(
				'available_variations' => $get_variations ? $product->get_available_variations() : false,
				'attributes'           => $product->get_variation_attributes(),
				'selected_attributes'  => $product->get_default_attributes(),
			)
		);
	}
}

function new_loop_shop_per_page( $cols ) {
 	$cols = 8;
 	return $cols;
}




/*
* Get image url of the post by id
* 
* @param number $id
*
* @return string
*/
function get_post_image_url($id) {
	$img_url = get_the_post_thumbnail_url($id);

	if ( !empty($img_url) ) 
		return $img_url;

	return get_template_directory_uri() . '/assets/images/photo_placeholder.png';
}


//// CATEGORY COLOR PICKER


/**
 * Add new colorpicker field to "Add new Category" screen
 * - https://developer.wordpress.org/reference/hooks/taxonomy_add_form_fields/
 *
 * @param String $taxonomy
 *
 * @return void
 */
function colorpicker_field_add_new_category( $taxonomy ) {
	// var_dump($taxonomy);
	// die();
  	?>
    <div class="form-field term-colorpicker-wrap">
        <label for="term-colorpicker">Category Color</label>
        <input name="_category_color" value="#ffffff" class="colorpicker" id="term-colorpicker" />
        <p>Select a color for this product variation.</p>
    </div>
  	<?php
}
add_action( 'pa_color_add_form_fields', 'colorpicker_field_add_new_category' );  // Variable Hook Name

/**
 * Add new colopicker field to "Edit Category" screen
 * - https://developer.wordpress.org/reference/hooks/taxonomy_add_form_fields/
 *
 * @param WP_Term_Object $term
 *
 * @return void
 */
function colorpicker_field_edit_category( $term ) {
    $color = get_term_meta( $term->term_id, '_category_color', true );
    $color = ( ! empty( $color ) ) ? "#{$color}" : '#ffffff';
  	?>
    <tr class="form-field term-colorpicker-wrap">
        <th scope="row"><label for="term-colorpicker">Product Color</label></th>
        <td>
            <input name="_category_color" value="<?php echo $color; ?>" class="colorpicker" id="term-colorpicker" />
            <p class="description">Product variation color.</p>
        </td>
    </tr>
  	<?php
}
add_action( 'pa_color_edit_form_fields', 'colorpicker_field_edit_category' );   // Variable Hook Name

/**
 * Term Metadata - Save Created and Edited Term Metadata
 * - https://developer.wordpress.org/reference/hooks/created_taxonomy/
 * - https://developer.wordpress.org/reference/hooks/edited_taxonomy/
 *
 * @param Integer $term_id
 *
 * @return void
 */
function save_termmeta( $term_id ) {
    // Save term color if possible
    if( isset( $_POST['_category_color'] ) && ! empty( $_POST['_category_color'] ) ) {
        update_term_meta( $term_id, '_category_color', sanitize_hex_color_no_hash( $_POST['_category_color'] ) );
    } else {
        delete_term_meta( $term_id, '_category_color' );
    }
}
add_action( 'created_pa_color', 'save_termmeta' );  // Variable Hook Name
add_action( 'edited_pa_color',  'save_termmeta' );  // Variable Hook Name

/**
 * Enqueue colorpicker styles and scripts.
 * - https://developer.wordpress.org/reference/hooks/admin_enqueue_scripts/
 *
 * @return void
 */
function category_colorpicker_enqueue( $taxonomy ) {
    if ( null !== ( $screen = get_current_screen() ) && 'edit-pa_color' !== $screen->id ) {
        return;
    }
    // Colorpicker Scripts
    wp_enqueue_script( 'wp-color-picker' );
    // Colorpicker Styles
    wp_enqueue_style( 'wp-color-picker' );
}
add_action( 'admin_enqueue_scripts', 'category_colorpicker_enqueue' );

/**
 * Print javascript to initialize the colorpicker
 * - https://developer.wordpress.org/reference/hooks/admin_print_scripts/
 *
 * @return void
 */
function colorpicker_init_inline() {
    if ( null !== ( $screen = get_current_screen() ) && 'edit-pa_color' !== $screen->id ) {
        return;
    }
  	?>
    <script>
        jQuery(document).ready(function($) {
            $('.colorpicker').wpColorPicker();
        }); // End Document Ready JQuery
    </script>
  	<?php
}
add_action( 'admin_print_scripts', 'colorpicker_init_inline', 20 );

function move_variation_price() {
    remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
    add_action( 'woocommerce_after_add_to_cart_button', 'woocommerce_single_variation', 10 );
}
add_action( 'woocommerce_before_add_to_cart_form', 'move_variation_price' );