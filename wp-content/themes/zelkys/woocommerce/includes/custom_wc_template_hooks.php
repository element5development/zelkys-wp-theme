<?php
/**
 * 
 */
add_action( 'woocommerce_before_shop_loop', 'woocommerce_category_filter', 25 );
add_filter( 'woocommerce_product_tabs', 'custom_woocommerce_product_tabs' );
//add_filter( 'woocommerce_structured_data_product', 'card_content' );

//add_action( 'woocommerce_variable_add_to_cart', 'custom_variable_add_to_cart', 30 );

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );