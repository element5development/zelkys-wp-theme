<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
//print_var($product->get_type());

$available_variations = null;

if ( $product->get_type() !== 'simple' && !is_singular('product'))
	$available_variations = $product->get_available_variations();

$attributes = false;
if ( !empty( $available_variations ) ) {
	// print_var( $available_variations );
	// print_var( $peoduct->list_attributes() );
	// wc_display_product_attributes( $product );
	$attributes = $product->get_attributes();
	// print_var( $attributes );
	foreach ( $attributes as $attribute ) {
		// print_var( $attribute->get_terms() );
	}
	
	$variations_json = wp_json_encode( $available_variations );
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// $total_sales = get_post_meta( $product->get_id(), 'total_sales', true );
?>
<div class="container-card">
	<div class="card" data-product="<?php if (isset($variations_json)) echo htmlspecialchars( $variations_json ); ?>">
		<?php if($product->is_featured()) { ?>
		<span class="selling">Best selling</span>
		<?php } ?>
		<div class="container-top">
			<div class="wrap-img">
				<a href="<?php echo get_permalink( $product->get_id() ); ?>"><img src="<?php echo get_post_image_url($product->get_id()); ?>" alt=""></a>
			</div>
			<?php
			if ( !empty( $available_variations ) ) {
				foreach ( $attributes as $attribute ) {
					// var_dump( $attribute->get_data() );
					if ( $attribute->get_data()['name'] == 'pa_color' ) {
						?>
						<ul class="list-color">
							<?php
							$selected = 'active';
							foreach ( $attribute->get_terms() as $term ) {
								$term = get_term_by( 'slug', $term->slug, 'pa_color' );
								$color = get_term_meta( $term->term_id, '_category_color', true );
								if ( $color ) {
									$color = '#' . $color;
								} else {
									$color = '#ffffff';
								}
								?><li class="<?php echo $selected; ?>" style="background-color: <?php echo $color; ?>" data-value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li><?php
								$selected = '';
							} ?>
						</ul>
						<?php
					}
				}
			}
			?>
		</div>
		<div class="container-bottom">
			<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'prise' ) );?>"><?php echo $product->get_price_html(); ?></p>
			<p class="title"><?php echo get_the_title($product->get_id()); ?></p>
			<?php
			if ( !empty( $available_variations ) ) {
				$available_sizes = [];
				foreach ($available_variations as $singleVariation) {
					array_push($available_sizes, $singleVariation['attributes']['attribute_pa_size']);
				}
				foreach ( $attributes as $attribute ) {
					// var_dump( $attribute->get_data() );
					if ( $attribute->get_data()['name'] == 'pa_size' ) {
						?>
						<ul class="list-size">
							<?php
							$selected = 'active';
							foreach ( get_terms("pa_size") as $term ) {
								if (in_array($term->slug, $available_sizes)) {
									?><li class="<?php echo $selected; ?>" data-value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li><?php
									$selected = '';
								}
							} ?>
						</ul>
						<?php
					}
				}
			}
			?>
		</div>
	</div>
	<div class="container-pop-up-card">
		<form method="post" action="<?php echo home_url( '/cart?add-to-cart=' . $product->id ); ?>" style="display: none;"  enctype="multipart/form-data">
			<div class="woocommerce-variation-add-to-cart variations_button">
		        <button type="submit" class="single_add_to_cart_button button"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		        <input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
		        <input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
		        <input type="hidden" name="variation_id" class="variation_id" value="0" />
		        <?php woocommerce_quantity_input(); ?>
		    </div>
	    </form>
		<a href="<?php echo get_permalink( $product->get_id() ); ?>" style="width: 100%">
			<p class="more">show more</p>
		</a>
		<a class="card-btn" href="#">
			<span class="quantity"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
		</a>
	</div>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
</div>
